\documentclass[
	fontsize=10pt, % Base font size
	twoside=true, % Use different layouts for even and odd pages (in particular, if twoside=true, the margin column will be always on the outside)
	%open=any, % If twoside=true, uncomment this to force new chapters to start on any page, not only on right (odd) pages
	%chapterprefix=true, % Uncomment to use the word "Chapter" before chapter numbers everywhere they appear
	%chapterentrydots=true, % Uncomment to output dots from the chapter name to the page number in the table of contents
	numbers=noenddot, % Comment to output dots after chapter numbers; the most common values for this option are: enddot, noenddot and auto (see the KOMAScript documentation for an in-depth explanation)
	%draft=true, % If uncommented, rulers will be added in the header and footer
	%overfullrule=true, % If uncommented, overly long lines will be marked by a black box; useful for correcting spacing problems
]{kaobook}

% Set the language
\usepackage[english]{babel} % Load characters and hyphenation
\usepackage[english=british]{csquotes} % English quotes

% Load packages for testing
\usepackage{blindtext}
%\usepackage{showframe} % Uncomment to show boxes around the text area, margin, header and footer
%\usepackage{showlabels} % Uncomment to output the content of \label commands to the document where they are used

% Load the bibliography package
\usepackage{styles/kaobiblio}
\addbibresource{main.bib} % Bibliography file

% Load mathematical packages for theorems and related environments. NOTE: choose only one between 'mdftheorems' and 'plaintheorems'.
\usepackage{styles/mdftheorems}
%\usepackage{styles/plaintheorems}

\graphicspath{{examples/documentation/images/}{images/}} % Paths in which to look for images

\makeindex[columns=3, title=Alphabetical Index, intoc] % Make LaTeX produce the files required to compile the index

\makeglossaries % Make LaTeX produce the files required to compile the glossary

\makenomenclature % Make LaTeX produce the files required to compile the nomenclature

% Reset sidenote counter at chapters
%\counterwithin*{sidenote}{chapter}

%----------------------------------------------------------------------------------------
\usepackage{listing}
\usepackage{hyperref}
\usepackage{braket}
\usepackage{cancel}

\newcommand{\R}{\mathbb{R}}
\newcommand{\Comp}{\mathbb{C}}
\newcommand{\diff}{\mathop{}\!d}

\newcommand{\unity}{\mathbb{1}}


\usepackage{xcolor, colortbl}
\definecolor{Gray}{gray}{0.95}
\definecolor{lGray}{gray}{0.90}
\definecolor{sGray}{gray}{0.75}

\usepackage{tikz}
\usetikzlibrary{matrix,arrows.meta}

\usepackage{braket}
\usepackage{amsmath}
%\usepackage{showframe}

%\usepackage{mathtools}
%%Hide non-used labeled equations
%\mathtoolsset{showonlyrefs=true}
%% this feature will not work if cleveref is activated
\begin{document}

%----------------------------------------------------------------------------------------
%	BOOK INFORMATION
%----------------------------------------------------------------------------------------

\titlehead{Periodical Report}
\subject{Surface Hopping, Topological Forces and geometric phases in Physics}

\title[Periodical Report]{$ \mathbf{1^{st}} $ Periodical Report}
\subtitle{Ideas, literature, notes, (partial) results and future works}

\author[Erik Pillon]{$ e\pi^i $ \thanks{A \LaTeX\ lover}}

\date{\today}

\publishers{\textsc{tcp} Group \\ University of Luxembourg }

%----------------------------------------------------------------------------------------

\frontmatter % Denotes the start of the pre-document content, uses roman numerals


%----------------------------------------------------------------------------------------
%	COPYRIGHT PAGE
%----------------------------------------------------------------------------------------

% \include{copyright}

%----------------------------------------------------------------------------------------
%	DEDICATION
%----------------------------------------------------------------------------------------

\dedication{
	The miracle of the appropriateness of the language of mathematics for the formulation of the laws of physics is a wonderful gift which we neither understand nor deserve.\\
	\flushright -- Eugene Paul Wigner.
	
	\vspace{2cm}

	And God said: \\
	"Let there be light".
	
	\vspace{0.2cm}
	
	And \emph{gauge invariance} was.	
}

%----------------------------------------------------------------------------------------
%	OUTPUT TITLE PAGE AND PREVIOUS
%----------------------------------------------------------------------------------------

% Note that \maketitle outputs the pages before here

% If twoside=false, \uppertitleback and \lowertitleback are not printed
% To overcome this issue, we set twoside=semi just before printing the title pages, and set it back to false just after the title pages
\KOMAoptions{twoside=semi}
\maketitle
\KOMAoptions{twoside=false}

%----------------------------------------------------------------------------------------
%	PREFACE
%----------------------------------------------------------------------------------------

%\input{chapters/preface.tex}

%----------------------------------------------------------------------------------------
%	TABLE OF CONTENTS & LIST OF FIGURES/TABLES
%----------------------------------------------------------------------------------------

\begingroup % Local scope for the following commands

% Define the style for the TOC, LOF, and LOT
%\setstretch{1} % Uncomment to modify line spacing in the ToC
%\hypersetup{linkcolor=blue} % Uncomment to set the colour of links in the ToC
\setlength{\textheight}{23cm} % Manually adjust the height of the ToC pages

% Turn on compatibility mode for the etoc package
\etocstandarddisplaystyle % "toc display" as if etoc was not loaded
\etocstandardlines % toc lines as if etoc was not loaded

\tableofcontents % Output the table of contents

\listoffigures % Output the list of figures

% Comment both of the following lines to have the LOF and the LOT on different pages
\let\cleardoublepage\bigskip
\let\clearpage\bigskip

\listoftables % Output the list of tables

\endgroup

%----------------------------------------------------------------------------------------
%	MAIN BODY
%----------------------------------------------------------------------------------------

\mainmatter % Denotes the start of the main document content, resets page numbering and uses arabic numbers
\setchapterstyle{kao} % Choose the default chapter heading style

\part{Bibliography and reviews}

\input{chapters/intro}
\input{chapters/SH}

\part{Ideas, Projects and Extensions}

\input{chapters/PO}
\input{chapters/CI}

\chapter{Philosophical/open questions}
\begin{flushright}
	\emph{A rather old fashioned way of looking at quantum mechanics\\
		 is that it is an application of the physics of waves.\\
		 A not so old fashioned way to get interesting physics out of mathematical objects \\
		 is by studying their singularities.\footnote{Berry, M.V.: Wave geometry: a plurality of singularities. In: Anandan, J.S. (ed.) Quantum
		 	Coherence, pp. 92–98. World Scientific, Singapore (1991)}}\\ 
	\textsc{Michael Berry}
\end{flushright}

I collect here in the following all the open (?) questions and the possible side-projects that I believe to be not only extremely interesting but also, potentially, groundbreaking. 

I stumbled upon them in different moments and I kept on the back of my mind along with the main projects workflow. 
\begin{itemize}
	\item Which conservation law underlines the Geometric phase and the exchange interaction?
	\item In the presence of a 2-fold CI, the best group is the $ U(2) $ while when we move away the best group becomes $ U(1) $. How is this transition happening? Which condition shall we impose on the group elements? See Section \ref{sec:U2_to_U1} for more information on this topic.
	\item Does it make sense to use the word \emph{topological spin}? 
	\item Is there any group that describes mathematically or physically the exchange interaction?
	\item We know (do we?) how to threat a semiclassical dynamics in the presence of a conical intersection. What's the difference with an avoided crossing?
	\item How is robust the group describing a symmetry when the symmetry is not present anymore (broken symmetry for example)
\end{itemize}

\begin{marginfigure}
	\includegraphics{Im/CI+mobius}
	\caption{When the electronic wavefunction goes around a Conical intersection it acquires a $ -1 $ sign like if it was travelling on a Mobius strip}
	\label{fig:mobius}
\end{marginfigure}


%\begin{marginfigure}
%	\includegraphics{Im/U2_U1}
%\end{marginfigure}

\section{From $ U(2) $ to $ U(1) $ }
\label{sec:U2_to_U1}

Here, the deterioration of $ SU(2) $ gauge symmetry as the energy gap between adiabats, $ |E_m-E_n $, increases is discussed. For small gaps, $ SU(2) $ is a good approximation. However, as the gap increases the system becomes non-degenerate, and eventually the adiabats lose their ability to communicate with one another through nonadiabatic transitions. Yet, the system retains memory of the intersection. For example, take the case of conical intersection. The topology of a cone is such that all of its curvature is at its apex. Thus, the curvature experienced in a closed circuit that encloses the origin does not depend on the distance from the origin. Spinor character is established near the degeneracy and it is not compromised by breakdown of $ SU(2) $ gauge symmetry.

In a close vicinity of the CI the of the degeneracy the BO covariant derivative $ D_m $ is expressed in terms of the gradient and the $ SU(2) $ generators and fields. We have:
\begin{equation}\label{eq:6.4}
D_{\mu}=-i
\begin{pmatrix}
W_3 & W_1-iW_2 \\
W_1+iW_2  & -W_3
\end{pmatrix}
=
-i\begin{pmatrix}
F_{mm} & F_{mn} \\
F_{nm}  & F_{nn}
\end{pmatrix}
\end{equation}
and we also note $ F_{mm}=-F_{nn} $ and $ F_{mn}=-F_{nm}^{\dagger} $.

In the immediate vicinity of the degeneracy, global $ U(2) $ gauge symmetry applies. It factors to $ U(1) \times SU(2) $, thereby enabling these subgroups to be judged separately. In anticipation of lifting the degeneracy through the tuning coordinates, $ SU(2) $ is gauged, but $ U(1) $ is not. As the energy gap between adiabats increases from zero, the system begins to lose $ SU(2) $ symmetry and replace it with a pair of related $ U(1) $ symmetries, i.e., one each for the upper and lower adiabats.
\begin{marginfigure}
	\includegraphics{Im/U2_to_U1_wittig}
	\caption{As $ |E_m-E_n $ increases, the off diagonals decrease. Diagonals are manifest in the $ U(1) $ regime as $ iF_{mm} $ and $ iF_{nn} $.}
	\label{fig:U2_to_U1_wittig}
\end{marginfigure}

Referring to Eq. \eqref{eq:6.4}, the coupling fields (off-diagonals) get smaller and eventually disappear, whereas $ W_3 $ 
and $ W_3 $ correlate to $ iF_{mm} $ and $ iF_{nn} $. 
Far from the intersection, the system can be on one or the other 
adiabat without experiencing transitions between them. $ SU(2) $ symmetry no longer exists. This is the regime of 
Berry’s connection. 
The sketch in Fig.\ref{fig:U2_to_U1_wittig} illustrates this correlation.

\begin{figure}[htbp!]
	\includegraphics{Im/U2_U1}
	\caption{The proximity of the region with the CI is dominated by the $ SU(2) $ regime. Moving away from the point, i.e., when $ |E_m-E_n| $ increases, there's a smooth (?) switch from $ U(2) $ to $ U(1) $.}
	\label{fig:moving_away_from_CI}
\end{figure}

\section{Is the AB effect real of quantum?}
Fundamental question about the AB effect are still going on nowadays \sidenote{See for example
\cite{stewart2016paper,aharonov2015comment,vaidman2015reply,pearle2017quantum,pearle2017quantized,li2018transition,WittigAppendiceV,wittig2012geometric}}. See also the sidenote at p.\pageref{note:vector_potential}

\section{Geometric Phase and exchange interaction}
\marginnote{See for example \cite{wittigAharonov} or the last section of \cite{cohen2019geometric}.}

\index{Exchange Interaction}
\input{chapters/exch_int}

There is a deep connection between exchange statistics of spins and the geometric phase. When
fully incorporating identicalness into the Hilbert space, the Pauli sign $ (-1)^{2S} $ was derived,
where $ S $ is the spin quantum number, as a geometric phase factor of topological origin
(167,168). The exchange of two identical particles, $ i $ and $ j $, belonging to a many-body state, $ \Psi $, is represented by the operator $ P $ . For Abelian particles, the outcome of the exchange $ i, j $ operation can be a phase $ \theta_{ij} $ , namely:
\begin{align}
	P_{i,j}\Psi(x_{1}, x_{2}, \dots, x_i, \dots, x_{j},\dots) &= 	\Psi(x_{1}, x_{2}, \dots, x_i, \dots, x_{j},\dots)\nonumber\\
	&= 	e^{i\theta_{ij}}\Psi(x_{1}, x_{2}, \dots, x_i, \dots, x_{j},\dots)\nonumber
\end{align}

Doubly exchanging the particles amounts to rotating one particle around the other. In three spatial dimensions, the trajectory representing this rotation can be continuously shrunk to a point, hence this braiding degenerates to the unit operator, $ P_{i,j}^2=1 $ . This implies that $ \theta_{ij} $ must be either equal to $ 0 $ or $ \pi $ (up to integer multiples of $ 2\pi $), defining bosons and fermions respectively. This turns out not to be the case in 2D, where the trajectory of particle $ i $
circumventing particle $ j $ cannot be brought continuously to a point. One may then have a
generalization of these two classes of particles (169): particles which are not necessarily bosons or fermions. Indeed, they may obey any statistics and thus are called \index{Anyons}\emph{anyons} (170). We note that the geometric topological phase accumulated over the closed trajectory (of $ i $
circumventing $ j $) is $ \mp 2\theta_{ij} $
(clockwise or counter-clockwise respectively). This is in contrast to the standard cases of bosons and fermions, in which the sign of the winding is immaterial(that is, the value of $ 2\theta_{ij} $ does not depend on the direction of the winding). The fact that for $ 2D $ anyons $ P_{ij} $ depends on the ‘history’ of the exchange (that is, on details of the trajectory of the particle exchange), implies that a simple-minded second quantized form of anyons is unattainable. One can nevertheless define anyonic Green functions by attaching a magnetic fluxon to the charged anyons (see for example, (171)), by implementing an exchange convention (for example, always rotate clockwise) (172), or by addressing an anyonic creation/annihilation product which is insensitive to the sign of $ \theta_{ij} $ (173).
Obvious realizations of anyons are quasi-particles of fractional quantum Hall phases (174). These particles possess fractional charge which may, under certain conditions, lead to fractional Aharonov-Bohm oscillations (175), and, at the same time fractional (anyonic) exchange statistics. Theoretical works proposed various interference platforms as protocols to detect fractional statistics (176), with the leading candidate being electronic Mach-Zehnder interferometers (173,177). There are also proposals for the detection of non-Abelian exchange statistics (177,178), where following a closed braiding trajectory the system’s many-body state is modified (that is, not only multiplied by a phase, but rather rotated in a degenerate Hilbert subspace). Notwithstanding extensive efforts, clear observation of a fractional statistical phase remains an open challenge.


\section{Redefinition of vector potential}
As said for example in \cite{feynman1989feynman} and \cite{iencinella2004introduction}.

\subsection{Going from $ U(1) $ to $ U(2) $}
An extremely nice paper on the topic is \cite{sundrum1986non} \sidenote{\label{note:vector_potential}In particular in \cite{sundrum1986non} a question is raised: \textbf{Aharonov Bohm effect is real/physical: there are no doubts about his existence and his reality. The Feynman-path description is a \emph{mathematical}/equivalent description: without this formalism, the AB effect would still exist. The vector potential is physical or mathematical?}} and also the book \cite{nakahara2003geometry} gives an operative way of defyning and working with SU(2) structures.


\section{Analogy of the theory of the GP with the Yang-Mills theory}
\cite{wittig2012geometric},\cite{william2008topological}, see also chapter 5 of \cite{baez1994gauge}

\section{Topological/geometrical force}
I collect here a series of paper where I found references more or less explicit to the topological/geometrical force

\begin{itemize}
	\item \fullcite{chuu2010semiclassical}
	\item \fullcite{subotnik2019demonstration}
\end{itemize}

\section{Time reversal invariance}
The following table is taken from \url{http://philsci-archive.pitt.edu/15033/1/Roberts2018-TimeReversal.pdf}
\begin{table}[htbp]
	\begin{tabular}{lllll}
		& \multicolumn{2}{c}{\textbf{Reversed}} & \multicolumn{2}{c}{\textbf{Preserved}} \\ \hline
		& Momentum & $ p\mapsto -p $ & Position & $ q\mapsto q $ \\
		& Magnetic field  &  $ B\mapsto -B $ & Electric Field &  $ E\mapsto E $   \\
		& Spin & $ \sigma\mapsto-\sigma $ & Kinetic Energy &   $ p^2/2m\mapsto p^2/2m $ \\
		& Position Wavefunction & $ \psi(x)\mapsto \psi(x)^{\dagger} $ & Transition probability &  $ |\braket{\psi|\phi}|^2\mapsto |\braket{\psi|\phi}|^2 $            
	\end{tabular}
\end{table}
In classical mechanics, the discussion about time reversal invariance is quite straightforward.
In quantum mechanics, instead, things are more complicated, especially because of the existence of internal degrees of freedom associated with observables like spin. For example, consider the standard representation of the Pauli matrices on a 2-dimensional Hilbert space $ H $:
\begin{equation} \nonumber
I = \begin{pmatrix}
1 & 0 \\
0 & 1
\end{pmatrix}
\qquad
\sigma_1 = \begin{pmatrix}
0 & 1 \\
1 & 0
\end{pmatrix}
\qquad
\sigma_2 = \begin{pmatrix}
0 & i \\
-i & 0
\end{pmatrix}
\qquad
\sigma_3 = \begin{pmatrix}
1 & 0 \\
0 & -1
\end{pmatrix}
\end{equation}
In this context, the time reversal operator is defined by
\begin{equation}
T\psi := \sigma_y \psi^{\dagger}
\end{equation}
where the vectors $ \psi $ are written in the eigenstate basis associated with the $ \sigma_3 $ operator.

In  quantum  mechanics,  time  reversal  invariance  means  that  if $ \psi(t) $ describes a unitary solution  to  the  Schr\"odinger  equation  for  some  Hamiltonian $ H $, in  that if $ \psi(t)=e^{-itH}\psi $ for some $ \psi\in H $, then the time-reversed trajectory $ T\psi(-t) $ is a unitary solution too, in that $ T\psi(-t) =e{-itH}\psi $ for some $ \psi\in H $. This turns out to be equivalent to the  statement that the antiunitary time reversal operator $ T $ commutes with the Hamiltonian,$ TH=HT $. 

Why think of time reversal in this way? In one sense, the answer is easy:  we want an operator that preserves position, and reverses momentum and spin.  But why do we want that? There are in fact systematic ways to answer this. For example, if one demands a time reversal transformation that is non-trivial, in that it allows at least one non-zero Hamiltonian that is time reversal invariant, then this is already enough to guarantee that the time reversal operator is antiunitary Symmetry arguments from the homogeneity and isotropy of space can then be used to determine the transformation rules for position, momentum and spin. The result of this analysis is a derivation of the standard time reversal transformation for quantum mechanics
\chapter{To add}

Arguments not to forget about include:
\begin{itemize}
	\item Work of Althorpe
	\item Hellman-Feynman Theorem \cite{esteve2010generalization,di2000hellmann,wallace2005introduction} \index{Hellman-Feynman Theorem}
	\item review of the Surface Hopping algorithm
	\item Reason why the Hamiltonian should be zero (back reasoning from the real electronic wavefunction)
	\item Yarkony was the first one to apply the study of the line integral for the detection of CI intersections
	\item The best way for introducing the Yang-Mills theory from the MABE is through the work of Wittig
	\item Intersection and time reversal invariance \cite{WittigAppendiceIV}, \cite{johnsson1998time}, \cite{wang2018dynamical}
	\item Possible (semi)classical explanation of the AB effect is in Iancinellla-Matteucci. \cite{iencinella2004introduction}
	\item Work of Takatsuka deriving the force from a path integral approach
	\item There's no built-in mechanism to ensure that the nuclear wave function changes sign in the conventional BO approximation (Kendrick, effect of pseudo magnetic fields)
	\item Curl Forces
\end{itemize}

\paragraph{NAC Sign problem}

--------------------------------------------------------- \newline
The sign of the NAC is one of the most important problems...; it is not only important for the Surface-Hopping algorithm but in general for all the studies that involve nonadiabatic phenomena. More generally, all of these algorithms require as input the potential-energy surfaces and the couplings between them: among the others, we can cite the already mentioned Surface-Hopping, the Meyer-Miller-Stock-Thoss (references needed) approach and and \emph{ab initio} multiple spawning. 

Derivative couplings are also needed in some methods for finding conical intersections \cite{domcke2004conical} and to compute rigorous diabatic states using the formalism of Baer.\cite{baer2002introduction} (other references needed)\newline
Taken from "Analytic derivative couplings between configuration-interaction-singles states with built-in electron-translation factors for translational invariance" \newline
--------------------------------------------------------- 

- In the supporting material information of the paper "An on the fly surface-hopping program JADE..." there's a good discussion about the numerical evaluation of the NAC

\appendix % From here onwards, chapters are numbered with letters, as is the appendix convention

\pagelayout{wide} % No margins
\addpart{Appendix}
\pagelayout{margin} % Restore margins

\input{appendix/3d}
\input{appendix/gauge}
%\input{appendix/calc}

\nocite{*}
%----------------------------------------------------------------------------------------

\backmatter % Denotes the end of the main document content
\setchapterstyle{plain} % Output plain chapters from this point onwards


%----------------------------------------------------------------------------------------
%	INDEX
%----------------------------------------------------------------------------------------

% The index needs to be compiled on the command line with 'makeindex main' from the template directory

\printindex % Output the index

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

% The bibliography needs to be compiled with biber using your LaTeX editor, or on the command line with 'biber main' from the template directory

\defbibnote{bibnote}{Here are the references in citation order.\par\bigskip} % Prepend this text to the bibliography
\printbibliography[heading=bibintoc, title=Bibliography, prenote=bibnote] % Add the bibliography heading to the ToC, set the title of the bibliography and output the bibliography note


%----------------------------------------------------------------------------------------
%	BACK COVER
%----------------------------------------------------------------------------------------

% If you have a PDF/image file that you want to use as a back cover, uncomment the following lines

%\clearpage
%\thispagestyle{empty}
%\null%
%\clearpage
%\includepdf{cover-back.pdf}

%----------------------------------------------------------------------------------------

\end{document}
