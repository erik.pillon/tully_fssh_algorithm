\chapter{3D vs. 2D Model}

This Chapter goes as follows: in Section \ref{sec:3d-model} we present all the calculations for the 3D model; in Section \ref{sec:2d-model} we perform the same calculations, but with a simplified Hamiltonian (i.e., the real projection of the 3D model above), while in section \ref{sec:proj-model} we show what happens if instead one takes the quantities $ A $ and $ \Omega $ and projects them in the plane $ k_y=0 $ and we show the differences with the previous calculations.

\section{3D Model}
\label{sec:3d-model}

Let us consider in detail the monopole model discussed in Ref.~\cite{gradhand2012first}.
Its Hamiltonian has the following form 
\begin{equation}\label{matr:complex_hamiltonian}
	\left(\begin{array}{cc} k_z & k_x - i k_y \\ 
		k_x + i k_y & -k_z \end{array}\right)\ ,
\end{equation}
The condensed form of the above Hamiltonian is $ \boldmath{\sigma}_i\cdot \textbf{k} $, where $ \boldmath{\sigma}_i $ are the Pauli matrices
\begin{equation}\label{eq:Pauli_matrices}
 \nonumber
I = \begin{pmatrix}
1 & 0 \\
0 & 1
\end{pmatrix}
\qquad
\sigma_1 = \begin{pmatrix}
0 & 1 \\
1 & 0
\end{pmatrix}
\qquad
\sigma_2 = \begin{pmatrix}
0 & i \\
-i & 0
\end{pmatrix}
\qquad
\sigma_3 = \begin{pmatrix}
1 & 0 \\
0 & -1
\end{pmatrix}
\end{equation}
The corresponding eigenvalue problem of Eq.\eqref{matr:complex_hamiltonian} \sidenote{Is clear that the eigenvalues of the Hamiltonian \eqref{matr:complex_hamiltonian} are 
	\[\lambda_{1,2}=\sqrt{H_z^2+H_x^2+H_y^2}, \]
	that is nothing but a $ S^3 $ sphere centered at the origin (see Section \ref{sec:2d-model}). Once we fix one plane, let's say $ H_z=const $, the rest is only a matter of encircling the conical intersection.}
\begin{align}
	\left(\begin{array}{cc} k_z & k_x - i k_y \\ 
		k_x + i k_y & -k_z \end{array}\right)
	\left(\begin{array}{c} C_1 \\ C_2 \end{array}\right) =
	{ \cal E} \left(\begin{array}{c} C_1 \\ C_2 \end{array}\right)\ .
\end{align}
Then we have
\begin{align}
	{\cal E}_\pm = \pm \sqrt{k_x^2 + k_y^2 + k_z^2} = \pm |{\textbf{k}}| = \pm k
\end{align}
Let us find the eigenvector corresponding to ${\cal E}_+ = k$:
\begin{align}
	\left(\begin{array}{cc} k_z - k & k_x - i k_y \\ 
		k_x + i k_y & -k_z -k \end{array}\right)
	\left(\begin{array}{c} C_1^+ \\ C_2^+ \end{array}\right) = 0\ ,
\end{align}
that means
\begin{align}
	(k_z - k) C_1^+ + (k_x - i k_y) C_2^+ = 0\, \nonumber\\ 
	(k_x + i k_y) C_1^+ -(k_z + k) C_2^+ = 0\ .\nonumber
\end{align}
From the first and second equation we have
\begin{align}
	C_2^+ = \frac{(k - k_z)}{(k_x - i k_y)}C_1^+ \ \ \text{and}
	\ \ \ \ C_2^+ = \frac{(k_x + i k_y)}{(k + k_z)} C_1^+\ ,
\end{align}
respectively. It is convenient to choose the following coefficients
\begin{align}
	C_1^+ = \frac {e^{-i \frac{\delta_+}2}}{\sqrt{k-k_z}} \ ,\ \ 
	C_2^+ = \frac {e^{+i \frac{\delta_+}2}}{\sqrt{k+k_z}}\ ,\ \ \  
	\delta_+ = \tan^{-1} \left(\frac{k_y}{k_x}\right)\ . 
\end{align}
Taking into account the normalization $C_1^* C_1 + C_2^* C_2 = 1$,
finally I find: 
\begin{align}
	C_1^+ = \sqrt{\frac{k+k_z}{2k}} e^{-i \frac{\delta_+}2} \ ,\ \ 
	C_2^+ = \sqrt{\frac{k-k_z}{2k}} e^{+i \frac{\delta_+}2}\ ,\ \ \  
	\delta_+ = \tan^{-1} \left(\frac{k_y}{k_x}\right)\ . 
\end{align}
Analogously, for ${\cal E}=-k$ we can write
\begin{align}
	C_1^- = \sqrt{\frac{k-k_z}{2k}} e^{-i \frac{\delta_-}2} \ ,
	\ \ C_2^- = -\sqrt{\frac{k+k_z}{2k}} e^{+i \frac{\delta_-}2}\ ,
	\ \ \ \delta_- = \tan^{-1} \left(\frac{k_y}{k_x}\right)\ .
\end{align}
Using $\delta = \delta_+ = \delta_- = \tan^{-1} (k_y/k_x)$, one gets
\begin{align}
	u_{+} (\textbf{k}) = \left(\begin{array}{c} C_1^+ \\ \\ C_2^+ \end{array}\right)
	= \left(\begin{array}{c} \sqrt{\frac{k+k_z}{2k}}\, e^{-\frac{i}{2} \delta}
		\\ \\ \sqrt{\frac{k-k_z}{2k}}\, e^{+\frac{i}{2} \delta} \end{array}\right)\ ,
	\ \ \ u_{-} (\textbf{k}) = \left(\begin{array}{c} C_1^- \\ \\ C_2^- \end{array}\right)
	= \left(\begin{array}{c} \sqrt{\frac{k-k_z}{2k}}\, e^{-\frac{i}{2} \delta}
		\\ \\ -\sqrt{\frac{k+k_z}{2k}}\, e^{+\frac{i}{2} \delta} \end{array}\right)\ .
	\label{eq.:wfs_1}
\end{align}
Let us introduce the following quantities
\begin{align}
	A (\textbf{k} ) = \sqrt{\frac{k+k_z}{2k}}\ ,\ \ B ({\textbf{k}}) = \sqrt{\frac{k-k_z}{2k}}\ ,
	\ \ C ({\textbf{k}}) = e^{-\frac{i}{2} \delta}\ ,\ \ D ({\textbf{k}}) = e^{+\frac{i}{2} \delta}\ ,
\end{align}
which allow us to rewrite Eq.~(\ref{eq.:wfs_1}) as
\begin{align}
	u_{+} ({\textbf{k}}) = \left(\begin{array}{c} A ({\textbf{k}}) C ({\textbf{k}}) \\ \\ B ({\textbf{k}}) D ({\textbf{k}}) \end{array}\right)\ ,
	\ \ \ u_{-} ({\textbf{k}}) = \left(\begin{array}{c} B ({\textbf{k}}) C ({\textbf{k}}) \\ \\ - A ({\textbf{k}}) D ({\textbf{k}}) \end{array}\right)\ .
\end{align}
Their partial derivatives are
\begin{align}
\label{eq:partial_derivatives_A}
	\frac{\partial A}{\partial k_x} = - \frac{k_z k_x}{4 A k^3}\ ,
	\ \ \ \frac{\partial A}{\partial k_y} = - \frac{k_z k_y}{4 A k^3}\ ,
	\ \ \ \frac{\partial A}{\partial k_z} = \frac{(k_x^2 + k_y^2)}{4 A k^3}\ ;\\
	\label{eq:partial_derivatives_B}
	\ \ \ \ \frac{\partial B}{\partial k_x} = \frac{k_z k_x}{4 B k^3}\ ,
	\ \ \ \frac{\partial B}{\partial k_y} = \frac{k_z k_y}{4 B k^3}\ ,
	\ \ \ \frac{\partial B}{\partial k_z} = - \frac{(k_x^2 + k_y^2)}{4 B k^3}\ .
\end{align}
and
\begin{align}
	\frac{\partial C}{\partial k_x} = \frac{i k_y C}{2 (k_x^2 + k_y^2)}\ ,
	\frac{\partial C}{\partial k_y} = \frac{- i k_x C}{2 (k_x^2 + k_y^2)}\ ,
	\frac{\partial C}{\partial k_z} = 0\ ;\\
	\frac{\partial D}{\partial k_x} = \frac{- i k_y D}{2 (k_x^2 + k_y^2)}\ ,
	\frac{\partial D}{\partial k_y} = \frac{i k_x D}{2 (k_x^2 + k_y^2)}\ ,
	\frac{\partial D}{\partial k_z} = 0\ .
\end{align}
Consequently, for $ k_x $ we have

\begin{align}
	\nonumber
	\frac{\partial u_{+} ({\textbf{k}})}{\partial k_x} &=
	\left(\begin{array}{c} \frac{\partial A}{\partial k_x} C + A \frac{\partial C}{\partial k_x} \\ 
 	\frac{\partial B}{\partial k_x} D + B \frac{\partial D}{\partial k_x} \end{array}\right) = 
	\left(\begin{array}{c} - \frac{k_z k_x}{4 A k^3} C + A \frac{i k_y C}{2 (k_x^2 + k_y^2)} \\ 
	\frac{k_z k_x}{4 B k^3} D - B \frac{i k_y D}{2 (k_x^2 + k_y^2)}  \end{array}\right)\\
	\nonumber
	A_{+}^{x} &= i \langle u_{+} | \nabla_{k_x} u_{+} \rangle = \frac{- k_y k_z}{2 k (k_x^2 + k_y^2)}\
\end{align}

Similarly for $ k_y $ and $ k_z $

\begin{align}
	\nonumber
	\frac{\partial u_{+} ({\textbf{k}})}{\partial k_y} &=
	\left(\begin{array}{c} \frac{\partial A}{\partial k_y} C + A \frac{\partial C}{\partial k_y} \\ 
	\frac{\partial B}{\partial k_y} D + B \frac{\partial D}{\partial k_y} \end{array}\right) =
	\left(\begin{array}{c} - \frac{k_z k_y}{4 A k^3} C - A \frac{i k_x C}{2 (k_x^2 + k_y^2)}\\ 
	\frac{k_z k_y}{4 B k^3} D + B \frac{i k_x D}{2 (k_x^2 + k_y^2)}  \end{array}\right)\\
	A_{+}^{y} &= i \langle u_{+} | \nabla_{k_y} u_{+} \rangle = \frac{k_x k_z}{2 k (k_x^2 + k_y^2)}\\
	\nonumber
	\frac{\partial u_{+} ({\textbf{k}})}{\partial k_z} &=
	\left(\begin{array}{c} \frac{\partial A}{\partial k_z} C + A \cancelto{0}{\frac{\partial C}{\partial k_z}}
		\\ \\ \frac{\partial B}{\partial k_z} D + B \cancelto{0}{\frac{\partial D}{\partial k_z}} \end{array}\right) =
	\left(\begin{array}{c} \frac{(k_x^2 + k_y^2)}{4 A k^3} C \\ - \frac{(k_x^2 + k_y^2)}{4 B k^3} D \end{array}\right)\\
	A_{+}^{z} &= i \langle u_{+} | \nabla_{k_z} u_{+} \rangle = \frac{(k_x^2 + k_y^2)}{4 k^3} - \frac{(k_x^2 + k_y^2)}{4 k^3} = 0\ .
\end{align}
and the same for $ u_{-} $
%\begin{align}
%\frac{\partial u_{+} ({\textbf{k} k})}{\partial k_x} =
%\begin{pmatrix}
%\frac{\partial B}{\partial k_x} D + B \frac{\partial D}{\partial k_x}\\
%\frac{\partial C}{\partial k_x} A + C \frac{\partial A}{\partial k_x}
%\end{pmatrix}=
%\begin{pmatrix}
%- \frac{k_z k_x}{4 B k^3} D + A \frac{i k_y C}{2 (k_x^2 + k_y^2)}\\
%\frac{k_z k_x}{4 B k^3} D - B \frac{i k_y D}{2 (k_x^2 + k_y^2)}
%\end{pmatrix},
%\ \ A_{+}^{x} = i \langle u_{+} | \nabla_{k_x} u_{+} \rangle = \frac{- k_y k_z}{2 k (k_x^2 + k_y^2)}\\
%\frac{\partial u_{+} ({\textbf{k} k})}{\partial k_x} =
%\begin{pmatrix}
%\frac{\partial A}{\partial k_x} C + A \frac{\partial C}{\partial k_x}\\
%\frac{\partial B}{\partial k_x} D + B \frac{\partial D}{\partial k_x}
%\end{pmatrix}=
%\begin{pmatrix}
%- \frac{k_z k_x}{4 A k^3} C + A \frac{i k_y C}{2 (k_x^2 + k_y^2)}\\
%\frac{k_z k_x}{4 B k^3} D - B \frac{i k_y D}{2 (k_x^2 + k_y^2)}
%\end{pmatrix},
%\ \ A_{+}^{x} = i \langle u_{+} | \nabla_{k_x} u_{+} \rangle = \frac{- k_y k_z}{2 k (k_x^2 + k_y^2)}\\
%\frac{\partial u_{+} ({\textbf{k} k})}{\partial k_x} =
%\begin{pmatrix}
%\frac{\partial A}{\partial k_x} C + A \frac{\partial C}{\partial k_x}\\
%\frac{\partial B}{\partial k_x} D + B \frac{\partial D}{\partial k_x}
%\end{pmatrix}=
%\begin{pmatrix}
%- \frac{k_z k_x}{4 A k^3} C + A \frac{i k_y C}{2 (k_x^2 + k_y^2)}\\
%\frac{k_z k_x}{4 B k^3} D - B \frac{i k_y D}{2 (k_x^2 + k_y^2)}
%\end{pmatrix},
%\ \ A_{+}^{x} = i \langle u_{+} | \nabla_{k_x} u_{+} \rangle = \frac{- k_y k_z}{2 k (k_x^2 + k_y^2)}\\
%\end{align}

\section*{Vector potential and Berry curvature}
Let's recall that the standard \emph{cross product} generalizes for complex valued vectors under the form
\begin{equation}\label{eq:gen cross product}
v\times w = 
\begin{pmatrix}
v_{1}\\ v_{2} \\ v_{3}
\end{pmatrix}
\times
\begin{pmatrix}
w_{1}\\ w_{2} \\ w_{3}
\end{pmatrix}=
\begin{pmatrix}
\overline{v_{2}w_{3}}-\overline{w_{2}v_{3}}\\ \overline{w_{1}v_{3}}-\overline{v_{1}w_{3}} \\ \overline{v_{1}w_{2}}-\overline{w_{1}v_{2}}
\end{pmatrix}
\end{equation}

In the end, the terms that I got for the vector potential $ \vec{A} $ are

\begin{equation}
\label{eq:Berry_connection}
A = 
\begin{pmatrix}
A_{++} & A_{+-}\\
A_{-+} & A_{--}
\end{pmatrix}=
\begin{bmatrix}
\begin{pmatrix}
\frac{- k_y k_z}{2 k (k_x^2 + k_y^2)}\\
\frac{k_x k_z}{2 k (k_x^2 + k_y^2)}\\
0
\end{pmatrix}
&
\begin{pmatrix}
-\frac{1}{\varphi}\frac{k_y}{(k_x^2 + k_y^2)}+i\varphi\frac{k_xk_z}{4k^3}\\
\frac{1}{\varphi}\frac{k_x}{(k_x^2 + k_y^2)}+i\varphi\frac{k_yk_z}{4k^3}\\
-i\frac{\varphi}{r}
\end{pmatrix}
\\
\vspace{0.05cm}\\
\begin{pmatrix}
-\frac{1}{\varphi}\frac{k_y}{(k_x^2 + k_y^2)}-i\varphi\frac{k_xk_z}{4k^3}\\
\frac{1}{\varphi}\frac{k_x}{(k_x^2 + k_y^2)}-i\varphi\frac{k_yk_z}{4k^3}\\
i\frac{\varphi}{r}
\end{pmatrix}
&
\begin{pmatrix}
\frac{k_y k_z}{2 k (k_x^2 + k_y^2)}\\
\frac{- k_x k_z}{2 k (k_x^2 + k_y^2)}\\
0
\end{pmatrix}
\end{bmatrix}
\end{equation}
where I defined $ \varphi $ as
\[\varphi=\frac{2k}{\sqrt{k_x^2+k_y^2}} \] 

Now, remembering eq.~\eqref{eq:gen cross product}, for what concerns the so called \textit{Berry Curvature}, we have

\begin{equation}
\label{eq:Berry_curvature}
\Omega = \nabla \times A =
\begin{pmatrix}
\Omega_{++} & \Omega_{+-}\\
\Omega_{-+} & \Omega_{--}
\end{pmatrix}=
\begin{pmatrix}
\nabla \times A_{++} & \nabla \times A_{+-}\\
\nabla \times A_{-+} & \nabla \times A_{--}
\end{pmatrix}=
\begin{bmatrix}
\begin{pmatrix}
-\frac{k_x}{k}\\
-\frac{k_y}{k}\\
-\frac{k_z}{k}\\
\end{pmatrix}
&
\begin{pmatrix}
\Omega_{+-}^x\\
\Omega_{+-}^y\\
\Omega_{+-}^z
\end{pmatrix}
\\
\vspace{0.05cm}\\
\begin{pmatrix}
\Omega_{+-}^x\\
\Omega_{+-}^y\\
\Omega_{+-}^z
\end{pmatrix}^{\!\!\dagger}
&
\begin{pmatrix}
\frac{k_x}{k}\\
\frac{k_y}{k}\\
\frac{k_z}{k}\\
\end{pmatrix}
\end{bmatrix}
\end{equation}

\noindent where I defined 
\begin{subequations}\label{eq:curvature_pm}
\begin{align}
	\Omega_{+-}^x = -\frac{4ik_y}{\sqrt{k_x^2+k_y^2}(k_x^2+k_y^2)}+\frac{2ik_y}{4\sqrt{k_x^2+k_y^2}}\left(\frac{k^2-2k_z^2}{k^4}\right)\\
	\Omega_{+-}^y=
	-\frac{4ik_x}{\sqrt{k_x^2+k_y^2}(k_x^2+k_y^2)}-\frac{2ik_x}{4\sqrt{k_x^2+k_y^2}}\left(\frac{k^2-2k_z^2}{k^4}\right)\\
	\Omega_{+-}^z= \frac{k_z^2}{k^2\sqrt{k_x^2+k_y^2}}+\frac{\sqrt{k_x^2+k_y^2}(k_x^2+k_y^2)}{k^2(k_x^2+k_y^2)^2}-i\sqrt\frac{k_x+k_y}{k_x-k_y}\frac{k_z}{k^3}
\end{align}
\end{subequations}
and the missing terms in the matrix \eqref{eq:Berry_curvature} are the complex conjugate of the terms in \eqref{eq:curvature_pm}.

Some similar calculations are performed also in \cite{tannor2006Introduction} at p. 206.
\nocite{*}

\section{2D Model}
\label{sec:2d-model}

The model that we consider here is only the real projection of \eqref{matr:complex_hamiltonian}, that is
\begin{equation}
\mathcal{H}=
\begin{bmatrix}
k_z & k_x \\
k_x & -k_z \\
\end{bmatrix}
\end{equation}

the corresponding eigenvalue problem is once again
\begin{align}
\begin{bmatrix}
k_z & k_x \\ 
k_x & -k_z
\end{bmatrix}
\begin{bmatrix}
C_1 \\
C_2
\end{bmatrix} 
=
\lambda\begin{bmatrix}
C_1 \\
C_2
\end{bmatrix} 
.
\end{align}
Then we have
\begin{align}
\lambda_\pm = \pm \sqrt{k_x^2 + k_y^2} = \pm |\textbf{k}| = \pm k
\end{align}

In the end we have again that the relative eigenvectors are given by
\begin{equation}
u_+(k) = 
\begin{pmatrix}
\sqrt{\frac{k+k_z}{2k}} \\
\sqrt{\frac{k-k_z}{2k}}
\end{pmatrix}
=
\begin{pmatrix}
A(\mathbf{k}) \\
B(\mathbf{k})
\end{pmatrix},
\qquad 
u_-(k) = 
\begin{pmatrix}
\sqrt{\frac{k-k_z}{2k}} \\
-\sqrt{\frac{k+k_z}{2k}}
\end{pmatrix}
=
\begin{pmatrix}
B(\mathbf{k}) \\
-A(\mathbf{k})
\end{pmatrix}
\end{equation}

Recalling then the partial derivatives, given already in eqs. \eqref{eq:partial_derivatives_A},~\eqref{eq:partial_derivatives_B}, we can easily recall the connection and the curvature.

\begin{equation}
\label{eq:Berry_connection_2d}
A = 
\begin{pmatrix}
A_{++} & A_{+-}\\
A_{-+} & A_{--}
\end{pmatrix}=
\begin{bmatrix}
\begin{pmatrix}
0\\
0
\end{pmatrix}
&
\begin{pmatrix}
\frac{k_z}{2k^2}\\
-\frac{k_x}{2k^2}
\end{pmatrix}
\\
\vspace{0.05cm}\\
\begin{pmatrix}
-\frac{k_z}{2k^2}\\
\frac{k_x}{2k^2}
\end{pmatrix}
&
\begin{pmatrix}
0\\
0
\end{pmatrix}
\end{bmatrix}
\end{equation}
And we can now evaluate the \textit{Curvature} as $ \Omega=\nabla\times A $. Notice that, being the (vector) components of $ A $ only a 2 component vector, we made the choice of embedding in a 3 dimensional space in the following way:
\[
u_+(k) = 
\begin{pmatrix}
\sqrt{\frac{k+k_z}{2k}} \\
\sqrt{\frac{k-k_z}{2k}}
\end{pmatrix}
\Rightarrow
\begin{pmatrix}
\sqrt{\frac{k+k_z}{2k}} \\
0 \\
\sqrt{\frac{k-k_z}{2k}}
\end{pmatrix}
,
\qquad 
u_-(k) = 
\begin{pmatrix}
\sqrt{\frac{k-k_z}{2k}} \\
-\sqrt{\frac{k+k_z}{2k}}
\end{pmatrix}
\Rightarrow
\begin{pmatrix}
\sqrt{\frac{k-k_z}{2k}} \\
0 \\
-\sqrt{\frac{k+k_z}{2k}}
\end{pmatrix}
\]
In the end, what we get, is
\begin{equation}
\label{eq:Berry_curvature_2d}
\Omega = \nabla \times A =
\begin{pmatrix}
\Omega_{++} & \Omega_{+-}\\
\Omega_{-+} & \Omega_{--}
\end{pmatrix}=
\begin{bmatrix}
\begin{pmatrix}
0\\
0\\
0
\end{pmatrix}
&
\begin{pmatrix}
0\\
\frac{1}{2k^2}\\
0
\end{pmatrix}
\\
\vspace{0.05cm}\\
\begin{pmatrix}
0\\
-\frac{1}{2k^2}\\
0
\end{pmatrix}
&
\begin{pmatrix}
0\\
0\\
0
\end{pmatrix}
\end{bmatrix}
\end{equation}

\section{Proj Model}
\label{sec:proj-model}
These results are taken by only putting whatever is in Section \ref{sec:3d-model} and putting $ k_y=0 $.

\begin{equation}
\label{eq:Berry_connection_proj}
A = 
\begin{pmatrix}
A_{++} & A_{+-}\\
A_{-+} & A_{--}
\end{pmatrix}=
\begin{bmatrix}
\begin{pmatrix}
0\\
\frac{k_z}{2 k k_x}\\
0
\end{pmatrix}
&
\begin{pmatrix}
i\frac{k_z}{2k^2}\\
\frac{1}{2k}\\
-i\frac{2}{k_x}
\end{pmatrix}
\\
\vspace{0.05cm}\\
\begin{pmatrix}
-i\frac{k_z}{2k^2}\\
\frac{1}{2k}\\
i\frac{2}{k_x}\end{pmatrix}
&
\begin{pmatrix}
0\\
-\frac{k_z}{2 k k_x}\\
0
\end{pmatrix}
\end{bmatrix}
\end{equation}

Instead the Berry curvature becomes

\begin{equation}
\label{eq:Berry_curvature_proj}
\Omega = \nabla \times A =
\begin{pmatrix}
\Omega_{++} & \Omega_{+-}\\
\Omega_{-+} & \Omega_{--}
\end{pmatrix}=
\begin{bmatrix}
\begin{pmatrix}
-\frac{k_x}{k}\\
0\\
-\frac{k_z}{k}\\
\end{pmatrix}
&
\begin{pmatrix}
0\\
-\frac{4i}{k_x^2}-\frac{i}{2}\left(\frac{k^2-2k_z^2}{k^4}\right)\\
\frac{k_z^2+1}{k^2k_x}-i\frac{k_z}{k^3}
\end{pmatrix}
\\
\vspace{0.05cm}\\
\begin{pmatrix}
0\\
\frac{4i}{k_x^2}+\frac{i}{2}\left(\frac{k^2-2k_z^2}{k^4}\right)\\
\frac{k_z^2+1}{k^2k_x}+i\frac{k_z}{k^3}
\end{pmatrix}
&
\begin{pmatrix}
\frac{k_x}{k}\\
0\\
\frac{k_z}{k}\\
\end{pmatrix}
\end{bmatrix}
\end{equation}

\section{Semiclassical (topological-geometric-Berry) force}
Now that we have our Connection we're finally able to plug into a suitable Lagrangian and derive the semiclassical equations of motion. Following \cite{krishna2007path} (Section III.A and more explicitly Appendix A), we define an effective magnetic field of the form
\begin{equation}\label{eq:effective magnetic field}
B_{mn} = \nabla_k \times A_{mn} -i\left(A\times A\right)_{mn}
\end{equation}
The first term is the magnetic field of standard electromagnetism. The only tricky part is the second one. A suitable expression for the last term of Eq.~\eqref{eq:effective magnetic field} is given, for example, in \cite{shindou2005noncommutative} in Eq.~7
\begin{equation}
B_{mn}= i\sum_{l=1}^{2}\left( \Braket{\frac{\partial u_{m}}{\partial k_{1}}|u_l}\Braket{u_l|\frac{\partial u_{m}}{\partial k_{2}}} - \Braket{\frac{\partial u_{m}}{\partial k_{2}}|u_l}\Braket{u_l|\frac{\partial u_{m}}{\partial k_{1}}} \right)
\end{equation} 

