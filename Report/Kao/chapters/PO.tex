\chapter{Periodic orbit localization in the phase space: Generalized normal mode analysis}

The idea of this project is once again to \textbf{calculate geometric phase effects in molecular systems by means of (real time) path integral methods}. 
Tough in principle exact, Path Integral merhods are in general considered to be a hard numerical challenge that leads often to numerical instabilities (namely, the sign error). 
Therefore, in the general case, the path integral idea is preserved in a  are perturbative fashion \sidenote{By "perturbative" we mean here the standard variational calculus.} and is used as an analytical technique to derive equations of motions for a semi-classical description of the system. 
Anyone who intends to use path integrals for numerical calculations, then the path integrals result a semi-classical approach to characterize the dynamics of the corresponding quantum mechanical system. It means, one needs to solve classical equations of motion, then using the trajectories to obtain the desired semiclassical quantities that are derived form the corresponding quantum mechanical picture.

\begin{figure}[htbp!]
	\includegraphics[width=\textwidth]{../Im/Periodic_orbit_scheme}
	\caption{Scheme}
	\label{fig:scheme}
\end{figure}

The two most fundamental phenomena in which we can trace the presence (and effects) of the geometric phase (?) in molecular systems are essentially of two families:
\begin{itemize}
	\item molecular collisions (scattering processes)
	\item bound state motions (rotation-vibration)
\end{itemize}
As of common knowledge in the literature, isolating the effects of the GP is already by itself a not trivial task; 
we need therefore to focus on possible ways/mechanisms of detecting the fingerprint of the GP.
For what concern the scattering processes, the cross section is the basic quantity that contains every information about the dynamics. There is an available path integral technique, the classical S-matrix theory (S-matrix: scattering matrix), that is an efficient method to incorporate the quantum effect into the classical dynamics. \sidenote{It has an improved version, the
Initial Value Representation \cite{miller2001semiclassical,lagoimplementation}} \footnote{To the best of the knowledge of the author, there is no studies with S-matrix theory on geometric phase effects. Unfortunately, the implementation of molecular collision processes is not trivial, that is why we focused essentially on bound state motions.} 
In the remaining part of this report I will focus on the bound state systems.

\section{Level density for bound state systems}

In the following sections I discuss the concept of level density as a suitable quantity that may measure the impact of geometric phase on bound state molecular motions. Density of states (\textsc{dos}) is essential, it plays a central role in statistical physics. 
The vibrational spectrum would be also a good choice to measure the the effect of geometric phase, but the level density is more general, it is related to many other quantities (even for vibrational spectrum).

The quantum mechanical sum of states (or number of states) is given by the following formula \sidenote[][0.8cm]{Here by $ G $ we denoted the well known Green function}
\begin{equation}\label{eq:dos}
N(E) = \sum \delta (E-E_i) = \mathrm{Tr} G(E,q_i,q_f) 
\end{equation}

This formula has a simple meaning: we need to count the quantum states, that is related to the trace of the energy dependent Green-function. The classical sum of states (or number of states) for a system that contains N atoms can be calculated as the phase space volume (the energy shell in phase space occupied by the molecular system):
\begin{equation}
N(E) = \frac{1}{h^{3N}}\int\dots\int\delta(H(\mathbf{q},\mathbf{p})-E)\diff \mathbf{q}_1 \dots \diff \textbf{q}_{3N}\diff \textbf{p}_1\dots \diff \textbf{p}_{3N} 
\end{equation}
The density of states (or level density) is given by the derivative of the sum of states:

\begin{equation}
\rho(E)=\dfrac{\diff N(E)}{\diff E}
\end{equation}

\section{General methodologies to calculate level density}
The general approaches for evaluating the level density go as follows: first come a study on the normal mode analysis and then comes the hypothesis of the rigid rotor. 
A simple and straightforward formula has been already developed in the literature and is by now standard textbook material. Yet, the formula is computationally intensive and a direct count method to calculate the sum and density of states at defined energies of a harmonic oscillator is requested. 
The harmonic oscillator model assumes that the difference between successive vibrational energy levels remains constant. 
In real oscillators, this difference decreases as the vibrational excitation increases. 
These anharmonic effects increase both the sum and density of states.
An accurate calculation of this rate constant requires that all vibrational anharmonicity and vibrational/rotational coupling be included in calculating the sum and density. 

The vibrational energy levels of the $ i $th vibrational mode can be represented by a power series:
\begin{equation}
E_i(n_i) = \hbar\omega_i\left( \frac{1}{2}+n_i\right)-\hbar\omega_i\chi_i\left( \frac{1}{2}+n_i\right)^2 + \left[\text{higher order terms}\right]
\end{equation}
where $ E_i(n_i) $ is the energy of the $ i $th mode at the $ n $th  vibrational level, $ \omega_i $ is the harmonic vibrational
frequency, and $ \chi_i $ is the anharmonicity constant for the $ i $th mode.
\sidenote{For a general polyatomic system the 
coupling between rotational and vibrational degrees of freedom are neglected.}

\section{Gutzwiller's trace formula}
Gutzwiller's trace formula provides an elegant path integral approach to calculate the central quantity of statistical
physics, the level density. Unfortunately, the application of this trace formula is not straightforward inspite of the pure classical treatment of the quantum mechanical system. 
The main problem, as we will see, of the classical simulations is the localization of the periodic orbits in a multidimensional system. 
\sidenote{At the best knowledge of the authors, there is no available method that can be used as an efficient and practical procedure to localize periodic orbits in bigger systems (bigger than 3-atoms). In general case the random shooting method is used to detect closed trajectories (periodic orbits) what is really inefficient and brute force solution of the problem}.
Martin Gutzwiller discovered that only the classical periodic orbits give contribution to the quantum level density using the path integral approach for bound state systems. \cite{lubcke2001gutzwiller,malik1999trace, vogl2017semiclassics}
Gutzwiller's trace formula can be considered as a multidimensional extension of the usual semiclassical WKB quantization procedure. According to Gutzwiller, the quantum level density $ \rho(E) $ can be split into the classical level density $ \tilde{\rho}(E) $ plus a term that is resulted from the quantum fluctuations:
\begin{equation}
\rho(E)=\tilde{\rho}(E)+\dfrac{1}{\hbar\pi}\Re\left\{ \sum_a T_aM_a e^{\frac{i}{h}S_a(E)} \right\}
\end{equation}
where $ a $ is the index of the periodic orbits\sidenote{A really big problem is already evident here: $ a $ denotes \emph{all} the periodic orbits of the system. As we know, finding periodic orbits is already a non trivial task at all, even in trivial supersymmetric systems. Checking then that they're \emph{all and only} the periodic orbits of the system is something that I personally consider almost impossible. An approximated truncated sum is necessary and the convergence can be checked only empirically}, $ T_a $ is the period (traversal time) of the given orbit, $ M_a $ is the determinant of the monodromy (or stability) matrix and $ S_a (E) $ is the classical reduced action of the corresponding periodic orbit: $ S_a (E)=\oint p_a\diff q_a $
This fluctuation part can be calculated from the periodic orbits of the classical system. 

Therefore, the main purpose of this project was to develop a simple and compact approach for
the localization of the periodic orbits to pave the way for the application of the
Gutzwiller's trace formula in molecular systems.

\section{Periodic orbit localization to compute level density}
The general approach goes as follows:
\begin{itemize}
	\item Take a general molecule and its respective PES \sidenote{In our example we took the PES of $ H_2O $, described for example in \cite{gonzalez2011flexible}}
	\item solves numerically the equation of motion of an arbitrary polyatomic system at a given energy and total angular momentum. 
	\item collect step by step the Kinetic and potential energy \sidenote{the rotational and translational degree of freedom needs to be singled out by recurring, for example, to the Jacoby coordinates, described for example in \cite{islampour2006molecular}}
	\item Fourier transform the time evolution of the Kinetic Energy
	\item the peaks in the frequency spectrum are the normal modes $ \omega_i $ that we need
\end{itemize}

In figure \ref{fig:peaks_energy_spectrum} we can observe 3 peaks in the kinetic energy spectrum of the water. Our intuition suggests that these peaks somehow relate to vibrational modes of the water molecule.
\begin{figure}
	\includegraphics{Im/PO/Peaks}
	\caption[KE time evolution]{time evolution of the Kinetic average of the pseudo-particles}
	\label{fig:peaks_energy_spectrum}
\end{figure}

According to the harmonic picture, the vibrational frequencies are independent of the total
energy of the molecule, but in an anharmonic system they must depend on the total energy
and angular momentum. As the total energy is increased, the peaks slowly change their
position (as it is expected due to the anharmonicity). After a while, when we gave enough
energy, there will appear new peaks in the spectrum. This means, that coupling between
different modes (vibration-vibration, and vibration-rotation) are rising and causing overtones
and combinations bands in the spectrum. These phenomena can be observed in the normal
vibrational spectrum as well (Fourier transform of the velocity-velocity autocorrelation function), not only in the kinetic energy spectrum. But the kinetic energy spectrum is more
suitable for our purpose. For the dynamical analysis of the spectrum one should write the
kinetic energy of a general system in the usual form:
\begin{equation}
T(t) = \frac{1}{2}\sum_{\alpha}m_{\alpha}\dot{q}_{\alpha}^2
\end{equation}
where $ \alpha = 1,\dots ,3N $ is the index of degrees of freedom and $ \dot{a} $ is the velocity.

Now, we assume that the time dependence of the kinetic energy is know (e.g. from the
numerical solution of the equations of motion)
\begin{align}
q_{\alpha}(t) = q_{\alpha}^0+\sum_{i}^M A_{i\alpha}cos(\omega_i t)\\
\dot{q}_{\alpha}(t) = -\sum_{i}^M A_{i\alpha}\omega_i sin(\omega_i t)
\end{align}
If $ M $ in the summation is equal with $ 3N-6 $ (the number of vibrational degrees of freedom),
it results the well known \emph{ansatz} for normal mode analysis. The usual normal mode analysis
is derived from a static picture: the real dynamical behavior of the system is neglected. I
mean here, we are doing an approximation in the standard normal mode analysis (a Taylor-
series expansion of the potential energy surface) before considering the real dynamics of the
molecular system, that is why the normal mode analysis is a static approximation. In this
case the real dynamics is truncated, a large body of the information about the dynamics
is lost. I would like to provide an opposite approach, an extended normal mode analysis:
first we should consider the exact dynamics (by numerical propagation), then we may use an
approximation to extract the natural frequencies of the molecular system.
\begin{align}
	T(t) &= \frac{1}{2}\sum_{\alpha}m_{\alpha} \left[-\sum_{i}^M A_{i\alpha}\omega_i sin(\omega_i t)\right]^2 \\
	&= \frac{1}{2}\sum_{\alpha}m_{\alpha} \left[-\sum_{i}^M A_{i\alpha}^2\omega_i^2 sin^2(\omega_i t)+ 
					2\sum_{i}\sum_{i<j}A_{j\alpha}A_{i\alpha}\omega_i\omega_j sin(\omega_j t)sin(\omega_i t) \right]
\end{align}
Now, we take the Fourier transform the kinetic energy. This results an exact formula with
due to our simple \emph{ansatz}:
\begin{align}
\mathcal{F}\left\{T(t) \right\} &= \mathcal{T}(\omega) = \sqrt{\dfrac{\pi}{32}} \sum_{\alpha} m_{\alpha}
\Big\{\sum_{i}A_{i\alpha}^2\omega_i^2(\delta(\omega-2\omega_i))+\nonumber\\
&+ 2\sum_{i}\sum_{i<j}A_{j\alpha}A_{i\alpha}\omega_i\omega_j (\delta(\omega_i - \omega_i +\omega) - \delta(\omega_j + \omega_i -\omega)+\nonumber\\
&+\delta(\omega - \omega_i +\omega_j)- \delta(\omega + \omega_i +\omega_j) )\Big\}\nonumber
\end{align}

First, we should test our method only for the classical density of states. In this case it
not necessary to use Monte Carlo or other expensive methods to calculate the phase space
integral, because we have independent oscillators (normal modes). Fortunately, there is an
analytical formula for the classical density of states for a collection of harmonic oscillators:
\begin{equation}
\tilde{\rho}(E)=\dfrac{E^{3N-1}}{(3N-1)!}\left(\prod_i \hbar\omega_i \right)^{-1}
\end{equation}

where $ N $ is the number of atoms and $ E $ is the total energy (the numerical value of the
Hamiltonian) and $ \omega_i $ are the normal mode frequencies. This density for a collection of
oscillators are only accurate at high energies in the equipartition limit.

All the data and the pictures below refer to the empirical potential obtained in \cite{gonzalez2011flexible}


\begin{marginfigure}
	\includegraphics{Im/PO/Picture1}
	\caption[Armonic Approximation]{The armonic approximation of the level density is trustable only in a close neighbourhood of the PES minima. }
	\labfig{fig:harmonic_approximation}
\end{marginfigure}

\begin{marginfigure}
	\includegraphics{Im/spectrum_comparison}
	\caption[Spectrum comparison]{Comparison of the spectra between the semiclassical dynamics of pseudo particles with GP (in blue) and without (in red).}
	\labfig{fig:peaks_shift}
\end{marginfigure}


\section{Future Works}
Encouraged by the results of figure \ref{fig:peaks_shift}, I think would be worth to try to finalize this small project. I believe we have good chances of producing a decent work and most of the coding has already been done.

For the case of the Mexican Hat potential, finding the periodic orbits is even possible analytically, so we can even try to evaluate the Gutzwiller trace formula.

I would set a plan of this type:
\begin{itemize}
	\item Rerun the code with the fixed MPI implementation
	\item The code is running with the term $ 1/R $, in agreement with the result we got from the conical intersection. As we know, $ 1/R $ is only a simplified version; in principle one should write $ \sigma/r $ where $ \sigma $ is a coupling constant \sidenote{I believe this coupling constant should be dependent on the type of degeneracy}. the shoft should be then proportional to $ \sigma $. 
	\item If we know some experimental values, we can try to match our value for $ \sigma $ with the one of the experiment.
	\begin{itemize}
		\item Peter is the expert of the literature. A task for him is to either to get a quantity that we can try to reproduce or try to evaluate the density of states of the mexican hat with a different approach, in order to benchmark.
		\item We need to work in parallel and check the result that match at best. It might be that our term is simply wrong or simply gives negligible effects.
	\end{itemize}
	\item Try to give an interpretation for this value of $ \sigma $ and try to evaluate the same quantity for a different system.
\end{itemize}