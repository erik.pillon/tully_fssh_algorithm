In a quantum mechanical many-body system composed of
identical particles, the interchange of any two particles is
accompanied by the overall wave function $ \Psi $ either changing
sign or not. This can be reconciled with the help of a permutation
operator $ P_{ij} $ whose role is to exchange particles $ i $ and $ j $. Applying
$ P_{ij} $ twice in succession must yield unity: $ P_{ij}^2\Psi = \Psi $, so the
eigenvalue of $ P_{ij}^2 $ is $ 1 $. It follows that $ P_{ij}\Psi = \pm\Psi  $, and
consequently $ \Psi $ must be either symmetric or antisymmetric with
respect to the exchange of identical particles. Particle spin
dictates whether $ \Psi $ is symmetric (bosons) or antisymmetric
(fermions). Integer $ s $ is associated with bosons, and odd-half-
integer $ s $ is associated with fermions, where $ s $ is the particle
spin. This relationship between a particle’s spin and its statistical
property can be taken for granted or proven through arduous
means.

Consider now the amplitude
\begin{equation}\label{eq:poliatomic_wavefunction}
\psi(x_{1}, x_{2}, \dots, x_{i}, \dots, x_{j}, \dots).
\end{equation}

The regions between commas are referred to as slots. Each slot
is associated with a particle. Convention is that particles are
numbered in ascending order from left to right; i.e., the first
slot corresponds to particle 1, the second to particle 2, and so
on. Particle numbering and subscript numbers are not to be
confused. Entries $ x_1 , x_2 $ , etc. denote \emph{descriptors} (i.e., spatial
coordinates and spin labels), but not particle \emph{identity}. The latter
is given by slot location, which is unaffected by permuting the
contents of slots. For example, $ \psi(x_{2}, x_{3}, x_{1},\dots ) $ is the amplitude
for finding particle 1 described by $ x_2 $ , particle 2 described by
$ x_3 $ , particle 3 described by $ x_1 $ , and so on.
The overall wave function $ \Psi $ can be expressed as a linear
combination of amplitudes such as the one in eq \eqref{eq:poliatomic_wavefunction}. This linear
combination spans all permutations of particle identity over the
field of descriptors. Leaving aside normalization, it can be
written
\begin{gather}
\begin{aligned}\label{eq:interchange}
\Psi = \psi(x_{1}, x_{2}, x_{3},\dots )+ c_{12}\psi(x_{2}, x_{1}, x_{3},\dots ) +\\ c_{13}\psi(x_{3}, x_{2}, x_{1},\dots )+\dots 
\end{aligned}
\end{gather}

where $ |c_{ij}|=1 $. Equation \eqref{eq:interchange} includes (though not indicated
explicitly) multiple interchanges that achieve all permutations
of particle identity over the field of descriptors. These are
expressed using products of coefficients, e.g., $ c_{23} c_{13} \psi(x_{3},x_{1} ,x_{2} ,\dots) $.
Referring to eq \eqref{eq:interchange}, when the contents of any two slots are
interchanged in each and every amplitude (of course, using the
same slot pair in each amplitude), $ \Psi $ must either change sign
or not. Consequently, for a given group of identical particles,
each $ c_{ij} $ must have the same value: $ +1 $ for bosons or $ -1 $ for
fermions. Thus,
\begin{equation}\label{eq:c_ij}
c_{ij}=(-1)^{2s}
\end{equation}
for all $ i\neq j $. Equations \eqref{eq:interchange} and \eqref{eq:c_ij} give wave functions $ \Psi $ that are
symmetric with respect to $ P_{ij} $ for integer $ s $ and antisymmetric
with respect to $ P_{ij} $ for odd-half-integer $ s $.
The above algorithm is beyond reproach in the sense that it
works. For example, electronic structure theory enlists it in the
form of Slater determinants or second quantization to construct
antisymmetrized electron wave functions. Moreover, hard and
fast rules for (identical) fermions and bosons follow. One
fermion at most can occupy a given spin-orbital, whereas any
number of bosons can occupy a given spin-orbital. Two or
more noninteracting fermions having the same spin quantum
number cannot be simultaneously present in the same place,
whereas noninteracting bosons prefer this.
What is disquieting in the above algorithm, however, is the
tacit assumption that the permutation operator $ P_{ij} $ exchanges
particles. It actually exchanges the contents of slots in a
mathematical expression. We shall see that particle exchange
carried out explicitly differs in a subtle way that yields eq \eqref{eq:c_ij}
directly.

Proofs of the relationship between a particle’s intrinsic spin
and its statistical properties have used relativistic quantum field
theory or something close to it in difficulty, 2 in contrast to the
simplicity of $ (-1)^{2s} $ . Thus, an elementary explanation has been
sought, e.g., as championed by Feynman: “The explanation is
deep down in relativistic quantum mechanics. This probably
means that we do not have a complete understanding of the
fundamental principle involved. For the moment, you will just
have to take it as one of the rules of the world.” Attempts to
rectify this situation have been varied. A proof by Sudarshan is
said by him to be “simple, physical, and intuitive, but still not
completely free from the complications of relativistic quantum
field theory”. Though easier than other proofs, it is not easy.
A proof by Berry and Robbins does not use relativistic quantum
field theory, but its topological arguments make it at least as
challenging as the field theory proofs. Derivations germane to
the nonrelativistic regime have been criticized for reasons
such as requiring that quantum mechanics acquires an additional
postulate, assigning shape to a point particle, overlooking the
fact that $ SO(3) $ is not a simply connected topological space,
disregarding the light cone, mixing homotopy groups, and so
on.

Feynman revisited this matter in his Dirac Memorial Lecture,
but without an elementary explanation. As an afterthought he
mentioned parlor tricks brought to his attention by Finkelstein.
For example, a belt is used to show that a $ 4\pi $ twist can be
undone by moving one object around another while maintaining
its orientation relative to the observation frame. On the other
hand, Hilborn argues that such demonstrations are irrelevant: 
“... this analogy is not an explanation. Nowhere does the spin
of the object enter the discussion nor is it clear what the twist
in the constraint has to do with the change in sign of the
fermion’s wave function.” Duck and Sudarshan side with
Hilborn, arguing that belts and the like have no bearing on
exchanging identical fundamental particles because the particles
and their spinors are points.
The relativistic quantum field theory proof of the spin-statistics theorem is of course rigorous. However, this does
not mean that relativity is necessary to explain the relationship
between spin and statistics. Because relativistic quantum field
theory is correct (by definition subsuming all of nonrelativistic
physics), it will yield correct results for a given phenomenon,
regardless of whether the underlying physics is inherently
relativistic or not. In this regard, it is noteworthy that the
relationship between a particle’s spin and its statistical properties
is robust over a quite large nonrelativistic energy range, which
supports the thesis that this relationship is not inherently
relativistic. Taking this a step further, in the nonrelativistic
regime there is, for all practical purposes, no new physics to be
uncovered, so the key to understanding the $ (-1)^{2s} $ term must
be bookkeeping. Moreover, this must involve distinguishing
labels and the exchange operation, as this is all that remains.
Because spin appears at the outset in the Dirac theory, one
might question whether the symmetric treatment of space and
time is essential to the spin-statistics relationship. It will be
pointed out that such is not the case. This is not surprising, as
spin and its properties are retained in the low-velocity limit of
the Dirac equation, i.e., the (nonrelativistic) Pauli equation. 

No attempt is made at a detailed derivation. We shall not
venture beyond standard quantum mechanics, nor will spin’s
origin or deep meaning be discussed. Spin is taken as a \emph{given}.

\subsection{Formal description of exchange interaction}
To examine the exchange of two identical particles, it is necessary to determine all parameters associated with the “before” and “after” configurations. \sidenote{We already had a quite interesting discussion about this point on August 19. Actually understanding what the internal coordinates are is the main issue the solution of which could open the doors to the final relationship to exchange interaction and spin.}
\begin{marginfigure}
	\includegraphics{Im/Exc_int}
	\caption{Picture adapted from \cite{wittig2009statistics}. (a) Locations are denoted $ \vec{r}_a $ and $ \vec{r}_b $ (open circles), with $ s_a $ and $ s_b $ (not indicated) being the respective spin labels. The $ z $-axis is taken as the spin quantization axis. The parameter sets: $ \vec{r}_a s_a $ and $ \vec{r}_b,s_b $ are not each for a specific particle because the particles are indistinguishable. (b) This indicates the situation before exchange. Particles 1 and 2 (filled black circles labeled particle 1 and particle 2) are assigned to $ \vec{r}_a s_a $ and $ \vec{r}_b,s_b $ respectively. (c) This indicates the situation after exchange. This diagram is the same as (b) except that particles 1 and 2 have traded locations.}
	\label{fig:exc_int}
\end{marginfigure}

Referring to Figure \ref{fig:exc_int}a, particle locations are indicated by $ \vec{r}_a $ and $ \vec{r}_b $(open circles). Respective spin parameters (not indicated in the figure) are labeled $ s_a $ and $ s_b $. These labels are collective: they refer to magnitudes, projections, and phases. It is often convenient to use kets such as $ e^{i\delta_a}\ket{s,m_a} $, where $ \delta_a $ is the phase, which plays a central r\^ole.
The pairs $ \vec{r}_a s_a $ and $ \vec{r}_b,s_b $ are defined relative to a reference frame that we shall call the laboratory frame. The placement of this frame is not important. In Figure \ref{fig:exc_int}, its origin is where the dashed lines intersect and its $ z $-axis can be taken as the spin quantization axis. This choice is arbitrary; it cannot affect the result. Keep in mind that $ \vec{r}_a s_a $ and $ \vec{r}_b,s_b $ are not each associated with a specific particle. Each is associated with both particles because of indistinguishability. All that can be known is that one of the particles is at $ \vec{r}_a $ with spin parameters $ s_a $, whereas the other is at $ \vec{r}_b $ with spin parameters $ s_b $. It is not possible to know which particle is at a given location.

Particles 1 and 2 are now assigned to $ \vec{r}_a s_a $ and $ \vec{r}_b,s_b $, respectively, as indicated in Figure \ref{fig:exc_int}b. Particle 1 is at $ \vec{r}_a $ in spin state  $ e^{i\delta_a}\ket{s,m_a} $, whereas particle 2 is at $ \vec{r}_b $ in spin state $ e^{i\delta_b}\ket{s,m_b} $. It is convenient to set the phases $ \delta_a $ and $ \delta_b $ each equal to zero in this before configuration. 
One can now envision exchange as the replacement of Figure \ref{fig:exc_int}b by Figure \ref{fig:exc_int}c. Particle 1 appears at $ \vec{r}_b $ in spin state $ e^{i\delta_b}\ket{s,m_b} $, whereas particle 2 appears at $ \vec{r}_a $ in spin state $ e^{i\delta_a}\ket{s,m_a} $. However, and this point is critical, the phase difference, $ \delta_a-\delta_b $, which has been set to zero before exchange, is not necessarily zero following exchange. Note that permuting the contents of slots does not take into account the possible change of $ \delta_a-\delta_b $. This is the reason $ (-1)^{2s} $ had to be imported ad hoc.

The scenario in Figure 1, despite its intuitive appeal, pays insufficient attention to angles. If exchange is accompanied by
a $ 2\pi $ displacement, this will go unnoticed in the absence of an additional diagnostic, because we do not readily 
distinguish angles that differ by integer multiples of $ 2\pi $. For example, suppose you have a model of a Cartesian 
frame in your office. You show it to a friend who leaves the room for a few minutes and then returns. He or she would 
not be able to tell whether the frame had been rotated by an integer multiple of $ 2\pi $ in their absence. Yet, for 
odd-half-integer spin and a $ 2\pi $ displacement, the phase difference, $ \delta_a-\delta_b $, would be affected profoundly, yielding the sign change described below.

\begin{figure}[htbp!]
	\includegraphics{Im/Exc_int_2}
	\caption{Particles 1 and 2 are indicated with black circles. (a) This is the situation before exchange. (b) After exchange the blue and red frames, together with particles 1 and 2, have traded locations.}
	\label{fig:exc_int_2}
\end{figure}

To address this issue, reference frames are assigned to each particle, as indicated in Figure \ref{fig:exc_int_2}. Particle 1 is located relative to its frame, and its frame is located relative to the laboratory, likewise for particle 2. The particles are placed on “$ y $-axes” for viewing convenience. This incurs no loss of generality, as the placement of these additional frames is at our discretion. These frames might appear to be redundant in the sense that they do no harm but also add no information. The reason for introducing them is that they will (eventually) prove instrumental in revealing a $ 2\pi $ displacement.

Next, $ \vec{r}_a s_a $ and $ \vec{r}_b,s_b $ are held fixed while the particle/frame combinations are exchanged. In effect, the particle/frame combinations enable 3D shapes to be transformed. A point object like an electron has no shape or size. It cannot rotate about its center-of-mass, so its orientation only has meaning with respect to a reference frame.

The theory of quantum mechanics is based on postulates that require the specification of coordinates. Spatial wave functions are not affected by adding integer multiples of $ 2\pi $ to an angular coordinate. The same is not true for odd-half-integer spin, for example, spin-$ 1/2 $. Though its spinor has no spatial wave function, rotational transformation changes its phase. Specifically, the spinor is single valued in $ 4\pi $ and changes sign in $ 2\pi $. When dealing with the exchange of identical particles, phases matter a great deal. Thus, it is necessary to distinguish integer multiples of $ 2\pi $ if we are to eliminate the sign ambiguity. This is the motivation behind frames 1 and 2.
In examining Figure \ref{fig:exc_int_2}, one is tempted to conclude that nothing new has been revealed. The scenario in Figure \ref{fig:exc_int} has been repeated, but with frames included. Figure \ref{fig:exc_int_2}, like Figure \ref{fig:exc_int}, pays insufficient attention to angles - a magic wand has converted (a) into (b).

To see what is going on, the frames are connected to one another with a tether. The r\^ole of the tether is strictly diagnostic, i.e., to answer the question: Does exchange induce a $ 2\pi $ reorientation? Quantum mechanics has nothing to do per se with the frames and tether. They are mere visual aids that enable us to identify a frame reorientation should it arise.

On a related matter, an intuitive way to introduce geometric phase uses a local reference frame to record the accumulation
of gemometric phase along a path. This arises in the electronic structure of polyatomic molecules, where conical intersections are common and the associated geometric phases play a significant role. The use of local reference frames in this context inspired the frames introduced in Figures \ref{fig:exc_int} and \ref{fig:exc_int_2}.

A simple example of geometric phase involves vector transport on a curved surface, as with the Foucault pendulum. To 
see how this works, place an arrow tangent to a sphere at its north pole, and carry it along a longitude (a geodesic) to the 
equator without twisting it relative to the longitude. Then carry the arrow along the equator through an azimuthal angle, again without twisting it. Finally, return the arrow to the north pole on a longitude without twisting it. The arrow has not been twisted on the three geodesics that comprise its path, but when it arrives back at the north pole it is displaced by the angle $ \chi $ from its original orientation. The angular changes that yield $ \chi $ occur at the apexes where the path switches from one geodesic to another. Had the trip around the equator been a complete revolution ($ \chi = 2\pi $), the arrow would have arrived back at the 
north pole appearing to have the same orientation as before it started its journey. In fact, it would have been reoriented 
by $ 2\pi $ relative to its original orientation.
In this case it is straightforward to show that the amount of
geometric phase depends only on the enclosed area of a closed
path on a (unit) sphere. In molecular electronic structure, the
most elementary geometric phases involve two diabats and two
adiabats. As only two states are involved, these systems have
spinor representations, so an enclosed area of $ 2\pi $ on a unit sphere
(in a space where the nuclear coordinates are used as parameters)
corresponds to a change of sign of the electronic wave function.
In the same spirit, the use of local frames in the present paper
is to reveal a $ 2\pi $ reorientation. Here, there is no spatial region
that is avoided when taking a closed circuit, as with conical
intersections and the Aharonov-Bohm effect. Nonetheless, the local frame serves the purpose of revealing angular
displacement.

Figure 3 shows that the tether acquires a $ 2\pi $ twist when the
particle/frame combinations are exchanged without rotating either relative to the laboratory. Following exchange, particle 1 can be assigned to $ \vec{r}_b,s_b $ with no angular displacement of frame 1 relative to the laboratory. Particle 2 is then assigned to $ \vec{r}_a,s_a $, but with a $ 2\pi $ angular displacement of its frame relative to the laboratory. (Generality is not compromised. It would be equally valid to assign frame 2 to the laboratory. Frame 1 would then acquire a $ -2\pi $ displacement and the same result would be obtained.) The $ 2\pi $ displacement changes the phase of the ket
$ \ket{s,m_a} $ that has been assigned to particle 2. When $ e^{i\vec{s}\cdot\vec{\chi}} $ ($ e^{-i2\pi s_z} $)
operates on $ \ket{s,m_a} $ it yields the term $ (-1)^{2s} $:
\begin{equation}
e^{-i2\pi s_z}\ket{s,m_a}=e^{-i2\pi m_a}\ket{s,m_a}= (-1)^{2m_a}\ket{s,m_a}=(-1)^{2s}\ket{s,m_a}
\end{equation}
For this result to be general it must be independent of which axis pair is coupled by the tether (i.e., $ x_1/x_2, y_1/y_2 $, or $ z_1/z_2 $), as well as the orientation of the tether. To ensure that this is the case, two frames were constructed and Velcro tabs were attached to each axis, as well as to the ends of a plastic tether. Variations of the scenario presented in Figure 3 were tested (this time using 3 axes rather than the 2 in Figure 3). If the tether hangs vertically, as in Figure 3, the axis pair to which it is appended does not matter. Following exchange, a $ 2\pi $ rotation in the horizontal plane recovers the original orientation. It is understood that “original orientation” is modulo $ 4\pi $, as discussed below. In other words, a $ 4\pi $ twist is the same as the identity. If the tether is now connected such that its width is vertical, it is found that, following exchange, a $ 2\pi $ rotation in the horizontal plane again recovers the original orientation. As before, this result is independent of the axis pair to which the tether is appended.
The above tests indicate that the tether width can be oriented in any of the three orthogonal directions and the original frame orientation is recovered by a single $ 2\pi $ rotation as in Figure 3c. The result is also independent of where the $ 2\pi $ rotation is taken. If the above exercise is repeated except the $ 2\pi $ rotation is about the $ x- $ or $y-$axis, the original orientation is still recovered. Thus, particle exchange includes a concomitant $ 2\pi $ reorientation. It does not matter where the $ 2\pi$ rotation is taken, so the result is independent of the choice of spin quantization axis, as required. As a final demonstration, 3 tethers were used to couple each axis pair simultaneously. The system appears quite tangled following exchange. Nonetheless, a $ 2\pi $ rotation about any axis undoes the twists in the tether, modulo $ 4\pi $. After all is said and done, nothing unusual has been unearthed. It is just a matter of taking orientation into account with the exchange operation.
Because odd-half-integer spin is single valued in $ 4\pi $, it does not matter if integer multiples of $ 4\pi $ are added. Referring to Figure 3b, were the blue frame to complete a full circuit around the red frame the result would be unity because exchange must be its own inverse. The $ 4\pi $ twist that would be incurred is consistent with this. It is pointed out in the next section (and in the Appendix) that a $ 2\pi $ twist cannot be removed by varying a parameter because the topological space $ SO(3) $ is not simply connected. However, a $ 4\pi $ twist can be removed by varying a parameter, so it is equivalent to the identity. Referring to Figure 3, note that it is impossible to exchange the positions of the red and blue frames (while maintaining no rotation relative to the laboratory) and not have the tether become twisted by an odd-integer multiple of $ 2\pi $, which (modulo $ 4\pi $) is equivalent to $ 2\pi $.
