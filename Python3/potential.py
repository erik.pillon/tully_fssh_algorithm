import numpy as np

def Potential_tully(x, A=0.01, B=1.6, C=0.005, D=1.0):
    """
    One dimensional potential used for the implementation of Tully's algorithm
    """
    V = np.zeros((2,2))
    if x>0:
        V[0,0] = A*(1-np.exp(-B*x))
    else:
        V[0,0] = -A*(1-np.exp(B*x))
    V[1,1] = -V[0,0]
    V[0,1] = C*np.exp(-D*x**2)
    V[1,0] = V[0,1]
    return V

def Potential_2D(q):
    x = q[0]
    y = q[1]
    V = np.zeros((2,2))
    V[0,0] = y
    V[1,1] = -y
    V[0,1] = x
    V[1,0] = x
    return V

def Potential_mf(q,A=0.1,B=3.0,W=0.3):
    """
    This Hamiltonian comes from the article 
    "An extension of the fssh algorithm to complex Hamiltonians"
    J. Chem. Phys. 150, 124101 (2019)
    https://doi.org/10.1063/1.5088770
    """
    x = q[0]
    y = q[1]
    theta = np.pi/2*(np.erf(B*x)+1)
    phi = W*y
    V = np.zeros((2,2),dtype=complex)
    V[0,0] = -A*np.cos(theta)
    V[1,1] = A*np.cos(theta)
    V[0,1] = A*np.sin(theta)*np.exp(1j*phi)
    V[1,0] = A*np.sin(theta)*np.exp(-1j*phi)
    return V        


# # Finds value of y for a given x using step size h 
# # and initial value y0 at x0. 
# def rungeKutta(x0, y0, x, h): 
#     # Count number of iterations using step size or 
#     # step height h 
#     n = (int)((x - x0)/h)  
#     # Iterate for number of iterations 
#     y = y0 
#     for i in range(1, n + 1): 
#         "Apply Runge Kutta Formulas to find next value of y"
#         k1 = h * dydx(x0, y) 
#         k2 = h * dydx(x0 + 0.5 * h, y + 0.5 * k1) 
#         k3 = h * dydx(x0 + 0.5 * h, y + 0.5 * k2) 
#         k4 = h * dydx(x0 + h, y + k3) 
  
#         # Update next value of y 
#         y = y + (1.0 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4) 
  
#         # Update next value of x 
#         x0 = x0 + h 
#     return y 
#




