"""
“Documentation is like sex; when it’s good, it’s very,
very good, and when it’s bad, it’s better than nothing.”
"""

# ------------------ Improvements ----------------------------------
# The code needs to be improved by taking charge properly of the nac
# Indeed, <phi_i | grad phi_j> = R_t * d_ij
# and the R_t needs to be referred to the proper state
# ------------------------------------------------------------------

import numpy as np
import numpy.linalg as LA
import numpy.random as rand
# The seed needs to be fixed only in the main algorithm...
# rand.seed(1234)

# -------------------------------------------------------------
import potential

# possible choices so far are:
# - Potential(q)
# - Potential_2D(q)
# - Potential_mf(q,A=1,B=1,W=1)
Potential = potential.Potential_tully
# -------------------------------------------------------------

# --- Some constants ---
hbar = 1
# ----------------------

class Particle:
    """ --------------------------------------------------------------------------
    This is the python class for simulating the classical movement of the particle
    The idea is that the movement of the particle is govenred by a claaaical law 
    of motion. Quantum mechanics is invoked only for the switching probability 
    criterion.
    -------------------------------------------------------------------------- """

    def __init__(self, k, pos, c1, on_surface, M0=2000, dx=0.001, dt=0.1, **kwargs):
        self.k = k
        self.pos = pos
        self.c1 = c1
        self.on_surface = on_surface
        self.M0 = M0
        self.vel = self.k/self.M0
        # c1 and c2 defines the distribution of the particles.
        # in the initial state they are supposed to be taken a Kronecker 
        # delta, even if there are not arbitary restrictions in principle
        self.c2 = np.sqrt(1-self.c1*np.conj(self.c1))
        self.dim = np.size(self.pos)
        self.dx = dx
        self.dt = dt

    def der_surface(self, surface=0):
        """
        This function is evaluating the gradient of the 
        potential energy surface.
        """
        if not (surface==0):
            surface = 1
        ev1 = LA.eigh(Potential(self.pos))[0]
        der = np.zeros(self.dim)
        for count in range(self.dim):
            ds = np.zeros(self.dim)
            ds[count] = self.dx
            ev2 = LA.eigh(Potential(self.pos+ds))[0]
            der[count] = (ev2[surface]-ev1[surface])/self.dx
        return der


    def nac(self,i,j):
        # function for the evaluation of the non adiabatic coupling vector d_ij
        # non-adiabatic vector cupling = Connection (= vector potential)
        # i and j can take ony values 1 and 2
        e_vec = LA.eigh(Potential(self.pos))[1]
        nac = np.zeros(self.dim,dtype=complex)
        for count in range(self.dim):
            ds = np.zeros(self.dim)
            ds[count] = self.dx
            e_vec2 = LA.eigh(Potential(self.pos+ds))[1]
            der = (e_vec2[:,j-1]-e_vec[:,j-1])/self.dx
            nac[count] = np.vdot(e_vec[:,i-1],der)
        return nac
    
    # def update_density_coefficients(self):
    #     # for the evolution of the coefficients we work in the adiabatic representation
    #     # Runge-Kutta4 method is used
    #     # k1
    #     e_val = LA.eigh(Potential(self.pos))[0]
    #     der_c1 = self.c1*(-1j*e_val[0]/hbar)
    #     der_c1 += self.c2*(np.vdot(self.non_adia_coupling(1,2),self.vel))
    #     self.c1 += self.dt*der_c1
    #     der_c2 = self.c1*(np.vdot(self.non_adia_coupling(2,1),self.vel))
    #     der_c2 += self.c2*(-1j*e_val[1]/hbar)
    #     self.c2 += self.dt*der_c2
        
    # def update_density_coefficients(self):
    #     # for the evolution of the coefficients we work in the adiabatic representation
    #     # Runge-Kutta4 method is used
    #     # k1
    #     # ===========  k1  =============
    #     # ===========  k2  =============
    #     # ===========  k3  =============
    #     # ===========  k4  =============
        
    #     e_val = LA.eigh(Potential(self.pos))[0]
    #     der_c1 = self.c1*(-1j*e_val[0]/hbar-self.nac(1,1)*self.vel)
    #     der_c1 += self.c2*(np.vdot(self.nac(1,2),self.vel))
    #     self.c1 += self.dt*der_c1
    #     der_c2 = self.c1*(np.vdot(self.nac(2,1),self.vel)-self.nac(2,2)*self.vel)
    #     der_c2 += self.c2*(-1j*e_val[1]/hbar)
    #     self.c2 += self.dt*der_c2

    def update_density_coefficients(self):
        # for the evolution of the coefficients we work in the adiabatic representation
        # Runge-Kutta4 method is used
        # k1
        # ===========  k1  =============
        # ===========  k2  =============
        # ===========  k3  =============
        # ===========  k4  =============
        e_val = LA.eigh(Potential(self.pos))[0]
        V = Potential(self.pos)
        der_c1 = self.c1*(-1j*V[0,0]/hbar-np.vdot(self.nac(1,1),self.vel))
        der_c1 += self.c2*(-1j*V[0,1]/hbar-np.vdot(self.nac(1,2),self.vel))
        der_c2 = self.c1*(-1j*V[1,0]/hbar-np.vdot(self.nac(2,1),self.vel))
        der_c2 += self.c2*(-1j*V[1,1]/hbar-np.vdot(self.nac(2,2),self.vel))
        #der_c1 = self.c1*(-1j*e_val[0]/hbar-np.vdot(self.nac(1,1),self.vel))
        #der_c1 += self.c2*(-np.vdot(self.nac(1,2),self.vel))
        #der_c2 = self.c1*(-np.vdot(self.nac(2,1),self.vel))
        #der_c2 += self.c2*(-1j*e_val[1]/hbar-np.vdot(self.nac(2,2),self.vel))
        self.c1 += self.dt*der_c1
        self.c2 += self.dt*der_c2
    
    def time_evolution(self):
        """
        This method defines the time evolution of a particle and
        update all the variables accordingly.
        Position is approximated at the first order, 
        Velocity is approximated at the second order.
        """
        force = self.der_surface(surface=int(self.on_surface)) # n-dimensional vector
        a1 = -force/self.M0
        self.pos += self.vel*self.dt+0.5*a1*self.dt*self.dt
        force = self.der_surface(surface=int(self.on_surface))
        a2 = -force/self.M0
        self.vel += 0.5*(a1+a2)*self.dt

    def density_matrix_coeff(self):
        # initialize electronic state
        a11 = self.c1*np.conj(self.c1)
        a12 = self.c1*np.conj(self.c2)
        a21 = self.c2*np.conj(self.c1)
        a22 = self.c2*np.conj(self.c2)
        return a11, a12, a21, a22

    def coeff_b(self,i,j):
        a11, a12, a21, a22 = self.density_matrix_coeff()
        b12 = 2/hbar*np.imag(np.conj(a12)*Potential(self.pos)[i-1,j-1])-2*np.real(np.conj(a12)*np.inner(self.vel,self.nac(1,2)))
        b21 = 2/hbar*np.imag(np.conj(a21)*Potential(self.pos)[i-1,j-1])-2*np.real(np.conj(a21)*np.inner(self.vel,self.nac(2,1)))
        b = [[0, b12],[b21, 0]]
        return b[i-1][j-1]

    def switch_surface(self):
        a11, a12, a21, a22 = self.density_matrix_coeff()
        criterion = 0
        if self.on_surface:
            criterion = self.coeff_b(1,2)*self.dt/a22
        else:
            criterion = self.coeff_b(2,1)*self.dt/a11
        criterion = max([0,criterion])    
        if rand.random() < criterion:
            dE = LA.eigh(Potential(self.pos))[0][int(not self.on_surface)]-LA.eigh(Potential(self.pos))[0][int(self.on_surface)] 
            if (dE<self.kinetic_energy()):
                # now we have to change the velocity keeping constant the energy
                # E = 1/2*M*v*v+V
                # then from the above definition we can define the new velocity wrt the 
                # energy surface we are
                tot = self.total_energy()
                self.on_surface = not self.on_surface
                V = self.potential_energy()
                new_kinetic = tot-V
                self.vel = np.sign(self.vel)*np.sqrt(2.*new_kinetic/self.M0)

    def kinetic_energy(self):
        K = 0.5*self.vel*self.vel*self.M0
        return K

    def potential_energy(self):
        V = LA.eigh(Potential(self.pos))[0][int(self.on_surface)]
        return V 

    def total_energy(self):
        E = self.kinetic_energy()+self.potential_energy()
        return E

if __name__ == "__main__":
    particle = Particle(np.array([0.1,0.2]), np.array([0.1,0.2]), 1, 0)
    for i in range(200):
        particle.time_evolution()
        particle.update_density_coefficients()
        particle.switch_surface()
        