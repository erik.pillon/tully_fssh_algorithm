import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA
import numpy.random as rand

class Particle:
    """ --------------------------------------------------------------------------
    This is the python class for simulating the classical movement of the particle
    The idea is that the movement of the particle is govenred by a claaaical law 
    of motion. Quantum mechanics is invoked only for the switching probability 
    criterion.
    -------------------------------------------------------------------------- """
    
    global dt # infinitesimal time step inherited from the environment
    global dx # infinitesimal spatial increment inherited from the environment
    global surface1
    global surface2
    global hbar

    def __init__(self, k0, pos, c1, on_surface, M0=2000):
        self.M0 = M0
        self.k = k0
        self.vel = self.k/self.M0
        self.pos = pos
        # c1 and c2 defines the distribution of the particles.
        # in the initial state they are supposed to be taken a Kronecker 
        # delta, even if there are not arbitary restrictions in principle
        self.c1 = c1
        self.c2 = np.sqrt(1-c1*np.conj(c1))
        self.on_surface = on_surface 
        self.energy = k0*k0/2/M0
                
    def update_density_coefficients(self):
        # for the evolution of the coefficients we work in the adiabatic representation
        e_values, e_vectors = LA.eigh(Potential(self.pos))
        self.c1 += dt*(-1j/hbar*e_values[0]*self.c1+self.c2*self.vel*self.non_adia_coupling(1,2))
        self.c2 += dt*(-1j/hbar*e_values[1]*self.c2+self.c1*self.vel*self.non_adia_coupling(2,1))
    
    def update_density_matrix(self):
        # initialize electronic state
        a11 = self.c1*np.conj(self.c1)
        a12 = self.c1*np.conj(self.c2)
        a21 = np.conj(a12)
        a22 = self.c2*np.conj(self.c2)
        return a11, a12, a21, a22
                                
    def der_surface(self, surface=0):
        e_values1, e_vectors1 = LA.eigh(Potential(self.pos))
        e_values2, e_vectors2 = LA.eigh(Potential(self.pos+dx))
        if not self.on_surface:
            surface = 1
        der = (e_values2[surface]-e_values1[surface])/dx
        return der

    def non_adia_coupling(self,i,j):
        # function for the evaluation of the non adiabatic coupling vector d_ij
        # i and j can take ony values 1 and 2
        e_values, e_vectors = LA.eigh(Potential(self.pos))
        e_values2, e_vectors2 = LA.eigh(Potential(self.pos+dx))
        der = (e_vectors2[:,j-1]-e_vectors[:,j-1])/dx
        nac = np.dot(e_vectors[:,i-1],der)
        return nac
    
    def time_evolution(self):
        force = self.der_surface()
        a1 = -force/self.M0
        self.pos += self.vel*dt+0.5*a1*dt*dt
        force = self.der_surface()
        a2 = -force/self.M0
        self.vel += 0.5*(a1+a2)*dt
    
    def coeff_b(self,i,j):
        a11, a12, a21, a22 = self.update_density_matrix()
        b12 = -2*np.real(np.conj(a12)*self.vel*self.non_adia_coupling(1,2))
        b21 = -2*np.real(np.conj(a21)*self.vel*self.non_adia_coupling(2,1))
        b = [[0, b12],[b21, 0]]
        return b[i-1][j-1]

    def switch_surface(self):
        a11, a12, a21, a22 = self.update_density_matrix()
        criterion = 0
        if self.on_surface:
            criterion = self.coeff_b(1,2)*dt/a11
        else:
            criterion = self.coeff_b(2,1)*dt/a22
        if rand.rand() < criterion:
            self.on_surface = not self.on_surface
            # now we have to change the velocity keeping constant the energy
            # E = 1/2*M*v^2+V
            # then from the above definition we can define the new velocity wrt the 
            # energy surface we are
            if self.on_surface:
                surface = 0
            else: 
                surface = 1
            e_values, e_vectors = LA.eigh(Potential(self.pos))    
            new_kinetic = self.energy-e_values[surface]
            self.vel = np.sign(self.vel) * np.sqrt(2.*new_kinetic/self.M0)

    def show_configuration(self):
        plt.plot(x,surface1)
        plt.plot(x,surface2)
        vals, vecs= LA.eigh(Potential(self.pos)) 
        plt.plot(self.pos,vals[0],'o')
        plt.show()
        
    def show_evolution(self, n_iterations):
        plt.ion()
        fig = plt.figure()
        for i in range(n_iterations):
            print(particle.pos)
            print(particle.vel)
            plt.plot(x,surface1)
            vals, vecs= LA.eigh(Potential(particle.pos))
            plt.plot(particle.pos,vals[0],'ro')
            plt.plot(x,surface1,'b-')
            plt.plot(x,surface2,'b-')
            particle.time_evolution()
            fig.canvas.draw()
            plt.pause(0.01)
""" -------------------------------------------------------
Now we define the constant that we will need within the 
code like the discretization, the time steps etc
------------------------------------------------------- """

hbar = 1
dt = 5 # time discretization
dx = 0.01 # space discretization
n = 1000
x = np.linspace(-10,10,n)
surface1 = np.zeros(n)
surface2 = np.zeros(n)
for i in range(n):
    vals, vecs = LA.eigh(Potential(x[i]))
    surface1[i] = vals[0]
    surface2[i] = vals[1]
""" --------------------------------------------------- """

if __name__ =="__main__":
    particle = Particle(50,-5,1,True)
    plt.ion()
    fig = plt.figure()
    for i in range(200):
        particle.time_evolution()
        particle.update_density_coefficients()
        particle.switch_surface()
        plt.plot(x,surface1)
        vals, vecs= LA.eigh(Potential(particle.pos))
        if particle.on_surface:
            surf = 0
        else:
            surf = 1
        plt.plot(particle.pos,vals[surf],'ro')
        plt.plot(x,surface1,'b-')
        plt.plot(x,surface2,'b-')
        fig.canvas.draw()
        plt.pause(0.022)

