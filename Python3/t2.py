from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

print(size)
print(rank)

data =(rank+1)**2

data = comm.gather(data, root=0)
if rank == 0:
    for i in range(1, size):
        data[i] = (i+i)**2
        value=data[i]
        print("process %s recieve %s from process %s " % (rank, value, i))
