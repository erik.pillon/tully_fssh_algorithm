import particle

"""
Class Particle is defined in the following way:
__init__(self, k, pos, c1, on_surface, M0=2000, dx=0.001, dt=1)
"""

import numpy as np
import numpy.linalg as LA
import matplotlib.pyplot as plt
import potential

Potential = potential.Potential_tully

# -----------------------------------
# global variables
dx = 0.001
# -----------------------------------

# n_points = 200
# W = 0.3
# A = 0.1
# B = 3.0
def non_adia_coupling(q,i,j):
        # function for the evaluation of the non adiabatic coupling vector d_ij
        # non-adiabatic vector cupling = Connection (= vector potential)
        # i and j can take ony values 1 and 2
        dim = np.size(q)
        e_vec = LA.eigh(Potential(q))[1]
        nac = np.zeros(dim,dtype=complex)
        for count in range(dim):
            ds = np.zeros(dim)
            ds[count] = dx
            e_vec2 = LA.eigh(Potential(q+ds))[1]
            der = (e_vec2[:,j-1]-e_vec[:,j-1])/dx
            nac[count] = np.vdot(e_vec[:,i-1],der)
        return nac

# ------------- DISPLAY THE PES ----------------------------
n_points = 500
x = np.linspace(-3, 3, n_points)

ev = np.zeros((2,n_points))
V_00 = np.zeros(n_points)
d_01 = np.zeros(n_points,dtype=complex)
d_10 = np.zeros(n_points,dtype=complex)

for i in range(n_points):
    ev[:,i] = LA.eigh(Potential(x[i]))[0]
    d_01[i] = non_adia_coupling(x[i],1,2)
    d_10[i] = non_adia_coupling(x[i],2,1)

plt.figure()
plt.title('Energy Surfaces')
plt.plot(x,ev[0,:], label=r'$E_{0}$',linewidth=2.0)
plt.plot(x,ev[1,:], label=r'$E_{1}$',linewidth=2.0)
plt.plot(x,V_00, '--', label=r'$H_{00}$',linewidth=1.0)
plt.plot(x,-V_00, '--', label=r'$H_{11}$',linewidth=1.0)
plt.plot(x,np.sqrt(np.real(d_01[:])**2+np.imag(d_01[:])**2)/50,'--', label=r'$d_{01}$',ms=1)
#plt.plot(x,np.sqrt(np.real(d_01[1,:])**2+np.imag(d_01[1,:])**2)/10,'.', label=r'$d_{10}$',ms=1)
plt.legend({'E_1', 'H_00', 'H_11', 'd01x','d01y'})
plt.xlabel(r'x $(a_0)$')
plt.ylabel('Energy (a.u.)')
plt.legend()
plt.savefig('PES_tully.png')
plt.show()
# ----------------------------------------------------------

x = np.array([-5.])
k = np.array([18.0])
c1 = 1.
on_surface = 0
# # p = particle.Particle(k, x, c1, on_surface, 2000, 0.001, 1)

it = 10
count = 0
k_range = [10,12,15,18,21,24,27,30]
k_range = [10]
counter = np.zeros(np.size(k_range))
for j in range(np.size(k_range)):
    count = 0
    for c in range(it):
        x = np.array([-5.])
        k = k_range[j]
        c1 = 1.
        on_surface = 0
        p = particle.Particle(k, x, c1, on_surface, 2000, 0.001,0.5)
        for i in range(10000):
            p.time_evolution()
            p.update_density_coefficients()
            p.switch_surface()
            if (p.pos>7):
               continue 
        if (c%10==0):
            print("we're at k= {:1.6f} at iteration {:5d}".format(k_range[j],c))
        
        # collect the data for the statistics    
        # print(p.c1)
        # print(p.c2)
        # print(p.on_surface)
        count += p.on_surface
        print(p.c1,p.c2)
    print('----------------------------------------')
    counter[j] = count/it*100
    print(counter) 
# print('running the probability of reflection in the lower state')
# print('--------------------------------------------------------')
