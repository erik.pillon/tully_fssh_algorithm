def Potential_test1(x, A=0.01, B=1.6, C=0.005, D=1.0):
    V = np.zeros((2,2))
    if x>0:
        V[0,0] = A*(1-np.exp(-B*x))
    else:
        V[0,0] = -A*(1-np.exp(B*x))
    V[1,1] = -V[0,0]
    V[0,1] = C*np.exp(-D*x**2)
    V[1,0] = V[0,1]
    return V