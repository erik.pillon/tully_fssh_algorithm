-------------------------------------------------------
This file was generated on: 2020-10-25T02:29:56.923

This is a report of time spent by the SH algorithm
In this simulation we simulated the dynamics at k = [8.0, 8.5, 8.75, 9.0, 9.5, 10.0, 11.0, 12.0, 13.0, 16.0, 20.0, 25.0, 30.0]
The results we got are Any[0.152, 0.191, 0.2, 0.2055, 0.2115, 0.2245, 0.2575, 0.2815, 0.336, 0.446, 0.56, 0.6835, 0.759]
We used 2000 number of iterations for the dynamics with a time step of 1 seconds a.u.
For every k we used a total of 2000 trajectories.
-------------------------------------------------------


==========================================================================================
|            Task                      |          Time          |       Percentage       |
|-----------------------------------------------------------------------------------------
| Initializing the variables           |                  17282 |   0.00060211173217602% |
| Importing the packages               |                   1454 |   0.00005065793649948% |
| Running the dynamics                 |           163989418144 |   5713.45634864143721643% |
| Switching and adjusting the momentum |                      0 |   0.00000000000000000% |
| NAC terms evaluation                 |                      0 |   0.00000000000000000% |
|-----------------------------------------------------------------------------------------
|                                      |               28702314 |   1.00000000000000000  | 
==========================================================================================
-------------------------------------------------------
This file was generated on: 2020-10-27T12:26:59.069

This is a report of time spent by the SH algorithm
In this simulation we simulated the dynamics at k = [8.0, 8.5, 9.0]
The results we got are Any[0.0, 0.0, 0.0]
We used 2000 number of iterations for the dynamics with a time step of 1 seconds a.u.
For every k we used a total of 200 trajectories.
-------------------------------------------------------


==========================================================================================
|            Task                      |          Time          |       Percentage       |
|-----------------------------------------------------------------------------------------
| Initializing the variables           |                  18944 |   0.05700083647764679% |
| Importing the packages               |                   1731 |   0.00520842736184579% |
| Running the dynamics                 |               30009972 |   90.29737783314176625% |
| Switching and adjusting the momentum |                      0 |   0.00000000000000000% |
| NAC terms evaluation                 |                      0 |   0.00000000000000000% |
|-----------------------------------------------------------------------------------------
|                                      |                 332346 |   1.00000000000000000  | 
==========================================================================================
-------------------------------------------------------
This file was generated on: 2020-10-27T14:34:53.663

This is a report of time spent by the SH algorithm
In this simulation we simulated the dynamics at k = [20, 25, 30]
The results we got are Any[0.0, 0.0, 0.0]
We used 2000 number of iterations for the dynamics with a time step of 1 seconds a.u.
For every k we used a total of 1 trajectories.
-------------------------------------------------------


==========================================================================================
|            Task                      |          Time          |       Percentage       |
|-----------------------------------------------------------------------------------------
| Initializing the variables           |                  13771 |   0.52888086642599275% |
| Importing the packages               |                   1277 |   0.04904370535371380% |
| Running the dynamics                 |                  10388 |   0.39895537291650662% |
| Switching and adjusting the momentum |                      0 |   0.00000000000000000% |
| NAC terms evaluation                 |                      0 |   0.00000000000000000% |
|-----------------------------------------------------------------------------------------
|                                      |                  26038 |   1.00000000000000000  | 
==========================================================================================
-------------------------------------------------------
This file was generated on: 2020-10-27T14:38:19.985

This is a report of time spent by the SH algorithm
In this simulation we simulated the dynamics at k = [20, 25, 30]
The results we got are Any[1.0, 1.0, 1.0]
We used 2000 number of iterations for the dynamics with a time step of 1 seconds a.u.
For every k we used a total of 1 trajectories.
-------------------------------------------------------


==========================================================================================
|            Task                      |          Time          |       Percentage       |
|-----------------------------------------------------------------------------------------
| Initializing the variables           |                  13794 |   0.54472218931406235% |
| Importing the packages               |                   1271 |   0.05019152549066067% |
| Running the dynamics                 |                   9736 |   0.38447261382932513% |
| Switching and adjusting the momentum |                      0 |   0.00000000000000000% |
| NAC terms evaluation                 |                      0 |   0.00000000000000000% |
|-----------------------------------------------------------------------------------------
|                                      |                  25323 |   1.00000000000000000  | 
==========================================================================================
-------------------------------------------------------
This file was generated on: 2020-10-27T20:46:11.007

This is a report of time spent by the SH algorithm
In this simulation we simulated the dynamics at k = [8.0, 8.5, 9.0, 9.5, 10.0, 11.0, 12.0, 13.0, 15.0, 17.5, 20.0, 25.0, 30.0]
The results we got are Any[0.129, 0.1445, 0.1855, 0.205, 0.2065, 0.2415, 0.271, 0.3235, 0.407, 0.485, 0.5505, 0.6675, 0.7665]
We used 2000 number of iterations for the dynamics with a time step of 1 seconds a.u.
For every k we used a total of 2000 trajectories.
-------------------------------------------------------


==========================================================================================
|            Task                      |          Time          |       Percentage       |
|-----------------------------------------------------------------------------------------
| Initializing the variables           |                  22447 |   0.00108303114977457% |
| Importing the packages               |                   1332 |   0.00006426682815074% |
| Running the dynamics                 |           102964276342 |   4967.85844845112842449% |
| Switching and adjusting the momentum |                      0 |   0.00000000000000000% |
| NAC terms evaluation                 |                      0 |   0.00000000000000000% |
|-----------------------------------------------------------------------------------------
|                                      |               20726089 |   1.00000000000000000  | 
==========================================================================================
-------------------------------------------------------
This file was generated on: 2020-11-24T15:26:59.351

This is a report of time spent by the SH algorithm
In this simulation we simulated the dynamics at k = [8.0, 8.5, 9.0]
The results we got are Any[0.0, 0.0, 0.0]
We used 500 number of iterations for the dynamics with a time step of 1 seconds a.u.
For every k we used a total of 20 trajectories.
-------------------------------------------------------


==========================================================================================
|            Task                      |          Time          |       Percentage       |
|-----------------------------------------------------------------------------------------
| Initializing the variables           |                  20194 |   0.63250540295048074% |
| Importing the packages               |                    238 |   0.00745450559087919% |
| Running the dynamics                 |                 152668 |   4.78178344348043982% |
| Switching and adjusting the momentum |                      0 |   0.00000000000000000% |
| NAC terms evaluation                 |                      0 |   0.00000000000000000% |
|-----------------------------------------------------------------------------------------
|                                      |                  31927 |   1.00000000000000000  | 
==========================================================================================
