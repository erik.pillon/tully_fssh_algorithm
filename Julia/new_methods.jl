
# ===================================
#     ________    _______
#    /  ______|  /   __  \
#    | |         |  |  \  |
#    | |_____    |  |__/  |
#    |  _____|   |  _____/
#    | |         | |
#    | |______   | |
#    \________|  \_/
#
#    erik.pillon@gmail.com
#    http://ErikPillon.github.io
#
# ====================================

using Dierckx
using LinearAlgebra
using Polynomials
using LaTeXStrings
using JLD2

mutable struct simple_wp
    pos::Array{Float64,1}
    k::Array{Float64,1}
    c1::Number
    c2::Number
    m::Int
end

function update_density_coefficients(p::simple_wp, dt=1)
    # for the evolution of the coefficients we work in the adiabatic representation
    # an explicit Euler method is used
    E_1 = E1(p.pos[1])
    E_2 = E2(p.pos[1])
    vel = p.k/p.m
    d12 = d_12_interp(p.pos[1])
    d21 = -conj(d12)
    # we evaluate first the derivatives
    der_c1 = p.c1*(-1im*E_1/hbar)
    der_c1 += p.c2*(-d12*vel[1])
    der_c2 = p.c1*(-d21*vel[1])
    der_c2 += p.c2*(-1im*E_2/hbar)
    p.c1 += dt*der_c1
    p.c2 += dt*der_c2
end

function der_surface(p::simple_wp, displacement=0)
    delta = 0.000001
    if p.surface == 1
        return (E1(p.pos[1]+displacement+delta)-E1(p.pos[1]+displacement-delta))/2delta
    else
        return (E2(p.pos[1]+displacement+delta)-E2(p.pos[1]+displacement-delta))/2delta
    end
end

function time_evolution(p::simple_wp, dV, dt::Real)
    # This method defines the time evolution of a particle and
    # update all the variables accordingly.
    # Position is approximated at the first order,
    # momentum is approximated at the second order.
    # ======= Surface averaged derivative ==========
    der_pot = [conj(p.c1) conj(p.c2)]*dV(p.pos[1])*[p.c1; p.c2]
    # ==============================================
    epss = 0.00000001
    # ======= Topological Force ==========
    d_d12 = (d_12_interp(p.pos[1]+epss)-d_12_interp(p.pos[1]-epss))/2epss
    d_d21 = -conj(d_d12)
    d_d11 = (d_11_interp(p.pos[1]+epss)-d_11_interp(p.pos[1]-epss))/2epss
    d_d22 = (d_22_interp(p.pos[1]+epss)-d_22_interp(p.pos[1]-epss))/2epss
    c12 = conj(p.c2)*p.c1*d_d21
    c21 = conj(p.c1)*p.c2*d_d12
    # ====================================
    der_pot = real(der_pot[1])
    force = -der_pot-real(1im*hbar*(c21+c12)*p.k[1])/p.m
    # force = -der_pot
    a1 = force/p.m
    p.k[1] += force*dt/2
    p.pos[1] += (p.k[1]/p.m)*dt
    # ======= Topological Force ==========
    d_d12 = (d_12_interp(p.pos[1]+epss)-d_12_interp(p.pos[1]-epss))/2epss
    d_d21 = -conj(d_d12)
    d_d11 = (d_11_interp(p.pos[1]+epss)-d_11_interp(p.pos[1]-epss))/2epss
    d_d22 = (d_22_interp(p.pos[1]+epss)-d_22_interp(p.pos[1]-epss))/2epss
    c12 = conj(p.c2)*p.c1*d_d21
    c21 = conj(p.c1)*p.c2*d_d12
    # ====================================
    force = -der_pot-real(1im*hbar*(c21+c12)*p.k[1])/p.m
    # force = -der_pot
    a2 = force/p.m
    p.k[1] += a2*dt/2
end

function time_evolution_ehrenfest(p::simple_wp, dV, dt::Real)
    # This method defines the time evolution of a particle and
    # update all the variables accordingly.
    # Position is approximated at the first order,
    # momentum is approximated at the second order.
    epss = 0.00000001
    # ======= Surface averaged derivative ==========
    der_E1 = (E1(p.pos[1]+epss)-E1(p.pos[1]))/epss
    der_E2 = (E2(p.pos[1]+epss)-E2(p.pos[1]))/epss
    der_pot = abs(p.c1)^2*der_E1+abs(p.c1)^2*der_E1
    # ==============================================
    non_adia = conj(p.c1)*p.c2*(E1(p.pos[1])-E2(p.pos[1]))*d_12_interp(p.pos[1])
    non_adia += conj(p.c2)*p.c1*(E2(p.pos[1])-E1(p.pos[1]))*(-conj(d_12_interp(p.pos[1])))
    # # ======= Topological Force ==========
    # d_d12 = (d_12_interp(p.pos[1]+epss)-d_12_interp(p.pos[1]-epss))/2epss
    # d_d21 = -conj(d_d12)
    # d_d11 = (d_11_interp(p.pos[1]+epss)-d_11_interp(p.pos[1]-epss))/2epss
    # d_d22 = (d_22_interp(p.pos[1]+epss)-d_22_interp(p.pos[1]-epss))/2epss
    # c12 = conj(p.c2)*p.c1*d_d21
    # c21 = conj(p.c1)*p.c2*d_d12
    # ====================================
    force = -der_pot + non_adia
    p.k[1] += force*dt/2
    p.pos[1] += (p.k[1]/p.m)*dt
    # # ======= Topological Force ==========
    # d_d12 = (d_12_interp(p.pos[1]+epss)-d_12_interp(p.pos[1]-epss))/2epss
    # d_d21 = -conj(d_d12)
    # d_d11 = (d_11_interp(p.pos[1]+epss)-d_11_interp(p.pos[1]-epss))/2epss
    # d_d22 = (d_22_interp(p.pos[1]+epss)-d_22_interp(p.pos[1]-epss))/2epss
    # c12 = conj(p.c2)*p.c1*d_d21
    # c21 = conj(p.c1)*p.c2*d_d12
    # # ====================================
    # force = -der_pot-real(1im*hbar*(c21+c12)*p.k[1])/p.m
    # force = -der_pot
    # ======= Surface averaged derivative ==========
    der_E1 = (E1(p.pos[1]+epss)-E1(p.pos[1]))/epss
    der_E2 = (E2(p.pos[1]+epss)-E2(p.pos[1]))/epss
    der_pot = abs(p.c1)^2*der_E1+abs(p.c1)^2*der_E1
    # ==============================================
    non_adia = conj(p.c1)*p.c2*(E1(p.pos[1])-E2(p.pos[1]))*d_12_interp(p.pos[1])
    non_adia += conj(p.c2)*p.c1*(E2(p.pos[1])-E1(p.pos[1]))*(-conj(d_12_interp(p.pos[1])))
    force = - der_pot + non_adia
    p.k[1] += force*dt/2
end

function run_dynamics(p::simple_wp, dV, dt::Number, ts::Int)
    # -- INPUT --
    # p: Particle
    # dt: timestep
    # ts: number of timesteps
    i = 0
    while i<ts && abs(p.pos[1])<5.5
        time_evolution(p,dV,dt)
        for k in 1:50
            update_density_coefficients(p, dt/50)
        end
        i += 1
    end
end

function run_dynamics_simple_ehrenfest(p::simple_wp, dt::Number, ts::Int)
    # -- INPUT --
    # p: Particle
    # dt: timestep
    # ts: number of timesteps
    i = 0
    while i<ts && abs(p.pos[1])<5.5
        time_evolution_ehrenfest(p,dt)
        for k in 1:50
            update_density_coefficients(p, dt/50)
        end
        i += 1
    end
end
