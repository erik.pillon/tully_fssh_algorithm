using Plots
using JLD2
using JLD
using Dierckx


# Possible systems are
# - Dual_avoided_crossing
# - Simple_avoided_crossing
# - Extended_coupling

system = "/Simple_avoided_crossing.jld"

s = pwd()*system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

#  :d_12_interp
#  :d_11_interp
#  :d_22_interp
#  :E1
#  :E2

if ARGS[1] == [] 
    C = 5
else 
    C = ARGS[1]
end

k_test = [6, 7, 8.5, 10, 11, 14, 15, 20, 30]
p_tran = [0, 0, 0,   15, 20, 25, 33, 52, 66]

k_range = [7, 7.5, 8, 8.5, 9, 9.5, 10, 11, 12, 13, 14, 15, 17.5, 20, 25, 30]
transm = [0.0, 0.0 , 0.0, 0.0, 0.1255, 0.128, 0.144, 0.166, 0.184, 0.2195, 0.241, 0.2675, 0.3655, 0.4745, 0.616, 0.7125]

variances = []
for i in transm
    x = 2000*i
    var = x*(1-i)^2+(2000-x)*(i)^2
    var = var/2000
    append!(variances,var)
end


gr()
p = plot(k_range,transm,
    grid=false,
    label="implementation",
    title = "Comparison FSSH-Ehrenfest \n C=",
    ytickfont = font(8),
    xtickfont = font(8),
    ylim = (-0.1,1),
    ylabel = "Transmission probability",
    xlabel = "k"
)
scatter!(k_range,transm,
markershape = :circle,
markersize = 5,
label="",
)
scatter!(k_test, p_tran/100,
    label = "FSSH data")
display(display(p))

readline()

s = pwd()*"result.png"

savefig(s)
