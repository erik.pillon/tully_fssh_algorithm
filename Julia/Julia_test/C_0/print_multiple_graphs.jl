# print_multiple_graphs.jl

using Plots

using Dierckx
using Polynomials
using JLD2
using JLD

system = "/Simple_avoided_crossing.jld"

s = pwd()*system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

# ================= FIRST PLOT =================
x = range(-5,stop=5, length=5000)
y1 = E1(x)
y2 = E2(x)
y3 = d_12_interp(x)

p1 = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates")
plot!(x,y2,label = "E2")
plot!(x,y3/200,label="d_12/200")
# ==============================================

# ================= SECOND PLOT =================

p2 = plot(xlabel="Nuclear Energy",
    ylabel="Transmission Probability")

# -----------------------------------------------
# simple_ehrenfest.jl
# standard_ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
plot!(k_range, transm,line = (:steppre, :dot, :red))
plot!(k_range, transm, seriestype = :scatter)
# -----------------------------------------------
# extended_FSSH.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
plot!(k_range, transm)
plot!(k_range, transm, seriestype = :scatter)
# -----------------------------------------------
# simple_Ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
plot!(k_range, transm)
plot!(k_range, transm, seriestype = :scatter)
# -----------------------------------------------
# standard_ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
plot!(k_range, transm)
plot!(k_range, transm, seriestype = :scatter)
# ===============================================


plot(p1,p2, layout=grid(2,1, heights=[0.3, 0.7]))
