# test_main_2d.jl

#
# “God does not care about our mathematical difficulties. He integrates empirically.”
# Albert Einstein
#

# -----------------------------
using Dates
t0 = Dates.now()
using Printf
using LinearAlgebra
using Plots
using Random
using MPI
using Dierckx
using Polynomials
using LaTeXStrings
using JLD2
using JLD
t1 = Dates.now()
println("Time needed for importing all the packages: ", t1-t0)
# -----------------------------
println("--- Script started ---")
println("--- Importing methods ---")
include("particle_methods_2d.jl")
t2 = Dates.now()
println("--- methods imported correctly ---")
# -----------------------------

dt = 1
hbar = 1

# Possible systems are
# - Dual_avoided_crossing
# - Simple_avoided_crossing
# - Extended_coupling

system = "/Simple_avoided_crossing.jld"

s = pwd()*system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

#  :d_12_interp
#  :d_11_interp
#  :d_22_interp
#  :E1
#  :E2

function main()
    n_time_steps = 500
    dt = 1
    n_test = 20;
    k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
    # k_range = [9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
    # k_range = [20, 25, 30]
    k_range = [8, 8.5, 9]
    # every value below 8.9 should be zero
    MPI.Init()

    time_actual_algorithm = 0
    t3_cumulative = 0

    comm = MPI.COMM_WORLD
    size = MPI.Comm_size(comm)
    rank = MPI.Comm_rank(comm)
    transm = []
    t3_total = 0
    t3 = Dates.now()

    for k in k_range
        println("started to analysze k=",k)
        count = 0
        for i in 1:n_test
            if !(mod(i,size)==rank); continue end
            if (i%50 == 0)
                println("we are at k= $k and iteration $i out of $n_test")
            end
            p = Particle([-5],[k],1,0,1,2000)
            run_dynamics(p,dt,n_time_steps)
            if p.surface == 2
                count += 1
            end
            t3_end = Dates.now()
            t3_total += Dates.value(Dates.Millisecond(t3_end-t3))
        end
        total_count = MPI.Reduce(count,MPI.SUM,0,comm)

        if rank == 0
            # ----- Time analysis -----
            # global time_actual_algorithm += t3_cumulative
            #    println("time spent for evaluation of k= ",k, " is: ", t3_cumulative)
            # println("time spent since the beginning of the algorithm: ", time_actual_algorithm)
            # ----- Actual analysis ------
            # print(total_count/n_test,"\n")
            push!(transm,total_count/n_test)
            print(transm)
        end
    end

    sleep(rank*2)

    time_actual_algorithm = MPI.Reduce(t3_total,MPI.SUM,0,comm)
    if rank == 0
        # ----- Time analysis -----
        # global time_actual_algorithm += t3_cumulative
        #    println("time spent for evaluation of k= ",k, " is: ", t3_cumulative)
        println("time spent for the dynamics for each core: ", time_actual_algorithm/size)
        println("time spent for the dynamics globally: ", time_actual_algorithm)
    end

    MPI.Barrier(comm)
    MPI.Finalize()

    t_final = Dates.now()

    if rank == 0
        path_to_file = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Simple_2D.txt"
        # variables
        package_time = Dates.value(Dates.Millisecond(t1-t0))
        # package_time = 0
        import_time = Dates.value(Dates.Millisecond(t2-t1))
        # import_time = 0
        dynamics_time = time_actual_algorithm/size
        # dynamics_time = 0
        total_time = Dates.value(Dates.Millisecond(t_final-t0))
        external_save(path_to_file, k_range, n_time_steps, dt, n_test, transm, package_time, import_time, dynamics_time, total_time)
    end

end

main()
