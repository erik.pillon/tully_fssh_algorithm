function Simple_avoided_crossing(pos::Real)::Array{Float64,2}
    x = pos
    A, B, C, D = 0.01, 1.6, 0.005, 1.0
    V = zeros(2,2)
    if x>0
        V[1,1] = A*(1-exp(-B*x))
    else
        V[1,1] = -A*(1-exp(B*x))
    end
    V[2,2] = -V[1,1]
    V[1,2] = C*exp(-D*x^2)
    V[2,1] = V[1,2]
    return V
end

function dV_Simple_Avoided_Crossing(x::Real)::Array{Real,2}
    A = 0.01
    B = 1.6
    C = 0.005
    D = 1.0
    dV = zeros(2,2)
    if x>0
        dV[1,1] = A*B*(exp(-B*x))
    else
        dV[1,1] = A*B*(exp(B*x))
    end
    dV[2,2] = -dV[1,1]
    dV[1,2] = dV[2,1] = 2x*C*D*exp(-D*x^2)
    return dV
end

function Dual_avoided_crossing(x::Real)::Array{Real,2}
    A, B, C, D = 0.1, 0.28, 0.015, 0.06
    E_0 = 0.05
    V = zeros(2,2)
    V[1,1] = 0.
    V[2,2] = -A*exp(-B*x^2)+E_0
    V[1,2] = V[2,1] = C*exp(-D*x^2)
    return V
end

function dV_Dual_avoided_crossing(x::Real)::Array{Real,2}
    A, B, C, D = 0.1, 0.28, 0.015, 0.06
    V = zeros(2,2)
    V[1,1] = 0.
    V[2,2] = A*B*2x*exp(-B*x^2)
    V[1,2] = V[2,1] = -C*D*2x*exp(-D*x^2)
    return V
end
