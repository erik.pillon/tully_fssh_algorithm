# particle_methods_2d.jl

# Erik Pillon
# TCP Group, University of Luxembourg
# December 2020


using Dierckx
using LinearAlgebra
using Polynomials
using LaTeXStrings
using JLD2

mutable struct Particle
    pos::Array{Float64,1}
    k::Array{Float64,1}
    c1::Number
    c2::Number
    surface::Int
    m::Int
end

function kinetic_energy(p::Particle)
    return sum(p.k.*p.k)/2/p.m
end

function potential_energy(p::Particle)
    if p.surface == 1
        return E1(p.pos[1])
    else
        return E1(p.pos[1])
    end
end

function total_energy(p::Particle)
    return potential_energy(p)+kinetic_energy(p)
end

function update_density_coefficients(p::Particle, dt=1)
    # for the evolution of the coefficients we work in the adiabatic representation
    # an explicit Euler method is used
    E_1 = E1(p.pos[1])
    E_2 = E2(p.pos[1])
    vel = p.k/p.m
    d12 = d_12_interp(p.pos[1])
    d21 = -conj(d12)
    # we evaluate first the derivatives
    der_c1 = p.c1*(-1im*E_1/hbar)
    der_c1 += p.c2*(-d12*vel[1])
    der_c2 = p.c1*(-d21*vel[1])
    der_c2 += p.c2*(-1im*E_2/hbar)
    p.c1 += dt*der_c1
    p.c2 += dt*der_c2
end

function der_surface(p::Particle, displacement=0)
    delta = 0.000001
    if p.surface == 1
        return (E1(p.pos[1]+displacement+delta)-E1(p.pos[1]+displacement-delta))/2delta
    else
        return (E2(p.pos[1]+displacement+delta)-E2(p.pos[1]+displacement-delta))/2delta
    end
end

function time_evolution_rk4(p::Particle, dt::Real)
    h = dt
    # ====== 1 ======
    E_1 = E1(p.pos[1])
    E_2 = E2(p.pos[1])
    d12 = d_12_interp(p.pos[1])
    d21 = -conj(d12)
    d11 = d_11_interp(p.pos[1])
    d22 = d_22_interp(p.pos[1])
    # ---------------
    vel = p.k[1]/p.m
    der_pos_1 = h*vel[1]
    der_vel_1 = -h*der_surface(p)/p.m
    der_c1_1 = h*(p.c1*(-1im*E_1/hbar-imag(d11)*vel)+p.c2*(-d12*vel))
    der_c2_1 = h*(p.c1*(-d21*vel)+p.c2*(-1im*E_2/hbar-imag(d22)*vel))
    # ====== 2 ======
    der_pos_2 = h*(vel[1]+der_vel_1[1]/2)
    der_vel_2 = -h*der_surface(p,der_pos_2[1]/2)/p.m
    # ---------------
    E_1 = E1(p.pos[1]+der_pos_1/2)
    E_2 = E2(p.pos[1]+der_pos_1/2)
    d12 = d_12_interp(p.pos[1]+der_pos_1/2)
    d21 = -conj(d12)
    d11 = d_11_interp(p.pos[1]+der_pos_1/2)
    d22 = d_22_interp(p.pos[1]+der_pos_1/2)
    # ---------------
    der_c1_2 = h*((p.c1+der_c1_1/2)*(-1im*E_1/hbar-imag(d11)*(vel+der_vel_1/2))+(p.c2+der_c2_1/2)*(-d12*(vel+der_vel_1/2)))
    der_c2_2 = h*((p.c1+der_c1_1/2)*(-d21*(vel+der_vel_1/2))+(p.c2+der_c2_1/2)*(-1im*E_2/hbar-imag(d22)*(vel+der_vel_1/2)))
    # ====== 3 ======
    E_1 = E1(p.pos[1]+der_pos_2/2)
    E_2 = E2(p.pos[1]+der_pos_2/2)
    d12 = d_12_interp(p.pos[1]+der_pos_2/2)
    d21 = -conj(d12)
    d11 = d_11_interp(p.pos[1]+der_pos_2/2)
    d22 = d_22_interp(p.pos[1]+der_pos_2/2)
    # ---------------
    der_pos_3 = h*(vel[1]+der_vel_2[1]/2)
    der_vel_3 = -h*der_surface(p,der_pos_2/2)/p.m
    # ---------------
    der_c1_3 = h*((p.c1+der_c1_2/2)*(-1im*E_1/hbar-imag(d11)*(vel+der_vel_2/2))+(p.c2+der_c2_2/2)*(-d12*(vel+der_vel_2/2)))
    der_c2_3 = h*((p.c1+der_c1_2/2)*(-d21*(vel+der_vel_2/2))+(p.c2+der_c2_2/2)*(-1im*E_2/hbar-imag(d22)*(vel+der_vel_2/2)))
    # ====== 4 ======
    E_1 = E1(p.pos[1]+der_pos_3)
    E_2 = E2(p.pos[1]+der_pos_3)
    d12 = d_12_interp(p.pos[1]+der_pos_3)
    d21 = -conj(d12)
    d11 = d_11_interp(p.pos[1]+der_pos_3)
    d22 = d_22_interp(p.pos[1]+der_pos_3)
    # ---------------
    der_pos_4 = h*(vel[1]+der_vel_3)
    der_vel_4 = -h*der_surface(p,der_pos_3)/p.m
    # ---------------
    der_c1_4 = h*(((p.c1+der_c1_3)*(-1im*E_1/hbar-imag(d11))*(vel+der_vel_3))+(p.c2+der_c2_3)*(-d12*(vel+der_vel_3)))
    der_c2_4 = h*((p.c1+der_c1_3)*(-d21*(vel+der_vel_3))+(p.c2+der_c2_3)*(-1im*E_2/hbar-imag(d22)*(vel+der_vel_3)))
    # === final values ===
    vel += (der_vel_1+2*der_vel_2+2*der_vel_3+der_vel_4)/6
    p.k[1] = vel*p.m
    p.pos[1] += (der_pos_1+2*der_pos_2+2*der_pos_3+der_pos_4)/6
    p.c1 += (der_c1_1+2*der_c1_2+2*der_c1_3+der_c1_4)/6
    p.c2 += (der_c2_1+2*der_c2_2+2*der_c2_3+der_c2_4)/6
end

function time_evolution(p::Particle, dt::Real)
    # This method defines the time evolution of a particle and
    # update all the variables accordingly.
    # Position is approximated at the first order,
    # momentum is approximated at the second order.
    # ---------------------------------------------------------
    # Half step momentum
    force = der_surface(p)
    p.k[1] += -force*dt/2
    # Complete step position
    p.pos[1] += p.k[1]*dt/p.m
    # Half step momentum
    force = der_surface(p)
    p.k[1] += -force*dt/2
end

function time_evolution_extended(p::Particle, dt::Real)
    # This method defines the time evolution of a particle and
    # update all the variables accordingly.
    # Position is approximated at the first order,
    # momentum is approximated at the second order.
    # ---------------------------------------------------------
    epss = 0.00000001
    d_d12 = (d_12_interp(p.pos[1]+epss)-d_12_interp(p.pos[1]-epss))/2epss
    d12 = d_12_interp(p.pos[1])
    if p.surface == 1
        dc = d_d12
        c = d12
    else
        dc = -conj(d_d12)
        c = -conj(d12)
    end
    # Half step momentum
    force = der_surface(p)
    p.k[1] += -(force+hbar*dc*p.k[1]/p.m)*dt/2
    # Complete step position
    # a correction to the position is needed since we're introducing a new
    # term in the momentum that should not appear in the equation for the
    # position (see e.g. Eq. 65 of Martens)
    p.pos[1] += p.k[1]*dt/p.m+hbar*c*dt
    d_d12 = (d_12_interp(p.pos[1]+epss)-d_12_interp(p.pos[1]-epss))/2epss
    if p.surface == 1
        dc = d_d12
    else
        dc = -conj(d_d12)
    end
    # Half step momentum
    force = der_surface(p)
    p.k[1] += -(force+hbar*dc*p.k[1]/p.m)*dt/2
end

function density_matrix_coeff(p::Particle)
    # initialize electronic state
    a11 = p.c1*conj(p.c1)
    a12 = p.c1*conj(p.c2)
    a21 = p.c2*conj(p.c1)
    a22 = p.c2*conj(p.c2)
    return a11, a12, a21, a22
end

function coeff_b(p::Particle,i::Int,j::Int)::Float64
    a11, a12, a21, a22 = density_matrix_coeff(p)
    vel = p.k[1]/p.m
    if i == 1
        d12 = d_12_interp(p.pos[1])
        b = -2*real(conj(a12)*sum(vel.*d12))
    else
        d21 = -conj(d_12_interp(p.pos[1]))
        b = -2*real(conj(a21)*sum(vel.*d21))
    end
    return b
end

function switch_surface(p::Particle, dt::Real)
    a11, a12, a21, a22 = density_matrix_coeff(p)
    criterion = 0
    if p.surface == 1
        criterion = coeff_b(p,2,1)*dt/a11
    else
        criterion = coeff_b(p,1,2)*dt/a22
    end
    criterion = maximum([0,real(criterion)])
    zeta = Random.rand()
    if  criterion > zeta
        # If I'm in 1 I want dE=E(2)-E(1)
        # while if I'm in 2 I want dE=E(1)-E(2)
        E_1 = E1(p.pos[1])
        E_2 = E2(p.pos[1])
        if p.surface == 1
            dE = E_2-E_1
        else
            dE = E_1-E_2
        end
        if (dE<kinetic_energy(p))
            # now we have to change the velocity keeping constant the energy
            # E = 1/2*M*v*v+V
            # then from the above definition we can define the new velocity wrt the
            # energy surface we are
            tot = total_energy(p)
            p.surface = 3-p.surface
            V_new = potential_energy(p)
            new_kinetic = tot-V_new
            # momentum adjustment
            p.k[1] = sign(p.k[1])*sqrt(2*new_kinetic*p.m)
        end
    end
end

function run_dynamics(p::Particle, dt::Number, ts::Int)
    # -- INPUT --
    # p: Particle
    # dt: timestep
    # ts: number of timesteps
    i = 0
    while i<ts && abs(p.pos[1])<5.5
        time_evolution(p,dt)
        for j = 1:50
            update_density_coefficients(p,dt/50)
        end
        switch_surface(p,dt)
        i += 1
    end
end

function run_dynamics_extended(p::Particle, dt::Number, ts::Int)
    # -- INPUT --
    # p: Particle
    # dt: timestep
    # ts: number of timesteps
    i = 0
    while i<ts && abs(p.pos[1])<5.5
        time_evolution_extended(p,dt)
        for j = 1:50
            update_density_coefficients(p,dt/50)
        end
        switch_surface(p,dt)
        i += 1
    end
end

function external_save(path_to_file::String,
        k_list,
        n_iter::Int,
        delta_t,
        n_test::Int,
        res,
        package_time::Int,
        import_time::Int,
        dynamics_time::Union{Int,Float64},
        total_time::Int)
        t = 0
        io = open(path_to_file, "a")
    println(io, "-------------------------------------------------------")
    rightnow = Dates.now()
    println(io, "This file was generated on: ", rightnow)
    println(io, "")
    println(io, "This is a report of time spent by the SH algorithm")
    println(io, "In this simulation we simulated the dynamics at k = $k_list")
    println(io, "The results we got are $res")
    println(io, "We used $n_iter number of iterations for the dynamics with a time step of $delta_t seconds a.u.")
    println(io, "For every k we used a total of $n_test trajectories.")
    println(io, "-------------------------------------------------------")
    println(io, "\n")
    println(io, "==========================================================================================")
    println(io, "|            Task                      |          Time          |       Percentage       |")
    println(io, "|-----------------------------------------------------------------------------------------")
    println(io, "| Initializing the variables           |$(@sprintf("%23.0f", package_time)) |   $(@sprintf("%1.17f", package_time/total_time))% |")
    println(io, "| Importing the packages               |$(@sprintf("%23.0f", import_time)) |   $(@sprintf("%1.17f", import_time/total_time))% |")
    println(io, "| Running the dynamics                 |$(@sprintf("%23.0f", dynamics_time)) |   $(@sprintf("%1.17f", dynamics_time/total_time))% |")
    println(io, "| Switching and adjusting the momentum |$(@sprintf("%23.0f", t)) |   $(@sprintf("%1.17f", t/total_time))% |")
    println(io, "| NAC terms evaluation                 |$(@sprintf("%23.0f", t)) |   $(@sprintf("%1.17f", t/total_time))% |")
    println(io, "|-----------------------------------------------------------------------------------------")
    println(io, "|                                      |$(@sprintf("%23.0f", total_time)) |   $(@sprintf("%1.17f", total_time/total_time))  | ")
    println(io, "==========================================================================================")
    close(io)
end
