using Pkg
Pkg.activate("../")

using Dierckx
using LinearAlgebra
using Polynomials

# C = parse(Float64,ARGS[1])
C = 0.00001

start = -10
stop = 10
l = 100000
x = range(start,stop=stop,length=l)
dx = (stop-start)/l

function create_nac(Pot, der_Pot, x, C, size::Int=2, delta_x=0.001)
    R = [0 -1; 1 0]
    # create the empty variables
    E = zeros(length(x),size)
    ev1 = zeros(length(x),size)
    ev2 = zeros(length(x),size)
    ev1_b = zeros(length(x),size)
    ev1_a = zeros(length(x),size)
    ev2_b = zeros(length(x),size)
    ev2_a = zeros(length(x),size)
    nac_12 = zeros(length(x))
    nac_11 = zeros(length(x))
    nac_22 = zeros(length(x))
    for i in 1:length(x)
        # y[i] = nac(x[i],1,2)[1]
        E[i,:] = eigvals(Pot(x[i],C))[:]
        ev1[i,:] = eigvecs(Pot(x[i],C))[1,:]
        ev2[i,:] = eigvecs(Pot(x[i],C))[2,:]
        ev1_b[i,:] = eigvecs(Pot(x[i]-delta_x,C))[1,:]
        ev2_b[i,:] = eigvecs(Pot(x[i]-delta_x,C))[2,:]
        ev1_a[i,:] = eigvecs(Pot(x[i]+delta_x,C))[1,:]
        ev2_a[i,:] = eigvecs(Pot(x[i]+delta_x,C))[2,:]
        if i ==1 # line 23
            nac_12[i] = -conj(ev1[i,:])'*der_Pot(x[i],C)*ev2[i,:]/(E[i,2]-E[i,1])
            if nac_12[i] < 0
                ev1[i,:] *= -1
            end
            continue
        end
        if abs(1-sum(ev1[i,:].*ev1[i-1,:]))>0.2
            ev1[i,:] *= -1
        end
        if abs(1-sum(ev2[i,:].*ev2[i-1,:]))>0.2
            ev2[i,:] *= -1
        end
        if abs(1-sum(ev1_a[i,:].*ev1_a[i-1,:]))>0.2
            ev1_a[i,:] *= -1
        end
        if abs(1-sum(ev2_a[i,:].*ev2_a[i-1,:]))>0.2
            ev2_a[i,:] *= -1
        end
        if abs(1-sum(ev1_b[i,:].*ev1_b[i-1,:]))>0.2
            ev1_b[i,:] *= -1
        end
        if abs(1-sum(ev2_b[i,:].*ev2_b[i-1,:]))>0.2
            ev2_b[i,:] *= -1
        end
        nac_12[i] = -conj(ev1[i,:])'*der_Pot(x[i],C)*ev2[i,:]/(E[i,2]-E[i,1])
        t_11_a = sum(conj(ev1[i,:])'.*ev1_a[i,:])
        t_11_b = sum(conj(ev1[i,:])'.*ev1_b[i,:])
        nac_11[i] = (t_11_a-t_11_b)/2delta_x
        t_22_a = sum(conj(ev2[i,:])'.*ev2_a[i,:])
        t_22_b = sum(conj(ev2[i,:])'.*ev2_b[i,:])
        nac_22[i] = (t_22_a-t_22_b)/2delta_x
    end
    return nac_12, nac_11, nac_22, E
end

function Simple_avoided_crossing(pos::Real,C::Real)::Array{Float64,2}
    x = pos
    A = 0.01
    B = 1.6
    D = 1.0
    V = zeros(2,2)
    if x>0
        V[1,1] = A*(1-exp(-B*x))
    else
        V[1,1] = -A*(1-exp(B*x))
    end
    V[2,2] = -V[1,1]
    V[1,2] = C*exp(-D*x^2)
    V[2,1] = V[1,2]
    return V
end

function dV_Simple_avoided_crossing(x::Real,C::Real)::Array{Real,2}
    A = 0.01
    B = 1.6
    D = 1.0
    dV = zeros(2,2)
    if x>0
        dV[1,1] = A*B*(exp(-B*x))
    else
        dV[1,1] = A*B*(exp(B*x))
    end
    dV[2,2] = -dV[1,1]
    dV[1,2] = dV[2,1] = -2x*C*D*exp(-D*x^2)
    return dV
end

d_12, d_11, d_22, E = create_nac(Simple_avoided_crossing,dV_Simple_avoided_crossing,x,C)
#d_12, d_11, d_22, E = create_nac(Extended_coupling,dV_Extended_coupling,x)
d_12_interp = Spline1D(x, d_12)
d_11_interp = Spline1D(x, d_11)
d_22_interp = Spline1D(x, d_22)
E1 = Spline1D(x, E[:,1])
E2 = Spline1D(x, E[:,2])

using JLD2

JLD2.@save "Simple_avoided_crossing.jld" d_12_interp d_11_interp d_22_interp E1 E2
