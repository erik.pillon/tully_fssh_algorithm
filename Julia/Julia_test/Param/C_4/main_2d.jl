# main_2d.jl

#
# “God does not care about our mathematical difficulties. He integrates empirically.”
# Albert Einstein
#

using Pkg
Pkg.activate("../")

# -----------------------------
using Dates
t0 = Dates.now()
using Printf
using LinearAlgebra
using Random
using MPI
using Dierckx
using Polynomials
using JLD2
using JLD
t1 = Dates.now()
# -----------------------------
println("--- Script started ---")
include("particle_methods_2d.jl")
t2 = Dates.now()
println("--- methods imported correctly ---")
# -----------------------------

dt = 0.1
hbar = 1

# Possible systems are
# - Dual_avoided_crossing
# - Simple_avoided_crossing
# - Extended_coupling

system = "/Simple_avoided_crossing.jld"

s = pwd()*system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

#  :d_12_interp
#  :d_11_interp
#  :d_22_interp
#  :E1
#  :E2

function main()
    n_time_steps = 40000
    dt = 0.1
    n_test = 2000;
    k_range = [7, 8, 8.5, 8.75, 8.85, 9, 9.5, 10, 11, 12, 13, 14, 15, 17.5, 20, 25, 30]
    # every value below 8.9 should be zero
    MPI.Init()

    time_actual_algorithm = 0
    t3_cumulative = 0

    comm = MPI.COMM_WORLD
    size = MPI.Comm_size(comm)
    rank = MPI.Comm_rank(comm)
    transm = []
    t3_total = 0
    t3 = Dates.now()

    for k in k_range
        println("started to analysze k=",k)
        count = 0
        for i in 1:n_test
            if !(mod(i,size)==rank); continue end
            p = Particle([-5],[k],1,0,1,2000)
            run_dynamics(p,dt,n_time_steps)
            if p.surface == 2
                count += 1
            end
            t3_end = Dates.now()
            t3_total += Dates.value(Dates.Millisecond(t3_end-t3))
        end
        total_count = MPI.Reduce(count,MPI.SUM,0,comm)

        if rank == 0
            push!(transm,total_count/n_test)
            print(transm)
        end
    end

    sleep(rank*2)

    time_actual_algorithm = MPI.Reduce(t3_total,MPI.SUM,0,comm)

    MPI.Barrier(comm)
    MPI.Finalize()

    t_final = Dates.now()

end

main()
