# print_multiple_graphs.jl

using Plots
using Dierckx
using Polynomials
using JLD2
using JLD

system = "/Simple_avoided_crossing.jld"

s = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Julia_test/Param/C_5"*system
#s = system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

# ================= FIRST PLOT =================
x = range(-5,stop=5, length=5000)
y1 = E1(x)
y2 = E2(x)
y3 = d_12_interp(x)

p1 = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates")
plot!(x,y2,label = "E2")
plot!(x,y3/50,
    line = (:dash, :green),
    label="d_12/50")
# ==============================================

# ================= SECOND PLOT =================
p2 = plot(xlabel="Energy (a.u.)",
    ylabel="Transmission Probability",
    tickfontsize = 10,
    legend=:bottomright)

# -----------------------------------------------
# main_2d.out
k_range = [7, 8, 8.5, 8.75, 8.85, 9, 9.5, 10, 11, 12, 13, 14, 15, 17.5, 20, 25, 30]
transm = [0.0, 0.0965, 0.1265, 0.1475, 0.1505, 0.1285, 0.1535, 0.1795, 0.2055, 0.249,
    0.281, 0.329, 0.3645, 0.4305, 0.487, 0.621, 0.703]
plot!(k_range, transm,
    line = (:purple),
    linewidth = 2,
    markersize = 5,
    markershape=:diamond,
    markercolor=:purple,
    markerstrokecolor = :purple,
    label="Standard FSSH")
# plot!(k_range, transm, seriestype = :scatter,label="")
# -----------------------------------------------
# modified_wrong_Ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.053925595758428695, 0.07401492651661745, 0.09279956412970787,
    0.1108002879812401, 0.12844316932850475, 0.16373674292600912, 0.1998902078015727,
    0.23706375996469448, 0.3128566207061094, 0.4047790076379365, 0.4879127681418985,
    0.6205539492622144, 0.7136688674561329]
plot!(k_range, transm,
    line = (:blue),
    linewidth = 2,
    markershape=:utriangle,
    markersize = 5,
    markercolor=:blue,
    markerstrokecolor = :blue,
    label="Standard Ehrenfest")
# plot!(k_range, transm, seriestype = :scatter,label="")

fig = plot(p1,p2,
    layout=grid(2,1,
    heights=[0.4, 0.6]),
    size = (800,800),
    tickfontsize=12,
    yguidefontsize=10,
    legendfontsize=10)

path = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Julia_test/Param/"
name = "C_5/Result_5C"
savefig(fig, path*name*"standard_results")

p2 = plot(xlabel="Energy (a.u.)",
    ylabel="Transmission Probability",
    tickfontsize = 10,
    legend=:bottomright)

# -----------------------------------------------
# main_2d.out
k_range = [7, 8, 8.5, 8.75, 8.85, 9, 9.5, 10, 11, 12, 13, 14, 15, 17.5, 20, 25, 30]
transm = [0.0, 0.0965, 0.1265, 0.1475, 0.1505, 0.1285, 0.1535, 0.1795, 0.2055, 0.249,
    0.281, 0.329, 0.3645, 0.4305, 0.487, 0.621, 0.703]
plot!(k_range, transm,
    line = (:purple),
    linewidth = 2,
    markersize = 5,
    markershape=:diamond,
    markercolor=:purple,
    markerstrokecolor = :purple,
    label="Standard FSSH")
# plot!(k_range, transm, seriestype = :scatter,label="")
# -----------------------------------------------
# modified_wrong_Ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.053925595758428695, 0.07401492651661745, 0.09279956412970787,
    0.1108002879812401, 0.12844316932850475, 0.16373674292600912, 0.1998902078015727,
    0.23706375996469448, 0.3128566207061094, 0.4047790076379365, 0.4879127681418985,
    0.6205539492622144, 0.7136688674561329]
plot!(k_range, transm,
    line = (:blue),
    linewidth = 2,
    markershape=:utriangle,
    markersize = 5,
    markercolor=:blue,
    markerstrokecolor = :blue,
    label="Standard Ehrenfest")
# -----------------------------------------------
# simple_ehrenfest.jl
# simple_ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
t = [0.3565087612592787, 0.37760534205097335, 0.39649317170211473, 0.41453367405344216,
    0.4320713052535279, 0.46641591370102, 0.5001672687193351, 0.5331512459661875,
    0.5951771270107584, 0.6625474770959036, 0.7178721251624632, 0.7983546667292009, 0.8507789817122509]
transm = [0.05392628031760849, 0.07397070321454782, 0.09271895019969759, 0.11068955727259193,
    0.12830628666642255, 0.16355732077331167, 0.19967417861977718, 0.2368151202998301,
    0.31255378043797316, 0.4044224018284189, 0.4875214258324813, 0.6201297429987204, 0.7132266628581817]
plot!(k_range, t.^2,
    line = (:red),
    linewidth = 2,
    markershape=:x,
    markersize = 5,
    markercolor=:red,
    markerstrokecolor=:red,
    label="Ehrenfest (CG)")
# plot!(k_range, t.^2, seriestype = :scatter, label="")
# ------------------------------------------------------------------------------
# extended_ehrenfest_erik.out
k_range = [5, 6, 7, 8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [2.4800554031108367e-6, 0.0005304055241285503, 0.0033952576618415673,
    0.040225828460009584, 0.059251730761522625, 0.07752292165600702, 0.09535389705723328,
    0.1130685267803067, 0.1490150097302425, 0.1862645607785577, 0.22477335124059614,
    0.3033903614179563, 0.39836190021975854, 0.4836376432470471, 0.618568570527625,
    0.7125619747802961]
plot!(k_range, transm,
    line = (:orange),
    linewidth = 2,
    markershape=:x,
    markersize = 5,
    markercolor=:orange,
    markerstrokecolor = :orange,
    label="Ext Ehrenfest")
# ===============================================

# ------------------------------------------------------------------------------
# standard_ehrenfest_erik.out
k_range = [4.0, 5.0, 6.0, 7.0, 8.0, 8.5, 9.0, 9.5, 10.0, 11.0, 12.0,
    13.0, 15.0, 17.5, 20.0, 25.0, 30.0]
transm = [3.0690625522226084e-10, 2.4800554031108367e-6, 0.0005304055241285503,
    0.0033952576618415673, 0.040225828460009584,
    0.059251730761522625, 0.07752292165600702, 0.09535389705723328,
    0.1130685267803067, 0.1490150097302425, 0.1862645607785577,
    0.22477335124059614, 0.3033903614179563, 0.39836190021975854,
    0.4836376432470471, 0.618568570527625, 0.7125619747802961]
plot!(k_range, transm,
    line = (:orange),
    linewidth = 2,
    markershape=:o,
    markersize = 5,
    markercolor=:brown,
    markerstrokecolor = :brown,
    label="Standard Ehrenfest Erik")
# ===============================================


fig = plot(p1,p2,
    layout=grid(2,1,
    heights=[0.4, 0.6]),
    size = (800,800),
    tickfontsize=12,
    yguidefontsize=10,
    legendfontsize=10)

path = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Julia_test/Param/"
name = "C_5/Result_5C"
savefig(fig, path*name)

# ==============================================================================
p2 = plot(xlabel="Energy (a.u.)",
    ylabel="Transmission Probability",
    tickfontsize = 10,
    legend=:bottomright)

# -----------------------------------------------
# main_2d.out
k_range = [7, 8, 8.5, 8.75, 8.85, 9, 9.5, 10, 11, 12, 13]
transm = [0.0, 0.0965, 0.1265, 0.1475, 0.1505, 0.1285, 0.1535, 0.1795, 0.2055, 0.249,
    0.281]
plot!(k_range, transm,
    line = (:purple),
    linewidth = 2,
    markersize = 5,
    markershape=:diamond,
    markercolor=:purple,
    markerstrokecolor = :purple,
    label="Standard FSSH")
# plot!(k_range, transm, seriestype = :scatter,label="")
# -----------------------------------------------
# modified_wrong_Ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13]
transm = [0.053925595758428695, 0.07401492651661745, 0.09279956412970787,
    0.1108002879812401, 0.12844316932850475, 0.16373674292600912, 0.1998902078015727,
    0.23706375996469448]
plot!(k_range, transm,
    line = (:blue),
    linewidth = 2,
    markershape=:utriangle,
    markersize = 5,
    markercolor=:blue,
    markerstrokecolor = :blue,
    label="Standard Ehrenfest")
# -----------------------------------------------
# simple_ehrenfest.jl
# simple_ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13]
t = [0.3565087612592787, 0.37760534205097335, 0.39649317170211473, 0.41453367405344216,
    0.4320713052535279, 0.46641591370102, 0.5001672687193351, 0.5331512459661875]
transm = [0.05392628031760849, 0.07397070321454782, 0.09271895019969759, 0.11068955727259193,
    0.12830628666642255, 0.16355732077331167, 0.19967417861977718, 0.2368151202998301,
    0.31255378043797316, 0.4044224018284189, 0.4875214258324813, 0.6201297429987204, 0.7132266628581817]
plot!(k_range, t.^2,
    line = (:red),
    linewidth = 2,
    markershape=:x,
    markersize = 5,
    markercolor=:red,
    markerstrokecolor=:red,
    label="Ehrenfest (CG)")
# plot!(k_range, t.^2, seriestype = :scatter, label="")
# ------------------------------------------------------------------------------
# extended_ehrenfest_erik.out
k_range = [7, 8, 8.5, 9, 9.5, 10, 11, 12, 13]
transm = [0.0033952576618415673,
    0.040225828460009584, 0.059251730761522625, 0.07752292165600702, 0.09535389705723328,
    0.1130685267803067, 0.1490150097302425, 0.1862645607785577, 0.2247733512405961]
plot!(k_range, transm,
    line = (:orange),
    linewidth = 2,
    markershape=:x,
    markersize = 5,
    markercolor=:orange,
    markerstrokecolor = :orange,
    label="Ext Ehrenfest")
# ===============================================

fig = p2

path = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Julia_test/Param/"
name = "C_5/Result_5C"
savefig(fig, path*name*"Zoomed")
