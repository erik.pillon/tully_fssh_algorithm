# print_multiple_graphs.jl

using Plots
using Dierckx
using Polynomials
using JLD2
using JLD

system = "/Simple_avoided_crossing.jld"

s = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Julia_test/Param/C_1"*system
#s = system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

# ================= FIRST PLOT =================
x = range(-5,stop=5, length=5000)
y1 = E1(x)
y2 = E2(x)
y3 = d_12_interp(x)

p1 = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates")
plot!(x,y2,label = "E2")
plot!(x,y3/200,
    line = (:dash, :green),
    label="d_12/200")
# ==============================================

# ================= SECOND PLOT =================
p2 = plot(xlabel="Energy (a.u.)",
    ylabel="Transmission Probability",
    tickfontsize = 10,
    legend=:bottomright)

# -----------------------------------------------
# main_2d.out
k_range = [7, 8, 8.5, 8.75, 8.85, 9, 9.5, 10, 11, 12]
transm = [0.0355, 0.4065, 0.9265, 0.915, 0.9165, 0.928, 0.946, 0.947, 0.9455, 0.9435]
plot!(k_range, transm,
    line = (:purple),
    linewidth = 2,
    markersize = 5,
    markershape=:diamond,
    markercolor=:purple,
    markerstrokecolor = :purple,
    label="Standard FSSH")
# plot!(k_range, transm, seriestype = :scatter,label="")
# -----------------------------------------------
# # extended_FSSH.out
# k_range = [7, 8, 8.5, 8.75, 8.85, 9, 9.5, 10, 11, 12, 13, 14, 15, 17.5, 20, 25, 30]
# transm = [0.0, 0.0, 0.0, 0.044, 0.0745, 0.1065, 0.112, 0.1205, 0.1705, 0.187, 0.235, 0.2515, 0.3055, 0.3955, 0.463, 0.593, 0.7115]
# plot!(k_range, transm,
#     line = (:green),
#     linewidth = 2,
#     markershape=:diamond,
#     markersize = 5,
#     markercolor=:green,
#     markerstrokecolor = :green,
#     label="Extended FSSH")
# # plot!(k_range, transm, seriestype = :scatter,label="")
# -----------------------------------------------
# standard_ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.04762949215203186, 0.11568798359382607, 0.837903862927201,
    0.8906445405171883, 0.9100771221439916, 0.9292928535891912,
    0.9400090490358284, 0.9474661451017246, 0.9580379296728384, 0.9671824699496673,
    0.9737328253436792, 0.9822725319771177, 0.9873645880772115]
plot!(k_range, transm,
    line = (:blue),
    linewidth = 2,
    markershape=:utriangle,
    markersize = 5,
    markercolor=:blue,
    markerstrokecolor = :blue,
    label="Standard Ehrenfest")
# plot!(k_range, transm, seriestype = :scatter,label="")

fig = plot(p1,p2,
    layout=grid(2,1,
    heights=[0.4, 0.6]),
    size = (800,800),
    tickfontsize=12,
    yguidefontsize=10,
    legendfontsize=10)

path = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Julia_test/Param/"
name = "C_1/Result_1C"
savefig(fig, path*name*"standard_results")

p2 = plot(xlabel="Energy (a.u.)",
    ylabel="Transmission Probability",
    tickfontsize = 10,
    legend=:bottomright)

# -----------------------------------------------
# main_2d.out
k_range = [7, 8, 8.5, 8.75, 8.85, 9, 9.5, 10, 11, 12]
transm = [0.0355, 0.4065, 0.9265, 0.915, 0.9165, 0.928, 0.946, 0.947, 0.9455, 0.9435]
plot!(k_range, transm,
    line = (:purple),
    linewidth = 2,
    markersize = 5,
    markershape=:diamond,
    markercolor=:purple,
    markerstrokecolor = :purple,
    label="Standard FSSH")
# plot!(k_range, transm, seriestype = :scatter,label="")
# -----------------------------------------------
# standard_ehrenfest.out
k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.04762949215203186, 0.11568798359382607, 0.837903862927201,
    0.8906445405171883, 0.9100771221439916, 0.9292928535891912,
    0.9400090490358284, 0.9474661451017246, 0.9580379296728384, 0.9671824699496673,
    0.9737328253436792, 0.9822725319771177, 0.9873645880772115]
plot!(k_range, transm,
    line = (:blue),
    linewidth = 2,
    markershape=:utriangle,
    markersize = 5,
    markercolor=:blue,
    markerstrokecolor = :blue,
    label="Standard Ehrenfest")
# -----------------------------------------------
# simple_ehrenfest.jl
# simple_ehrenfest.out
k_range = [4, 5, 6, 7, 8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [0.02925323951465926, 0.8416843219226321, 0.8950363848316354, 0.9158812343677146,
    0.9281735714486726, 0.932781885550018, 0.9367492000948326, 0.9402404127791003,
    0.9433673054010557, 0.9488159734282652, 0.953482805625873, 0.9575716364300159,
    0.9644416612230877, 0.9711630652786463, 0.9763352626174024, 0.9835004901141899, 0.9880079614537666]
plot!(k_range, transm,
    line = (:red),
    linewidth = 2,
    markershape=:+,
    markersize = 5,
    markercolor=:red,
    markerstrokecolor=:green,
    label="Ext Ehrenfest (CG)")
# plot!(k_range, t.^2, seriestype = :scatter, label="")
# -----------------------------------------------
# extended_ehrenfest_erik.out
k_range = [2,3,4,5,6,7,8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
transm = [9.349221298322534e-25, 3.277328519803152e-15, 1.1520383029951705e-9,
    2.0706032813108688e-6, 0.00024509807622277843, 0.005691972644980824,
    0.04362507666998038, 0.086810843493735, 0.1694618312854264,
    0.8751963330933739, 0.9043256406942378, 0.9271001613517449,
    0.9388180423694867, 0.9467550104910746, 0.9577663624409573, 0.9670988154699595,
    0.9737110222459513, 0.98227497588535, 0.9873687645362723]
plot!(k_range, transm,
    line = (:orange),
    linewidth = 2,
    markershape=:x,
    markersize = 5,
    markercolor=:orange,
    markerstrokecolor = :orange,
    label="Ext Ehrenfest")
# ===============================================

fig = plot(p1,p2,
    layout=grid(2,1,
    heights=[0.4, 0.6]),
    size = (800,800),
    tickfontsize=12,
    yguidefontsize=10,
    legendfontsize=10)

path = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Julia_test/Param/"
name = "C_1/Result_1C"
savefig(fig, path*name)
