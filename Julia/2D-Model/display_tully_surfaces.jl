using LinearAlgebra
using Plots
using Dierckx
using LaTeXStrings
using JLD2

@load "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Simple_avoided_crossing.jld"

# 5-element Array{Symbol,1}:
#  :d_12_interp
#  :d_11_interp
#  :d_22_interp
#  :E1
#  :E2

x = range(-5,stop=5, length=1000)

p = plot(x,E1(x))
p = plot!(x,E2(x))
p = plot!(x,d_12_interp(x)/50)

display(p)
println("for ending the script, press any button")
readline()
