{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tully's Fewest Surface Hopping Algorithm\n",
    "This code implement the Tully's Fewest Surface Hopping Algorithm as introduced in the article Tully, J.C. *J. Chem. Phys.* (**1990**) 93 1061."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simulation is performed taking care of the following parameters:\n",
    "- **pos**: position of the particle (float)\n",
    "- **k**: momentum of the particle\n",
    "- $c_1$: coefficient of \"how much\" the particle is smeared on the first surface and how much is on the second surface. $c_1^2 + c_2^2 = 1 $. \n",
    "- **surface**: can take value 1 or 2. Denotes the surface in which the particle is moving.\n",
    "- **m**: mass of the system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# -----------------------------\n",
    "using LinearAlgebra\n",
    "using Plots\n",
    "using Random\n",
    "# -----------------------------"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Useful constants\n",
    "hbar = 1;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we define the potential $V$ as in the erticle of Tully. In general, $V$ is given by the following formula\n",
    "\n",
    "$$V_{ij}=<\\phi_{i}|\\hat{H}_{elec}|\\phi_{j}> $$\n",
    "\n",
    "but according to the article of Tully (example 1) the potential we will use is already parametrized in a simple way."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following potential is the Potential test selected in the original paper of Tully."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Potential (generic function with 1 method)"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function Potential(x)::Array{Float64,2}\n",
    "    A = 0.01\n",
    "    B = 1.6\n",
    "    C = 0.005\n",
    "    D = 1.0\n",
    "    V = zeros(2,2)\n",
    "    if x>0\n",
    "        V[1,1] = A*(1-exp(-B*x))\n",
    "    else\n",
    "        V[1,1] = -A*(1-exp(B*x))\n",
    "    end\n",
    "    V[2,2] = -V[1,1]\n",
    "    V[1,2] = C*exp(-D*x^2)\n",
    "    V[2,1] = V[1,2]\n",
    "    return V\n",
    "end\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "potential_energy (generic function with 1 method)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function init(k=10, pos=-5, c1=1, surface=1, m=2000)\n",
    "    return pos, k, c1, surface, m\n",
    "end\n",
    "\n",
    "function kinetic_energy(vel::Real, m::Real)::Float64\n",
    "    return vel*vel*m/2\n",
    "end\n",
    "\n",
    "function total_energy(pos::Real, vel::Real, surface::Int,m=2000)::Float64\n",
    "    return potential_energy(pos,surface)+kinetic_energy(vel,m)\n",
    "end\n",
    "\n",
    "function potential_energy(pos::Real, surface::Int)::Float64\n",
    "    return eigvals(Potential(pos))[surface]\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.09483365576574734"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "potential_energy(0.1,1)\n",
    "kinetic_energy(0.01, 2000)\n",
    "total_energy(0.1,0.01,1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "der_surface (generic function with 2 methods)"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function der_surface(pos, surface=1)\n",
    "    # This function is evaluating the gradient of the \n",
    "    # potential energy surface.\n",
    "    dx = 0.001\n",
    "    dim = length(pos)\n",
    "    der = zeros(dim)\n",
    "    for count in 1:dim\n",
    "        ev1 = eigvals(Potential(pos[count]-dx))[surface]\n",
    "        ev2 = eigvals(Potential(pos[count]+dx))[surface]\n",
    "        der[count] = (ev2-ev1)/2/dx\n",
    "    end\n",
    "    return der\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Function `nac` evaluates the non-adiabatic coupling coefficients. In general the formula is given by \n",
    "\n",
    "$$d_{ij}(\\bf{R})=\\langle\\phi_i(\\bf{R})|\\nabla_{\\bf{R}}\\phi_j(\\bf{R})\\rangle$$\n",
    "\n",
    "The parameters of the function `nac` are of course:\n",
    "- `pos`: $\\bf{R}$\n",
    "- `i,j`: surfaces $i,j$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "nac (generic function with 2 methods)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function nac(pos::Real, i::Int64, j::Int64)\n",
    "    # function for the evaluation of the non adiabatic coupling vector d_ij\n",
    "    # non-adiabatic vector cupling = Connection (= vector potential)\n",
    "    # i and j can take ony values 1 and 2\n",
    "    dim = length(pos);\n",
    "    dx = 0.001;\n",
    "    nac = zeros(dim);\n",
    "    for count in 1:dim\n",
    "        e_vec1 = eigvecs(Potential(pos[count]))[:,j];\n",
    "        e_vec2_1 = eigvecs(Potential(pos[count]-dx))[:,i];\n",
    "        e_vec2_2 = eigvecs(Potential(pos[count]+dx))[:,i];\n",
    "        der = (e_vec2_2-e_vec2_1)/2/dx;\n",
    "        nac[count] = sum(conj(e_vec1).*der)\n",
    "    end\n",
    "    return nac\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = range(-5,5,length=100)\n",
    "y = zeros(length(x))\n",
    "ev = zeros(length(x),2)\n",
    "for i in 1:length(x)\n",
    "    y[i] = nac(x[i],1,2)[1]\n",
    "    ev[i,:]=eigvals(Potential(x[i]))[:]\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/svg+xml": [
       "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n",
       "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" width=\"600\" height=\"400\" viewBox=\"0 0 2400 1600\">\n",
       "<defs>\n",
       "  <clipPath id=\"clip0600\">\n",
       "    <rect x=\"0\" y=\"0\" width=\"2400\" height=\"1600\"/>\n",
       "  </clipPath>\n",
       "</defs>\n",
       "<path clip-path=\"url(#clip0600)\" d=\"\n",
       "M0 1600 L2400 1600 L2400 0 L0 0  Z\n",
       "  \" fill=\"#ffffff\" fill-rule=\"evenodd\" fill-opacity=\"1\"/>\n",
       "<defs>\n",
       "  <clipPath id=\"clip0601\">\n",
       "    <rect x=\"480\" y=\"0\" width=\"1681\" height=\"1600\"/>\n",
       "  </clipPath>\n",
       "</defs>\n",
       "<path clip-path=\"url(#clip0600)\" d=\"\n",
       "M270.627 1425.62 L2352.76 1425.62 L2352.76 47.2441 L270.627 47.2441  Z\n",
       "  \" fill=\"#ffffff\" fill-rule=\"evenodd\" fill-opacity=\"1\"/>\n",
       "<defs>\n",
       "  <clipPath id=\"clip0602\">\n",
       "    <rect x=\"270\" y=\"47\" width=\"2083\" height=\"1379\"/>\n",
       "  </clipPath>\n",
       "</defs>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  525.982,1425.62 525.982,47.2441 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  918.837,1425.62 918.837,47.2441 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  1311.69,1425.62 1311.69,47.2441 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  1704.55,1425.62 1704.55,47.2441 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  2097.4,1425.62 2097.4,47.2441 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  270.627,1386.72 2352.76,1386.72 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  270.627,1053.65 2352.76,1053.65 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  270.627,720.579 2352.76,720.579 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  270.627,387.508 2352.76,387.508 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#000000; stroke-width:2; stroke-opacity:0.1; fill:none\" points=\"\n",
       "  270.627,54.4375 2352.76,54.4375 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  270.627,1425.62 2352.76,1425.62 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  270.627,1425.62 270.627,47.2441 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  525.982,1425.62 525.982,1409.08 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  918.837,1425.62 918.837,1409.08 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  1311.69,1425.62 1311.69,1409.08 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  1704.55,1425.62 1704.55,1409.08 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  2097.4,1425.62 2097.4,1409.08 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  270.627,1386.72 295.612,1386.72 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  270.627,1053.65 295.612,1053.65 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  270.627,720.579 295.612,720.579 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  270.627,387.508 295.612,387.508 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  270.627,54.4375 295.612,54.4375 \n",
       "  \"/>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:middle;\" transform=\"rotate(0, 525.982, 1479.62)\" x=\"525.982\" y=\"1479.62\">-4</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:middle;\" transform=\"rotate(0, 918.837, 1479.62)\" x=\"918.837\" y=\"1479.62\">-2</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:middle;\" transform=\"rotate(0, 1311.69, 1479.62)\" x=\"1311.69\" y=\"1479.62\">0</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:middle;\" transform=\"rotate(0, 1704.55, 1479.62)\" x=\"1704.55\" y=\"1479.62\">2</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:middle;\" transform=\"rotate(0, 2097.4, 1479.62)\" x=\"2097.4\" y=\"1479.62\">4</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:end;\" transform=\"rotate(0, 246.627, 1404.22)\" x=\"246.627\" y=\"1404.22\">-0.01</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:end;\" transform=\"rotate(0, 246.627, 1071.15)\" x=\"246.627\" y=\"1071.15\">0.00</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:end;\" transform=\"rotate(0, 246.627, 738.079)\" x=\"246.627\" y=\"738.079\">0.01</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:end;\" transform=\"rotate(0, 246.627, 405.008)\" x=\"246.627\" y=\"405.008\">0.02</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:end;\" transform=\"rotate(0, 246.627, 71.9375)\" x=\"246.627\" y=\"71.9375\">0.03</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:66px; text-anchor:middle;\" transform=\"rotate(0, 1311.69, 1559.48)\" x=\"1311.69\" y=\"1559.48\">Nuclear coordinates</text>\n",
       "</g>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:66px; text-anchor:middle;\" transform=\"rotate(-90, 89.2861, 736.431)\" x=\"89.2861\" y=\"736.431\">Strength</text>\n",
       "</g>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#009af9; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  329.555,1053.65 349.396,1053.65 369.237,1053.65 389.078,1053.65 408.919,1053.65 428.76,1053.65 448.602,1053.65 468.443,1053.65 488.284,1053.65 508.125,1053.65 \n",
       "  527.966,1053.65 547.807,1053.65 567.648,1053.65 587.49,1053.65 607.331,1053.65 627.172,1053.64 647.013,1053.64 666.854,1053.63 686.695,1053.61 706.536,1053.57 \n",
       "  726.378,1053.51 746.219,1053.4 766.06,1053.23 785.901,1052.95 805.742,1052.5 825.583,1051.8 845.424,1050.74 865.266,1049.17 885.107,1046.89 904.948,1043.68 \n",
       "  924.789,1039.24 944.63,1033.25 964.471,1025.38 984.312,1015.27 1004.15,1002.6 1023.99,987.103 1043.84,968.618 1063.68,947.114 1083.52,922.717 1103.36,895.695 \n",
       "  1123.2,866.385 1143.04,835.011 1162.88,801.343 1182.72,764.157 1202.56,720.446 1222.41,664.325 1242.25,585.679 1262.09,469.367 1281.93,299.711 1301.77,86.2547 \n",
       "  1321.61,86.2547 1341.45,299.711 1361.29,469.367 1381.14,585.679 1400.98,664.325 1420.82,720.446 1440.66,764.157 1460.5,801.343 1480.34,835.011 1500.18,866.385 \n",
       "  1520.02,895.695 1539.86,922.717 1559.71,947.114 1579.55,968.618 1599.39,987.103 1619.23,1002.6 1639.07,1015.27 1658.91,1025.38 1678.75,1033.25 1698.59,1039.24 \n",
       "  1718.43,1043.68 1738.28,1046.89 1758.12,1049.17 1777.96,1050.74 1797.8,1051.8 1817.64,1052.5 1837.48,1052.95 1857.32,1053.23 1877.16,1053.4 1897,1053.51 \n",
       "  1916.85,1053.57 1936.69,1053.61 1956.53,1053.63 1976.37,1053.64 1996.21,1053.64 2016.05,1053.65 2035.89,1053.65 2055.73,1053.65 2075.58,1053.65 2095.42,1053.65 \n",
       "  2115.26,1053.65 2135.1,1053.65 2154.94,1053.65 2174.78,1053.65 2194.62,1053.65 2214.46,1053.65 2234.3,1053.65 2254.15,1053.65 2273.99,1053.65 2293.83,1053.65 \n",
       "  \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#e26f46; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  329.555,1386.61 349.396,1386.59 369.237,1386.57 389.078,1386.54 408.919,1386.51 428.76,1386.47 448.602,1386.43 468.443,1386.37 488.284,1386.31 508.125,1386.24 \n",
       "  527.966,1386.16 547.807,1386.06 567.648,1385.94 587.49,1385.81 607.331,1385.65 627.172,1385.46 647.013,1385.24 666.854,1384.98 686.695,1384.67 706.536,1384.31 \n",
       "  726.378,1383.89 746.219,1383.39 766.06,1382.81 785.901,1382.12 805.742,1381.32 825.583,1380.37 845.424,1379.25 865.266,1377.95 885.107,1376.41 904.948,1374.6 \n",
       "  924.789,1372.49 944.63,1370.01 964.471,1367.12 984.312,1363.75 1004.15,1359.85 1023.99,1355.38 1043.84,1350.27 1063.68,1344.52 1083.52,1338.1 1103.36,1331.01 \n",
       "  1123.2,1323.27 1143.04,1314.85 1162.88,1305.72 1182.72,1295.76 1202.56,1284.8 1222.41,1272.68 1242.25,1259.33 1262.09,1245.14 1281.93,1231.5 1301.77,1221.76 \n",
       "  1321.61,1221.76 1341.45,1231.5 1361.29,1245.14 1381.14,1259.33 1400.98,1272.68 1420.82,1284.8 1440.66,1295.76 1460.5,1305.72 1480.34,1314.85 1500.18,1323.27 \n",
       "  1520.02,1331.01 1539.86,1338.1 1559.71,1344.52 1579.55,1350.27 1599.39,1355.38 1619.23,1359.85 1639.07,1363.75 1658.91,1367.12 1678.75,1370.01 1698.59,1372.49 \n",
       "  1718.43,1374.6 1738.28,1376.41 1758.12,1377.95 1777.96,1379.25 1797.8,1380.37 1817.64,1381.32 1837.48,1382.12 1857.32,1382.81 1877.16,1383.39 1897,1383.89 \n",
       "  1916.85,1384.31 1936.69,1384.67 1956.53,1384.98 1976.37,1385.24 1996.21,1385.46 2016.05,1385.65 2035.89,1385.81 2055.73,1385.94 2075.58,1386.06 2095.42,1386.16 \n",
       "  2115.26,1386.24 2135.1,1386.31 2154.94,1386.37 2174.78,1386.43 2194.62,1386.47 2214.46,1386.51 2234.3,1386.54 2254.15,1386.57 2273.99,1386.59 2293.83,1386.61 \n",
       "  \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0602)\" style=\"stroke:#3da44d; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  329.555,720.69 349.396,720.71 369.237,720.733 389.078,720.76 408.919,720.792 428.76,720.829 448.602,720.873 468.443,720.925 488.284,720.986 508.125,721.057 \n",
       "  527.966,721.141 547.807,721.24 567.648,721.356 587.49,721.492 607.331,721.652 627.172,721.841 647.013,722.062 666.854,722.322 686.695,722.628 706.536,722.987 \n",
       "  726.378,723.41 746.219,723.906 766.06,724.49 785.901,725.176 805.742,725.983 825.583,726.93 845.424,728.044 865.266,729.353 885.107,730.89 904.948,732.695 \n",
       "  924.789,734.811 944.63,737.289 964.471,740.182 984.312,743.549 1004.15,747.444 1023.99,751.922 1043.84,757.025 1063.68,762.781 1083.52,769.202 1103.36,776.288 \n",
       "  1123.2,784.033 1143.04,792.448 1162.88,801.582 1182.72,811.543 1202.56,822.497 1222.41,834.623 1242.25,847.969 1262.09,862.157 1281.93,875.799 1301.77,885.538 \n",
       "  1321.61,885.538 1341.45,875.799 1361.29,862.157 1381.14,847.969 1400.98,834.623 1420.82,822.497 1440.66,811.543 1460.5,801.582 1480.34,792.448 1500.18,784.033 \n",
       "  1520.02,776.288 1539.86,769.202 1559.71,762.781 1579.55,757.025 1599.39,751.922 1619.23,747.444 1639.07,743.549 1658.91,740.182 1678.75,737.289 1698.59,734.811 \n",
       "  1718.43,732.695 1738.28,730.89 1758.12,729.353 1777.96,728.044 1797.8,726.93 1817.64,725.983 1837.48,725.176 1857.32,724.49 1877.16,723.906 1897,723.41 \n",
       "  1916.85,722.987 1936.69,722.628 1956.53,722.322 1976.37,722.062 1996.21,721.841 2016.05,721.652 2035.89,721.492 2055.73,721.356 2075.58,721.24 2095.42,721.141 \n",
       "  2115.26,721.057 2135.1,720.986 2154.94,720.925 2174.78,720.873 2194.62,720.829 2214.46,720.792 2234.3,720.76 2254.15,720.733 2273.99,720.71 2293.83,720.69 \n",
       "  \n",
       "  \"/>\n",
       "<path clip-path=\"url(#clip0600)\" d=\"\n",
       "M1989.93 372.684 L2280.76 372.684 L2280.76 130.764 L1989.93 130.764  Z\n",
       "  \" fill=\"#ffffff\" fill-rule=\"evenodd\" fill-opacity=\"1\"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#000000; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  1989.93,372.684 2280.76,372.684 2280.76,130.764 1989.93,130.764 1989.93,372.684 \n",
       "  \"/>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#009af9; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  2013.93,191.244 2157.93,191.244 \n",
       "  \"/>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:start;\" transform=\"rotate(0, 2181.93, 208.744)\" x=\"2181.93\" y=\"208.744\">y1</text>\n",
       "</g>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#e26f46; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  2013.93,251.724 2157.93,251.724 \n",
       "  \"/>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:start;\" transform=\"rotate(0, 2181.93, 269.224)\" x=\"2181.93\" y=\"269.224\">y2</text>\n",
       "</g>\n",
       "<polyline clip-path=\"url(#clip0600)\" style=\"stroke:#3da44d; stroke-width:4; stroke-opacity:1; fill:none\" points=\"\n",
       "  2013.93,312.204 2157.93,312.204 \n",
       "  \"/>\n",
       "<g clip-path=\"url(#clip0600)\">\n",
       "<text style=\"fill:#000000; fill-opacity:1; font-family:Arial,Helvetica Neue,Helvetica,sans-serif; font-size:48px; text-anchor:start;\" transform=\"rotate(0, 2181.93, 329.704)\" x=\"2181.93\" y=\"329.704\">y3</text>\n",
       "</g>\n",
       "</svg>\n"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "plot(x, y/50)\n",
    "plot!(x, ev)\n",
    "plot!(xlabel = \"Nuclear coordinates\", ylabel = \"Strength\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "update_density_coefficients (generic function with 2 methods)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function update_density_coefficients(pos::Float64, c1::Union{Real,Complex}, c2::Union{Real,Complex}, vel::Float64, dt=1)\n",
    "    # for the evolution of the coefficients we work in the adiabatic representation\n",
    "    # Runge-Kutta4 method is used\n",
    "    # k1\n",
    "    # ===========  k1  =============\n",
    "    # ===========  k2  =============\n",
    "    # ===========  k3  =============\n",
    "    # ===========  k4  =============\n",
    "    e_val = eigvals(Potential(pos))\n",
    "    V = Potential(pos)\n",
    "    der_c1 = c1*(-1im*V[1,1]/hbar-nac(pos,1,1)*vel)\n",
    "    der_c1 += c2*(-1im*V[1,2]/hbar - nac(pos,1,2)*vel)\n",
    "    c1 += dt*der_c1\n",
    "    der_c2 = c1*(-1im*V[2,1]/hbar-nac(pos,2,1)*vel)\n",
    "    der_c2 += c2*(-1im*V[2,2]/hbar-nac(pos,2,2)*vel)\n",
    "    c2 += dt*der_c2\n",
    "    return c1, c2\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "time_evolution (generic function with 1 method)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function time_evolution(pos::Real, surface::Int64, m::Float64, dt::Float64, vel::Float64)\n",
    "    # This method defines the time evolution of a particle and\n",
    "    # update all the variables accordingly.\n",
    "    # Position is approximated at the first order, \n",
    "    # Velocity is approximated at the second order.\n",
    "    force = der_surface(pos, surface) # n-dimensional vector\n",
    "    a1 = -force/m\n",
    "    pos += vel*dt+0.5*a1*dt*dt\n",
    "    force = der_surface(pos, surface)\n",
    "    a2 = -force/m\n",
    "    vel += 0.5*(a1+a2)*dt\n",
    "    return pos, vel\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "density_matrix_coeff (generic function with 1 method)"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function density_matrix_coeff(c1::Union{Real,Complex}, c2::Union{Real,Complex})\n",
    "    # initialize electronic state\n",
    "    a11 = c1*conj(c1)\n",
    "    a12 = c1*conj(c2)\n",
    "    a21 = c2*conj(c1)\n",
    "    a22 = c2*conj(c2)\n",
    "    return a11, a12, a21, a22\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "coeff_b (generic function with 1 method)"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function coeff_b(pos::Real, vel::Real, i::Int64, j::Int64, c1::Union{Real,Complex}, c2::Union{Real,Complex})::Float64\n",
    "    a11, a12, a21, a22 = density_matrix_coeff(c1, c2)\n",
    "    b12 = 2/hbar*imag(conj(a12)*Potential(pos)[i,j])-2*real(conj(a12)*sum(vel.*nac(pos,1,2)))\n",
    "    b21 = 2/hbar*imag(conj(a21)*Potential(pos)[i,j])-2*real(conj(a21)*sum(vel.*nac(pos,2,1)))\n",
    "    b = [[0, b12],[b21, 0]]\n",
    "    return b[i][j]\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A state switch from $1$ to $2$ will be invoked if\n",
    "$$\\frac{\\Delta t b_{21}}{a_{11}}\\ge \\zeta $$\n",
    "\n",
    "In general, if I want to go from state `i` to state `j`, I need to evaluate the function `switch_surface(pos,j,i,...)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "switch_surface (generic function with 1 method)"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function switch_surface(pos::Real, vel::Real, surface::Int, c1::Union{Real,Complex}, c2::Union{Real,Complex}, m::Real, dt::Real)\n",
    "    a11, a12, a21, a22 = density_matrix_coeff(c1, c2)\n",
    "    criterion = 0\n",
    "    if surface == 1\n",
    "        criterion = coeff_b(pos,vel,2,1,c1,c2)*dt/a11\n",
    "    else\n",
    "        criterion = coeff_b(pos,vel,1,2,c1,c2)*dt/a22\n",
    "    end\n",
    "    criterion = maximum([0,real(criterion)])    \n",
    "    if Random.rand() < criterion\n",
    "        # If I'm in 1 I want dE=E(2)-E(1)\n",
    "        # while if I'm in 2 I want dE=E(1)-E(2) \n",
    "        dE = eigvals(Potential(pos))[3-surface]-eigvals(Potential(pos))[surface]  \n",
    "        if (dE<kinetic_energy(vel, m))\n",
    "            # now we have to change the velocity keeping constant the energy\n",
    "            # E = 1/2*M*v*v+V\n",
    "            # then from the above definition we can define the new velocity wrt the \n",
    "            # energy surface we are\n",
    "            tot = total_energy(pos,vel,surface,m)\n",
    "            surface = 3-surface\n",
    "            V_new = potential_energy(pos,surface)\n",
    "            new_kinetic = tot-V_new\n",
    "            vel = sign(vel)*sqrt(2*new_kinetic/m)\n",
    "        end\n",
    "    end\n",
    "    return vel, surface\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(0.01, 1)"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "switch_surface(0, 0.01, 1, 0.5+0.5im, 0.5-0.5im, 2000, 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-1"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = 1-3im\n",
    "b = conj(a)\n",
    "c = imag(a)\n",
    "sign(c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "run_dynamics (generic function with 1 method)"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function run_dynamics(pos, k, c1)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "ename": "MethodError",
     "evalue": "MethodError: no method matching kinetic_energy()\nClosest candidates are:\n  kinetic_energy(!Matched::Real, !Matched::Real) at In[4]:6",
     "output_type": "error",
     "traceback": [
      "MethodError: no method matching kinetic_energy()\nClosest candidates are:\n  kinetic_energy(!Matched::Real, !Matched::Real) at In[4]:6",
      "",
      "Stacktrace:",
      " [1] run() at ./In[18]:11",
      " [2] top-level scope at In[18]:14"
     ]
    }
   ],
   "source": [
    "n_time_steps = 2000\n",
    "\n",
    "function run()\n",
    "    dx = 0.001;\n",
    "    dt = 1;\n",
    "    pos, k, c1, surface, m = init();\n",
    "    c2 = 1-c1^2;\n",
    "    for i in 1:n_time_steps\n",
    "        run_dynamics(pos, k, c1)\n",
    "    end\n",
    "    K = kinetic_energy()\n",
    "    # collect data\n",
    "end\n",
    "\n",
    "run()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code should go following the scheme below:\n",
    "- Initialize a set of initial particles\n",
    "- for each particle:\n",
    "    - for all the time steps required:\n",
    "        - Move following the law of motion\n",
    "        - Update the coefficients $c_1$ and $c_2$\n",
    "        - Eveluate if a jump happens in the interval $\\delta t$\n",
    "    - Collect the information of the particles (e.g., how many are in surface 0 and 1 etc...)\n",
    "- plot the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Int64"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "typeof(2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.4.0",
   "language": "julia",
   "name": "julia-1.4"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.4.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
