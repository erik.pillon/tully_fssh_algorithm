# --------------------
println("Script started")
include("particle_methods.jl")
println("Methods imported correctly")
# --------------------

Potential(x) = Simple_avoided_crossing(x)
dV(x) = dV_Simple_Avoided_Crossing(x)
x = range(-10,10,length=1000)
y = zeros(length(x))
ev = zeros(length(x),2)
for i in 1:length(x)
    #y[i] = nac(x[i],1,2)[1]
    ev[i,:]=eigvals(Potential(x[i]))[:]
end

p1 = plot(x, nac.(x,1,2)/50,
    linestyle=:dash,
    label="NAC/50")
plot!(x, ev,label=["" ""])

# ============================================

Potential(x) = Dual_avoided_crossing(x)
dV(x) = dV_Dual_avoided_crossing(x)
ev = zeros(length(x),2)
for i in 1:length(x)
    ev[i,:]=eigvals(Potential(x[i]))[:]
end

p2 = plot(x, nac.(x,1,2)/12,
    linestyle=:dash,
    label="NAC/12")
plot!(x, ev,label=["" ""])

# ============================================

Potential(x) = Extended_coupling(x)
dV(x) = dV_Extended_coupling(x)
ev = zeros(length(x),2)
for i in 1:length(x)
    ev[i,:]=eigvals(Potential(x[i]))[:]
end

p3 = plot(x, nac.(x,1,2),
    linestyle=:dash,
    label="NAC")
plot!(x, ev, label=["" ""])

# ============================================

plot(p1,p2,p3,
    layout=(3,1),
    legend=true,
    title=""
    )



# savefig("Desktop/GitLab_works/tully_fssh_algorithm/Julia/NAC Tully")
