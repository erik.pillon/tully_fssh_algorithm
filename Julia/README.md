# Fewest Switches Surface Hopping Algorithm 

Numerical implementation of Tully's FSSH algorithm. The code follows the original implementation of Tully.
The code in this folder has been entirely implemented in Julia v1.4.

The file `main.jl` t=contains the script for running the simulation, `particle_methods` contains all the functions.
`plot_surfaces` gives a visualization of the surfaces we're working with.


