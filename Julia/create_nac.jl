# create_nac.jl

# This file should return the following quantities in a jld file
#  :d_12_interp
#  :d_11_interp
#  :d_22_interp
#  :E1
#  :E2

using Dierckx
using LinearAlgebra
using Polynomials
using Plots
using LaTeXStrings

import Potentials

dt = 1
hbar = 1

function create_nac(Pot, der_Pot, x, size::Int=2, delta_x=0.000000001)
    R = [0 -1; 1 0]
    # create the empty variables
    E = zeros(length(x),size)
    ev1 = zeros(length(x),size)
    ev2 = zeros(length(x),size)
    ev2_b = zeros(length(x),size)
    ev2_a = zeros(length(x),size)
    nac_12 = zeros(length(x))
    for i in 1:length(x)
        # y[i] = nac(x[i],1,2)[1]
        E[i,:] = eigvals(Pot(x[i]))[:]
        ev1[i,:] = eigvecs(Pot(x[i]))[1,:]
        ev2[i,:] = eigvecs(Pot(x[i]))[2,:]
        if i ==1
            t1 = sum(conj((ev1[i,:]).*ev2_b[i,:]))
            t2 = sum(conj((ev1[i,:]).*ev2_a[i,:]))
            d_12_1 = (t2-t1)/2delta_x
            continue
        end
        # check if rotation is needed
        if abs(sum(ev1[i,:].*ev1[i-1,:]))<0.1
            ev1[i,:] = R*ev1[i,:]
        end
        if abs(sum(ev2[i,:].*ev2[i-1,:]))<0.1
            ev2[i,:] = R*ev2[i,: ]
        end
        if abs(1-sum(ev1[i,:].*ev1[i-1,:]))>0.2
            ev1[i,:] *= -1
        end
        if abs(1-sum(ev2[i,:].*ev2[i-1,:]))>0.2
            ev2[i,:] *= -1
        end
        nac_12[i] = -ev1[i,:]'*der_Pot(x[i])*ev2[i,:]/(E[i,2]-E[i,1])

    end
    return nac_12, E
end


d_12 = create_nac(Dual_avoided_crossing,dV_Dual_avoided_crossing,x)
d_12_interp = Spline1D(x, d_12) # degree = 2

function diplay_nac(x,f)
    gr()
    #plot(x,d_12,ylim=[-1,1])
    p = plot(x,f.(x),
        title="Non adiabatic coupling term",
        label=L"NAC_{12}",
        xlabel=L"x",
        ylabel="Strength")
    # savefig("/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/NAC_2.png")
end

using JLD2
@save "d_12_interp.jld" d_12_interp
