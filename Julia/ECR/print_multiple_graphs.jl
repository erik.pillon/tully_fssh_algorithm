# multiple_graphs_ECR.jl

using Plots
using Dierckx
using Polynomials
using JLD2
using JLD

system = "/Extended_coupling.jld"

s = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Surfaces"*system
#s = system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

# ================= FIRST PLOT =================
x = range(-10,stop=10, length=5000)
y1 = E1(x)
y2 = E2(x)
y3 = d_12_interp(x)

p1 = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates")
plot!(x,y2,label = "E2")
plot!(x,y3,
    line = (:dash, :green),
    label="d_12")
# ==============================================

# ================= SECOND PLOT =================
p2 = plot(xlabel="Energy (a.u.)",
    ylabel="Transmission Probability",
    tickfontsize = 10,
    legend=:bottomright)

# -----------------------------------------------
# main_2d.out
k_range = [4,5,6,7,8,9,12,15,18,21,24,25,26,27,28,28,30,31,32]
# transm = [0.012718024268821982, 0.015504530963196563, 0.01801154142648661,
#     0.020261044704394673, 0.02228418579626982, 0.024112028414454024,
#     0.028666957565482952, 0.032161320509642814, 0.034913311337145904,
#     0.037159166314626534, 0.0390327199442095, 0.03958790013396556,
#     0.04011130560391041, 0.04060541079254244, 0.04107271816587742, 0.04107271816587742,
#     0.04193468367218581, 0.0423331508751863, 0.0427123114764021]
# plot!(k_range, transm,
#     line = (:purple),
#     linewidth = 2,
#     markersize = 5,
#     markershape=:diamond,
#     markercolor=:purple,
#     markerstrokecolor = :purple,
#     label="Standard FSSH")
# plot!(k_range, transm, seriestype = :scatter,label="")
# -----------------------------------------------
# modified_wrong_Ehrenfest.out
# standard ehrenfest
transm = [0.012718024268821982, 0.015504530963196563, 0.01801154142648661,
    0.020261044704394673, 0.02228418579626982, 0.024112028414454024,
    0.028666957565482952, 0.032161320509642814, 0.034913311337145904,
    0.037159166314626534, 0.0390327199442095, 0.03958790013396556,
    0.04011130560391041, 0.04060541079254244, 0.04107271816587742, 0.04107271816587742,
    0.04193468367218581, 0.0423331508751863, 0.0427123114764021]
plot!(k_range, transm,
    line = (:blue),
    linewidth = 2,
    markershape=:utriangle,
    markersize = 5,
    markercolor=:blue,
    markerstrokecolor = :blue,
    label="Standard Ehrenfest")
# plot!(k_range, transm, seriestype = :scatter,label="")

# top_force.jl
# top_force.out.out
t = [0.10373583366086986, 0.11736214702196703, 0.12871349825259332, 0.13859387621291797,
    0.14727418084943544, 0.15, 0.17,  0.181,
    0.188, 0.193, 0.198, 0.199,
    0.200, 0.2021, 0.2032, 0.2032, 0.2053,
    0.2062, 0.2071]
plot!(k_range, t.^2,
    line = (:red),
    linewidth = 2,
    markershape=:x,
    markersize = 5,
    markercolor=:red,
    markerstrokecolor=:red,
    label="Top Force")
# plot!(k_range, t.^2, seriestype = :scatter, label="")
# ------------------------------------------------------------------------------

# # extended_FSSH.out
# transm = [0.0026666666666666666, 0.004, 0.012, 0.021, 0.029333333333333333, 0.044,
#     0.068, 0.09133333333333334, 0.13533333333333333, 0.19266666666666668, 0.289,
#     0.38433333333333336, 0.501, 0.6516666666666666, 0.7793333333333333,
#     0.9016666666666666, 0.967, 0.9866666666666667, 0.9926666666666667,
#     0.9946666666666667, 0.996]
# plot!(k_range, transm,
#     line = (:green),
#     linewidth = 2,
#     markersize = 5,
#     markershape=:diamond,
#     markercolor=:green,
#     markerstrokecolor = :purple,
#     label="Modified FSSH")

# ------------------------------------------------------------------------------
# standard_ehrenfest_erik
# transm = [0.058759884418298776, 0.05262747395044259, 0.04208446963323773,
#     0.02507471206745231, 0.004492100614569053, 0.005552496810214715,
#     0.06260532907200325, 0.20049381900353444, 0.4222860330277598,
#     0.5908641525559082, 0.6214427571345973, 0.5121480849752283,
#     0.28810268350130264, 0.06875887274718538, 0.0019763021232312972,
#     0.09826078253413709, 0.2750192315232064, 0.4597793433997796,
#     0.6142397254476057, 0.7216633845955948, 0.7771789442491354]
# plot!(k_range, transm,
#     line = (:yellow),
#     linewidth = 2,
#     markersize = 5,
#     markershape=:utriangle,
#     markercolor=:yellow,
#     markerstrokecolor = :purple,
#     label="Ehrenfest Erik")
# ------------------------------------------------------------------------------



fig = plot(p1,p2,
    layout=grid(2,1,
    heights=[0.4, 0.6]),
    size = (800,800),
    tickfontsize=12,
    yguidefontsize=10,
    legendfontsize=10)

# fig = p2

path = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/ECR/"
name = "Extended_coupling_results"
savefig(fig, path*name)
