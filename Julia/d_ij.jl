# -----------------------------
println("Script started")
# -----------------------------
using LinearAlgebra
using Plots
using Random
using Polynomials
using LaTeXStrings
# -----------------------------
a = 0.01
dist = 1
function Simple_avoided_crossing(x::Real,a::Float64,dist=1.0)::Array{Real,2}
    A, B, C, D = a, 1.6, 0.005, 1.0
    V = zeros(2,2)
    if x>0
        V[1,1] = A*(dist-exp(-B*x))
    else
        V[1,1] = -A*(dist-exp(B*x))
    end
    V[2,2] = -V[1,1]
    V[1,2] = C*exp(-D*x^2)
    V[2,1] = V[1,2]
    return V
end

function new_nac(pos::Float64, a::Float64, dist, i::Int, j::Int)
    nac = 0;
    e_vecs = eigvecs(Potential(pos,a,dist))
    dx = 0.0000001;
    e_vecs_1 = eigvecs(Potential(pos-dx,a,dist))
    e_vecs_2 = eigvecs(Potential(pos+dx,a,dist))
    t1 = sum(conj((e_vecs[:,i]).*e_vecs_1[:,j]))
    t2 = sum(conj((e_vecs[:,i]).*e_vecs_2[:,j]))
    d_ij = (t2-t1)/(2dx)
    nac = abs(d_ij)
    return nac[1]
end

Potential(x,a,dist=1) = Simple_avoided_crossing(x,a,dist)

x = range(-5,5,length=1000)
y = zeros(length(x))
ev = zeros(length(x),2)
for i in 1:length(x)
    #y[i] = nac(x[i],1,2)[1]
    ev[i,:]=eigvals(Potential(x[i],a))[:]
end

p1 = plot(x, new_nac.(x,a,dist,1,2)/50,
    linestyle=:dash,
    label="NAC/50")
plot!(x, ev,label=["" ""])

# ============================================
l = 100
d = range(0.5, stop=5, length=l)
D_ij = zeros(l)

for (index,dist) in enumerate(d)
    D_ij[index] = new_nac(0.0001,a,dist,1,2)
end

gr()

f = fit(d, D_ij, 2) # degree = 2
p2 = scatter(d, D_ij,
    markerstrokewidth=0,
    label=L"d_{ij}")
p2 = plot!(ylabel="Strenght of the non adiabatic coupling")
p2 = plot!(xlabel="Distance between the adiabats")
# p2 = plot!(f, extrema(d)..., label="Linear Fit")

savefig("/Users/erik.pillon/Desktop/Reports/Report2/Im/dij_versus_d")
