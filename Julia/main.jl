# Next step is the following:
# create the full picture of the original work of Tully
#   model = ["model 1", "model 2", "model 3"]
#   pic = ["pic a","pic b","pic c"]
# and then one goes
#   for p = pic ...
# and then can create all the plots


#
# “God does not care about our mathematical difficulties. He integrates empirically.”
# Albert Einstein
#

# -----------------------------
using Dates
t0 = Dates.now()
using Printf
using LinearAlgebra
using Plots
using Random
using MPI
t1 = Dates.now()
# -----------------------------

println("--- Script started ---")
println("--- Importing methods ---")
include("particle_methods.jl")
t2 = Dates.now()
println("--- methods imported correctly ---")

dt = 1;

# the label debug can assume two values:
# - false: will run a single trajectory dynamics and will plot all the meaningful quantitiues
#   for debugging
# - true: will run plot the results of the original paper of Tully
debug = false;


# Possible models are
#   - Simple_avoided_crossing
#   - Dual_avoided_crossing
#   - Extended_coupling
Potential(x) = Simple_avoided_crossing(x)

# There are 3 possible plots. So far the code is able to reproduce only "a" and "b"
pic = "a"

if debug
    n_test = 1
    k_range = [9.0]
else
    n_test = 2000;
    # k_range = [15, 20, 25, 30, 35, 40]
    k_range = [8, 8.5, 8.75, 9, 9.5, 10, 11, 12, 13, 16, 20, 25, 30]
    # k_range = [15, 20, 25]
    # k_range = [10,11,12,13,16,20,25,30]
    # every value below 8.9 should be zero
    MPI.Init()

    comm = MPI.COMM_WORLD
    size = MPI.Comm_size(comm)
    rank = MPI.Comm_rank(comm)
end

n_time_steps = 2000;

if debug
    dens = zeros(n_time_steps)
    nacs = zeros(n_time_steps)
    cb = zeros(n_time_steps)
    E_s = zeros(n_time_steps)
    T_s = zeros(n_time_steps)
    V_s = zeros(n_time_steps)
    c1s = zeros(Complex,(1,n_time_steps))
    c2s = zeros(Complex,(1,n_time_steps))
end

time_actual_algorithm = 0
t3_cumulative = 0

#k_range = [15]
if pic == "a"
    transm = []
    for k = k_range
        t3_total = 0
        counter = 0;
        for i = 1:n_test
            if !(mod(i,size) == rank); continue end
            if (i%50 == 0)
                println("we are at k= $k and iteration $i out of $n_test")
            end
            t3 = Dates.now()
            pos, k, c1, surf, m = create(-4,k);
            c2 = sqrt(1.0-c1*conj(c1));
            vel = k/m;
            iter = 0;
            while iter < n_time_steps && abs(pos) < 4.01
                # The algorithm goes as follows
                # - integrate the equation for the coefficients
                # - hopping (Y/N)
                # - Newton Equation of motion for the nuclei
                iter += 1
                # new_pos, new_vel = time_evolution(pos,surf,m,dt,vel)
                # for time_interval in 1:1
                #     c1, c2 = new_time_evolution_rk4(pos,surf,c1,c2,vel,m,dt)
                # end
                pos, c1, c2, vel = time_evolution_rk4(pos, surf, c1, c2, vel, m, dt)
                if debug
                    dens[iter] = sqrt(c1*conj(c1)+c2*conj(c2));
                    nacs[iter] = nac(pos,surf,3-surf)
                    c1s[iter] = c1
                    c2s[iter] = c2
                    E_s[iter] = total_energy(pos,vel,surf);
                    V_s[iter] = potential_energy(pos,surf);
                    T_s[iter] = kinetic_energy(vel,m);
                    if surf == 1
                        cb[iter] = coeff_b(pos,vel,2,1,c1,c2)*dt/(c1*conj(c1))
                    else
                        cb[iter] = coeff_b(pos,vel,1,2,c1,c2)*dt/(c2*conj(c2))
                    end
                end
                new_vel, surf = switch_surface(pos, vel, surf, c1, c2, m, dt)
            end
            # println(pos)
            if surf == 2
                counter += 1
            end
            t3_end = Dates.now()
            # println(t3_total)
            # println(t3_end-t3)
            t3_total += Dates.value(Dates.Millisecond(t3_end-t3))
        end
        # ------------------------------------------------------
        # Total time spent in the dynamics
        t3_cumulative = MPI.Reduce(t3_total,MPI.SUM,0,comm) 

        total_count = 0
        total_count = MPI.Reduce(counter,MPI.SUM,0,comm)
        if rank == 0
            # ----- Time analysis -----
            global time_actual_algorithm += t3_cumulative
            println("time spent for evaluation of k= ",k, " is: ", t3_cumulative)
            println("time spent since the beginning of the algorithm: ", time_actual_algorithm)
            # ----- Actual analysis ------
            print(total_count/n_test,"\n")
            push!(transm,total_count/n_test)
            print(transm)
        end
        # ------------------------------------------------------
    end
end

sleep(rank*2)

MPI.Barrier(comm)
MPI.Finalize()

t_final = Dates.now()

# ==========================================================
# Plotting the 
# 

# Data of Tully
k_test = [6, 7, 8.5, 10, 11, 14, 15, 20, 30]
p_tran = [0, 0, 0,   15, 20, 25, 33, 52, 66]
if pic == "b"
    transm = []
    for k = k_range
        counter = 0;
        for i = 1:n_test
            if (i%25 == 0)
                println("we are at k= $k and iteration $i out of $n_test\n")
            end
            pos, k, c1, surf, m = create(-5,k,0,2);
            c2 = sqrt(1.0-c1*conj(c1));
            vel = k/m;
            iter = 0;
            while iter < n_time_steps && pos < 4.0
                iter += 1
                #pos, vel = time_evolution(pos,surf,m,dt,vel)
                pos, c1, c2, vel = time_evolution_rk4(pos,surf,c1,c2,vel,m,dt)
                #c1,c2 = update_density_coefficients(pos, c1, c2, vel, dt)
                if debug
                    dens[iter] = sqrt(c1*conj(c1)+c2*conj(c2));
                    nacs[iter] = nac(pos,surf,3-surf)
                    c1s[iter] = c1
                    c2s[iter] = c2
                    E_s[iter] = total_energy(pos,vel,surf);
                    V_s[iter] = potential_energy(pos,surf);
                    T_s[iter] = kinetic_energy(vel,m);
                    if surf == 1
                        cb[iter] = coeff_b(pos,vel,2,1,c1,c2)*dt/(c1*conj(c1))
                    else
                        cb[iter] = coeff_b(pos,vel,1,2,c1,c2)*dt/(c2*conj(c2))
                    end
                end
                vel, surf = switch_surface(pos, vel, surf, c1, c2, m, dt)
            end
            # println(pos)
            if surf == 1
                counter += 1
            end
        end
        print(counter/n_test)
        push!(transm,counter/n_test)
        print(transm)
    end
end

if debug
    gr()
    # wavefunction density plot
    p1 = plot(1:n_time_steps,dens,
        label="wavefunction",
        #xlabel="Time steps",
        #xtickfont = font(20, "Courier")
        )
    #ylabel!("Norm of the wavefunction")
    #title!("Time evolution of the pseudo-particle")
    # non adiabatic coupling terms plot
    p2=plot(1:n_time_steps,nacs,label="NAC")
    # evolution of the coefficients c_1 and c_2
    p3=plot(1:n_time_steps,norm.(c1s)[:],label="c_1")
    p3=plot!(1:n_time_steps,norm.(c2s)[:],label="c_2")
    #title!("evolution of c_1 and c_2")
    p4 = plot(1:n_time_steps,[E_s, T_s, V_s])
    # final plot
    display(plot(p1,p2,p3,p4,layout=(4,1),legend=false))
    # plot!(1:n_time_steps,cb)
else
    if pic == "a"
        gr()
        p = plot(k_range,transm,
            label="implementation",
            ytickfont = font(8),
            xtickfont = font(8),
            ylim = (-0.1,1),
            ylabel="Transmission probability"
        )
        scatter!(k_range,transm,
        markershape = :circle,
        markersize = 5,
        label="",
        )
        scatter!(k_test, p_tran/100,
            label = "FSSH data")
        if rank == 0
            display(display(p))
            readline()
        end
    elseif pic == "b"
        gr()
        plot(k_range,ones(size(k_range))-transm,
        label="FSSH data",
        ytickfont = font(8),
        xtickfont = font(8),
        ylim = (0,1),
        ylabel="Reflection probability"
        )
        scatter!(k_range,ones(size(k_range))-transm,
        markershape = :circle,
        markersize = 5,
        label="",
        )
    end
end

if rank == 0
    path_to_file = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/file_to_write.txt"
    # variables
    package_time = Dates.value(Dates.Millisecond(t1-t0)) 
    import_time = Dates.value(Dates.Millisecond(t2-t1))
    dynamics_time = time_actual_algorithm
    total_time = Dates.value(Dates.Millisecond(t_final-t0))
    # function
    external_save(path_to_file, k_range, n_time_steps, dt, n_test, transm, package_time, import_time, dynamics_time, total_time)
end

println("script ended\n")
