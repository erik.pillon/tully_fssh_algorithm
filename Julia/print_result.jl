using Plots

k_test = [6, 7, 8.5, 10, 11, 14, 15, 20, 30]
p_tran = [0, 0, 0,   15, 20, 25, 33, 52, 66]

k_range = [8.0, 8.5, 8.75, 9.0, 9.5, 10.0, 11.0, 12.0, 13.0, 16.0, 20.0, 25.0, 30.0]
transm = [0.0875, 0.1255, 0.128, 0.144, 0.166, 0.184, 0.2195, 0.241, 0.2675, 0.3655, 0.4745, 0.616, 0.7125]

gr()
p = plot(k_range,transm,
    label="implementation",
    ytickfont = font(8),
    xtickfont = font(8),
    ylim = (-0.1,1),
    ylabel="Transmission probability"
)
scatter!(k_range,transm, markershape = :circle, markersize = 5, label="")
scatter!(k_test, p_tran/100,
    label = "FSSH data")
display(display(p))

readline()

savefig("/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/tully_result.png")
