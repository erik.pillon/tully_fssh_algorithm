# ===================================
#     ________    _______
#    /  ______|  /   __  \
#    | |         |  |  \  |
#    | |_____    |  |__/  |
#    |  _____|   |  _____/
#    | |         | |
#    | |______   | |
#    \________|  \_/
#
#    erik.pillon@gmail.com
#    http://ErikPillon.github.io
#
# ====================================

# This script is dedicated to the printing of the PES

using Printf
using LinearAlgebra
using Random
using Dierckx
using Polynomials
using JLD2
using JLD
using Plots

gr()

# ================= FIRST PLOT =================
system = "/Surfaces/Simple_avoided_crossing.jld"
s = pwd()*system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

x = range(-5,stop=5, length=5000)
y1 = E1(x)
y2 = E2(x)
y3 = d_12_interp(x)

fig = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates",
    title = "Simple Avoided Crossing")
plot!(x,y2,label = "E2")
plot!(x,y3/50,
    line = (:dash, :green),
    label="d_12/50")

savefig(fig, "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Simple_avoided_crossing.png")
# ==============================================


# ================= SECOND PLOT =================
system = "/Surfaces/Dual_avoided_crossing.jld"
s = pwd()*system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

x = range(-5,stop=5, length=5000)
y1 = E1(x)
y2 = E2(x)
y3 = d_12_interp(x)

fig = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates",
    title = "Dual Avoided Crossing")
plot!(x,y2,label = "E2")
plot!(x,y3/12,
    line = (:dash, :green),
    label="d_12/12")

savefig(fig, "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Dual_avoided_crossing.png")
# ==============================================

# ================= THIRD PLOT =================
system = "/Surfaces/Extended_coupling.jld"
s = pwd()*system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

x = range(-5,stop=5, length=5000)
y1 = E1(x)
y2 = E2(x)
y3 = d_12_interp(x)

fig = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates",
    title = "Extended Coupling")
plot!(x,y2,label = "E2")
plot!(x,y3,
    line = (:dash, :green),
    label="d_12")

savefig(fig, "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Extended_coupling.png")
# ==============================================

# ================= FOURTH PLOT =================
system = "/Surfaces/Super_Exchange_model.jld"
s = pwd()*system

# Super_Exchange_model loads all the following quantities
# d_12_interp
# d_11_interp
# E3
# E2
# d_22_interp
# E1

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")
E3 = load(s,"E3")

x = range(-5,stop=5, length=5000)
y1 = E1(x)
y2 = E3(x)
y3 = E2(x)
y4 = d_12_interp(x)

fig = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates",
    title = "Super Exchange model")
plot!(x,y2,label = "E2")
plot!(x,y3,label = "E3")
#plot!(x,y4/400,
#    line = (:dash, :green),
#    label="d_12")

savefig(fig, "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Super_Exchange_model.png")
# ==============================================
