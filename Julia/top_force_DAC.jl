# top_force_DAC.jl

# ===================================
#     ________    _______
#    /  ______|  /   __  \
#    | |         |  |  \  |
#    | |_____    |  |__/  |
#    |  _____|   |  _____/
#    | |         | |
#    | |______   | |
#    \________|  \_/
#
#    erik.pillon@gmail.com
#    http://ErikPillon.github.io
#
# ====================================

# -----------------------------
#
# “The same equations have the same solutions.”
# — Richard P. Feynman
#
# -----------------------------

using Printf
using LinearAlgebra
using Plots
using Random
using MPI
using Dierckx
using Polynomials
using LaTeXStrings
using JLD2

# -----------------------------
println("--- Script started ---")
println("--- Importing methods ---")
include("new_methods.jl")
include("Potentials.jl")
println("--- methods imported correctly ---")
# -----------------------------

dt = 1

hbar = 1

# Possible systems are
# - Dual_avoided_crossing
# - Simple_avoided_crossing
# - Extended_coupling

system = "Dual_avoided_crossing"

@load "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Dual_avoided_crossing.jld"

function main()
    n_time_steps = 20000
    dt = 0.1
    k_range = [8, 8.5, 9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
    k_range = [sqrt(2*2000*exp(i)) for i in -4:0.25:1]
    # k_range = [9, 9.5, 10, 11, 12, 13, 15, 17.5, 20, 25, 30]
    # k_range = [20, 25, 30]
    # k_range = [8, 8.5, 9]
    # every value below 8.9 should be zero

    for k in k_range
        println("started to analysze k=",k)
        p = simple_wp([-5.],[k],1,0,2000)
        run_dynamics(p,dV_Simple_Avoided_Crossing,dt,n_time_steps)
        println("For k= ", k ," we have c1:", abs(p.c1), " and c2: ",abs(p.c2))
        println(p.pos)
        println(p.k)
    end


    # if rank == 0
    #     path_to_file = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Simple_2D.txt"
    #     # variables
    #     package_time = Dates.value(Dates.Millisecond(t1-t0))
    #     # package_time = 0
    #     import_time = Dates.value(Dates.Millisecond(t2-t1))
    #     # import_time = 0
    #     dynamics_time = time_actual_algorithm/size
    #     # dynamics_time = 0
    #     total_time = Dates.value(Dates.Millisecond(t_final-t0))
    #     external_save(path_to_file, k_range, n_time_steps, dt, n_test, transm, package_time, import_time, dynamics_time, total_time)
    # end

end

main()
