# -----------------------------
using LinearAlgebra
using Plots
using Random
using Printf
# -----------------------------

# Useful constants
hbar = 1;

"""
Simple Avoided Crossing Potential,
as model "A" in the original paper of Tully.
"""
function Simple_avoided_crossing(x::Real)::Array{Real,2}
    A = 0.01
    B = 1.6
    C = 0.005
    D = 1.0
    V = zeros(2,2)
    if x>0
        V[1,1] = A*(1-exp(-B*x))
    else
        V[1,1] = -A*(1-exp(B*x))
    end
    V[2,2] = -V[1,1]
    V[1,2] = C*exp(-D*x^2)
    V[2,1] = V[1,2]
    return V
end

function dV_Simple_Avoided_Crossing(x::Real)::Array{Real,2}
    A = 0.01
    B = 1.6
    C = 0.005
    D = 1.0
    dV = zeros(2,2)
    if x>0
        dV[1,1] = A*B*(exp(-B*x))
    else
        dV[1,1] = A*B*(exp(B*x))
    end
    dV[2,2] = -dV[1,1]
    dV[1,2] = dV[2,1] = 2x*C*D*exp(-D*x^2)
    return dV
end

"""
Dual Avoided Crossing Potential,
as model "B" in the original paper of Tully.
"""
function Dual_avoided_crossing(x::Real)::Array{Real,2}
    A, B, C, D = 0.1, 0.28, 0.015, 0.06
    E_0 = 0.05
    V = zeros(2,2)
    V[1,1] = 0.
    V[2,2] = -A*exp(-B*x^2)+E_0
    V[1,2] = V[2,1] = C*exp(-D*x^2)
    return V
end

function dV_Dual_avoided_crossing(x::Real)::Array{Real,2}
    A, B, C, D = 0.1, 0.28, 0.015, 0.06
    V = zeros(2,2)
    V[1,1] = 0.
    V[2,2] = A*B*2x*exp(-B*x^2)
    V[1,2] = V[2,1] = -C*D*2x*exp(-D*x^2)
    return V
end

"""
Extended coupling Potential,
as model "C" in the original paper of Tully.
"""
function Extended_coupling(x::Real)::Array{Real,2}
    A, B, C = 6e-4, 0.1, 0.9
    V = zeros(2,2)
    V[1,1] = A
    V[2,2] = -A
    if x<0
        V[1,2] = B*exp(C*x)
    else
        V[1,2] = B*(2-exp(-C*x))
    end
    V[2,1] = V[1,2]
    return V
end

function dV_Extended_coupling(x::Real)::Array{Real,2}
    A, B, C = 6e-4, 0.1, 0.9
    dV = zeros(2,2)
    dV[1,1] = 0
    dV[2,2] = 0
    if x<0
        dV[1,2] = B*C*exp(C*x)
    else
        dV[1,2] = B*C*exp(-C*x)
    end
    dV[2,1] = dV[1,2]
    return dV
end

function create(pos=-5, k=10, c1=1.0+0.0im, surf=1, m=2000)
    return pos, k, c1, surf, m
end

function kinetic_energy(vel::Real, m::Real)::Float64
    return vel*vel*m/2
end

function potential_energy(pos::Real, surface::Int)::Float64
    return eigvals(Potential(pos))[surface]
end

function total_energy(pos::Real, vel::Real, surface::Int,m=2000)::Float64
    return potential_energy(pos,surface)+kinetic_energy(vel,m)
end


"""
    der_surface(pos, surf)

    Compute the derivative of the adiabatic surface `surf` at position `pos`.
"""
function der_surface(pos, surf)
    # This function is evaluating the gradient of the
    # potential energy surface.
    dx = 0.00001
    dim = length(pos)
    der = zeros(dim)
    for count in 1:dim
        ev1 = eigvals(Potential(pos[count]-dx))[surf]
        ev2 = eigvals(Potential(pos[count]+dx))[surf]
        der[count] = (ev2-ev1)/2/dx
    end
    return der[1]
end

"""
    nac(pos, i, j)

    Function for the evaluation of the non adiabatic coupling vector d_ij
    non-adiabatic vector cupling = Connection (= vector potential)
    `i` and `j` can take ony values 1 and 2

    N.B.: if `i==j`, then the result needs to be a purely imaginary quantity

    The implementation of the following formula is taken from
    http://iopenshell.usc.edu/pubs/pdf/jcp-148-044103.pdf
"""
function nac_old(pos::Real, i::Int, j::Int)
    dim = length(pos);
    nac = zeros(dim);
    e_vecs = eigvecs(Potential(pos))
    e_vals = eigvals(Potential(pos))
    for count = 1:dim
        r = dV(pos)*e_vecs[:,j]
        d_ij = sum(conj(e_vecs[:,i]).*r)
        nac[count] = d_ij/(e_vals[j]-e_vals[i])
    end
    return nac[1]
end

"""
    The method for evaluating the NAC follows the instruction
    of the following link

    https://en.wikipedia.org/wiki/Vibronic_coupling
"""
function nac(pos::Real, i::Int, j::Int)
    dim = length(pos);
    nac = zeros(dim);
    e_vecs = eigvecs(Potential(pos))
    dx = 0.0000001;
    e_vecs_1 = eigvecs(Potential(pos-dx))
    e_vecs_2 = eigvecs(Potential(pos+dx))
    for count = 1:dim
        t1 = sum(conj((e_vecs[:,i]).*e_vecs_1[:,j]))
        t2 = sum(conj((e_vecs[:,i]).*e_vecs_2[:,j]))
        d_ij = (t2-t1)/(2dx)
        nac[count] = d_ij
    end
    return nac[1]
end

function update_density_coefficients(pos::Real, c1::Number, c2::Number, vel::Real, dt=1)
    # for the evolution of the coefficients we work in the adiabatic representation
    # an explicit Euler method is used
    V = Potential(pos)
    #der_c1 = c1*(-1im*V[1,1]/hbar-nac(pos,1,1)*vel)+c2*(-1im*V[1,2]/hbar-nac(pos,1,2)*vel)
    #der_c2 = c1*(-1im*V[2,1]/hbar-nac(pos,2,1)*vel)+c2*(-1im*V[2,2]/hbar-nac(pos,2,2)*vel)
    # der_c1 = c1*(-1im*eigvals(V)[1]/hbar-imag(nac(pos,1,1)*vel))
    # der_c1 += c2*(-nac(pos,1,2)*vel)
    # der_c2 = c1*(-nac(pos,2,1)*vel)
    # der_c2 += c2*(-1im*eigvals(V)[2]/hbar-imag(nac(pos,2,2)*vel))
    # c1 += dt*der_c1
    # c2 += dt*der_c2
    der_c1 = c1*(-1im*eigvals(V)[1]/hbar)
    der_c1 += c2*(-nac(pos,1,2)*vel)
    der_c2 = c1*(-nac(pos,2,1)*vel)
    der_c2 += c2*(-1im*eigvals(V)[2]/hbar)
    c1 += dt*der_c1
    c2 += dt*der_c2
    return c1, c2
end

function old_time_evolution_rk4(pos::Real, surf::Int, c1::Number, c2::Number, vel::Real, m::Real, dt::Real)
    # ------
    # der_c1 = c1*(-1im*V[1,1]/hbar-nac(pos,1,1)*vel)+c2*(-1im*V[1,2]/hbar-nac(pos,1,2)*vel)
    # der_c2 = c1*(-1im*V[2,1]/hbar-nac(pos,2,1)*vel)+c2*(-1im*V[2,2]/hbar-nac(pos,2,2)*vel)
    # ------
    h = dt
    # ====== 1 ======
    der_pos_1 = h*vel
    der_vel_1 = -h*der_surface(pos, surf)/m
    V = Potential(pos)
    V_x = dV(pos)
    #der_c1_1 = h*c1*(-1im*V[1,1]/hbar-nac(pos,1,1)*vel)+c2*(-1im*V[1,2]/hbar-nac(pos,1,2)*vel)
    #der_c2_1 = h*c1*(-1im*V[2,1]/hbar-nac(pos,2,1)*vel)+c2*(-1im*V[2,2]/hbar-nac(pos,2,2)*vel)
    der_c1_1 = h*(c1*(-1im*V[1,1]/hbar-nac(pos,1,1)*vel)+c2*(-1im*V[1,2]/hbar-nac(pos,1,2)*vel))
    der_c2_1 = h*(c1*(-1im*V[2,1]/hbar-nac(pos,2,1)*vel)+c2*(-1im*V[2,2]/hbar-nac(pos,2,2)*vel))
    # ====== 2 ======
    der_pos_2 = h*(vel+der_vel_1/2)
    der_vel_2 = -h*der_surface(pos+der_pos_1/2, surf)/m
    V = Potential(pos+der_pos_1/2)
    V_x = dV(pos+der_pos_1/2)
    #der_c1_2 = h*(c1+der_c1_1/2)*(-1im*(V[1,1]+V_x[1,1]/2)/hbar-nac(pos+der_pos_1/2,1,1)*(vel+der_vel_1/2))+(c2+der_c2_1/2)*(-1im*(V[1,2]+V_x[1,2]/2)/hbar-nac(pos+der_pos_1/2,1,2)*(vel+der_vel_1/2))
    #der_c2_2 = h*(c1+der_c1_1/2)*(-1im*(V[2,1]+V_x[2,1]/2)/hbar-nac(pos+der_pos_1/2,2,1)*(vel+der_vel_1/2))+(c2+der_c2_1/2)*(-1im*(V[2,2]+V_x[2,2]/2)/hbar-nac(pos+der_pos_1/2,2,2)*(vel+der_vel_1/2))
    der_c1_2 = h*((c1+der_c1_1/2)*(-1im*(V[1,1])/hbar-nac(pos+der_pos_1/2,1,1)*(vel+der_vel_1/2))+(c2+der_c2_1/2)*(-1im*(V[1,2])/hbar-nac(pos+der_pos_1/2,1,2)*(vel+der_vel_1/2)))
    der_c2_2 = h*((c1+der_c1_1/2)*(-1im*(V[2,1])/hbar-nac(pos+der_pos_1/2,2,1)*(vel+der_vel_1/2))+(c2+der_c2_1/2)*(-1im*(V[2,2])/hbar-nac(pos+der_pos_1/2,2,2)*(vel+der_vel_1/2)))
    # ====== 3 ======
    der_pos_3 = h*(vel+der_vel_2/2)
    der_vel_3 = -h*der_surface(pos+der_pos_2/2, surf)/m
    V_x = dV(pos+der_pos_2/2)
    V = Potential(pos+der_pos_2/2)
    #der_c1_3 = h*(c1+der_c1_2/2)*(-1im*(V[1,1]+V_x[1,1]/2)/hbar-nac(pos+der_pos_2/2,1,1)*(vel+der_vel_2/2))+(c2+der_c2_2/2)*(-1im*(V[1,2]+V_x[1,2]/2)/hbar-nac(pos+der_pos_2/2,1,2)*(vel+der_vel_2/2))
    #der_c2_3 = h*(c1+der_c1_2/2)*(-1im*(V[2,1]+V_x[2,1]/2)/hbar-nac(pos+der_pos_2/2,2,1)*(vel+der_vel_2/2))+(c2+der_c2_2/2)*(-1im*(V[2,2]+V_x[2,2]/2)/hbar-nac(pos+der_pos_2/2,2,2)*(vel+der_vel_2/2))
    der_c1_3 = h*((c1+der_c1_2/2)*(-1im*(V[1,1])/hbar-nac(pos+der_pos_2/2,1,1)*(vel+der_vel_2/2))+(c2+der_c2_2/2)*(-1im*(V[1,2])/hbar-nac(pos+der_pos_2/2,1,2)*(vel+der_vel_2/2)))
    der_c2_3 = h*((c1+der_c1_2/2)*(-1im*(V[2,1])/hbar-nac(pos+der_pos_2/2,2,1)*(vel+der_vel_2/2))+(c2+der_c2_2/2)*(-1im*(V[2,2])/hbar-nac(pos+der_pos_2/2,2,2)*(vel+der_vel_2/2)))
    # ====== 4 ======
    der_pos_4 = h*(vel+der_vel_3)
    der_vel_4 = -h*der_surface(pos+der_pos_3,surf)/m
    V_x = dV(pos+der_pos_3)
    V = Potential(pos+der_pos_3)
    #der_c1_4 = h*(c1+der_c1_3)*(-1im*(V[1,1]+V_x[1,1])/hbar-nac(pos+der_pos_3,1,1)*(vel+der_vel_3))+(c2+der_c2_3)*(-1im*(V[1,2]+V_x[1,2])/hbar-nac(pos+der_pos_3,1,2)*(vel+der_vel_3))
    #der_c2_4 = h*(c1+der_c1_3)*(-1im*(V[2,1]+V_x[2,1])/hbar-nac(pos+der_pos_3,2,1)*(vel+der_vel_3))+(c2+der_c2_3)*(-1im*(V[2,2]+V_x[2,2])/hbar-nac(pos+der_pos_3,2,2)*(vel+der_vel_3))
    der_c1_4 = h*((c1+der_c1_3)*(-1im*(V[1,1])/hbar-nac(pos+der_pos_3,1,1)*(vel+der_vel_3))+(c2+der_c2_3)*(-1im*(V[1,2])/hbar-nac(pos+der_pos_3,1,2)*(vel+der_vel_3)))
    der_c2_4 = h*((c1+der_c1_3)*(-1im*(V[2,1])/hbar-nac(pos+der_pos_3,2,1)*(vel+der_vel_3))+(c2+der_c2_3)*(-1im*(V[2,2])/hbar-nac(pos+der_pos_3,2,2)*(vel+der_vel_3)))
    # === final values ===
    vel += (der_vel_1+2*der_vel_2+2*der_vel_3+der_vel_4)/6
    pos += (der_pos_1+2*der_pos_2+2*der_pos_3+der_pos_4)/6
    c1 += (der_c1_1+2*der_c1_2+2*der_c1_3+der_c1_4)/6
    c2 += (der_c2_1+2*der_c2_2+2*der_c2_3+der_c2_4)/6
    return pos, c1, c2, vel
end

function time_evolution_rk4(pos::Real, surf::Int, c1::Number, c2::Number, vel::Real, m::Real, dt::Real)
    # ------
    # der_c1 = c1*(-1im*V[1,1]/hbar-nac(pos,1,1)*vel)+c2*(-1im*V[1,2]/hbar-nac(pos,1,2)*vel)
    # der_c2 = c1*(-1im*V[2,1]/hbar-nac(pos,2,1)*vel)+c2*(-1im*V[2,2]/hbar-nac(pos,2,2)*vel)
    # ------
    h = dt
    # ====== 1 ======
    der_pos_1 = h*vel
    der_vel_1 = -h*der_surface(pos, surf)/m
    V = Potential(pos)
    val = eigvals(V);
    V1 = val[1]
    V2 = val[2]
    # der_c1_1 = h*c1*(-1im*V[1,1]/hbar-nac(pos,1,1)*vel)+c2*(-1im*V[1,2]/hbar-nac(pos,1,2)*vel)
    # der_c2_1 = h*c1*(-1im*V[2,1]/hbar-nac(pos,2,1)*vel)+c2*(-1im*V[2,2]/hbar-nac(pos,2,2)*vel)
    der_c1_1 = h*(c1*(-1im*val[1]/hbar-imag(nac(pos,1,1))*vel)+c2*(-nac(pos,1,2)*vel))
    der_c2_1 = h*(c1*(-nac(pos,2,1)*vel)+c2*(-1im*val[2]/hbar-imag(nac(pos,2,2)*vel)))
    # ====== 2 ======
    der_pos_2 = h*(vel+der_vel_1/2)
    der_vel_2 = -h*der_surface(pos+der_pos_1/2, surf)/m
    V = Potential(pos+der_pos_1/2)
    val = eigvals(V);
    #der_c1_2 = h*(c1+der_c1_1/2)*(-1im*(V[1,1])/hbar-nac(pos+der_pos_1/2,1,1)*(vel+der_vel_1/2))+(c2+der_c2_1/2)*(-1im*(V[1,2])/hbar-nac(pos+der_pos_1/2,1,2)*(vel+der_vel_1/2))
    #der_c2_2 = h*(c1+der_c1_1/2)*(-1im*(V[2,1])/hbar-nac(pos+der_pos_1/2,2,1)*(vel+der_vel_1/2))+(c2+der_c2_1/2)*(-1im*(V[2,2])/hbar-nac(pos+der_pos_1/2,2,2)*(vel+der_vel_1/2))
    der_c1_2 = h*((c1+der_c1_1/2)*(-1im*(val[1])/hbar-imag(nac(pos+der_pos_1/2,1,1))*(vel+der_vel_1/2))+(c2+der_c2_1/2)*(-nac(pos+der_pos_1/2,1,2)*(vel+der_vel_1/2)))
    der_c2_2 = h*((c1+der_c1_1/2)*(-nac(pos+der_pos_1/2,2,1)*(vel+der_vel_1/2))+(c2+der_c2_1/2)*(-1im*(val[2])/hbar-imag(nac(pos+der_pos_1/2,2,2))*(vel+der_vel_1/2)))
    # ====== 3 ======
    der_pos_3 = h*(vel+der_vel_2/2)
    der_vel_3 = -h*der_surface(pos+der_pos_2/2, surf)/m
    V = Potential(pos+der_pos_2/2)
    val = eigvals(V);
    #der_c1_3 = h*(c1+der_c1_2/2)*(-1im*(V[1,1])/hbar-nac(pos+der_pos_2/2,1,1)*(vel+der_vel_2/2))+(c2+der_c2_2/2)*(-1im*(V[1,2])/hbar-nac(pos+der_pos_2/2,1,2)*(vel+der_vel_2/2))
    #der_c2_3 = h*(c1+der_c1_2/2)*(-1im*(V[2,1])/hbar-nac(pos+der_pos_2/2,2,1)*(vel+der_vel_2/2))+(c2+der_c2_2/2)*(-1im*(V[2,2])/hbar-nac(pos+der_pos_2/2,2,2)*(vel+der_vel_2/2))
    der_c1_3 = h*((c1+der_c1_2/2)*(-1im*(val[1])/hbar-imag(nac(pos+der_pos_2/2,1,1))*(vel+der_vel_2/2))+(c2+der_c2_2/2)*(-nac(pos+der_pos_2/2,1,2)*(vel+der_vel_2/2)))
    der_c2_3 = h*((c1+der_c1_2/2)*(-nac(pos+der_pos_2/2,2,1)*(vel+der_vel_2/2))+(c2+der_c2_2/2)*(-1im*(val[2])/hbar-imag(nac(pos+der_pos_2/2,2,2)*(vel+der_vel_2/2))))
    # ====== 4 ======
    der_pos_4 = h*(vel+der_vel_3)
    der_vel_4 = -h*der_surface(pos+der_pos_3,surf)/m
    V = Potential(pos+der_pos_3)
    val = eigvals(V);
    #der_c1_4 = h*(c1+der_c1_3)*(-1im*(V[1,1])/hbar-nac(pos+der_pos_3,1,1)*(vel+der_vel_3))+(c2+der_c2_3)*(-1im*(V[1,2])/hbar-nac(pos+der_pos_3,1,2)*(vel+der_vel_3))
    #der_c2_4 = h*(c1+der_c1_3)*(-1im*(V[2,1])/hbar-nac(pos+der_pos_3,2,1)*(vel+der_vel_3))+(c2+der_c2_3)*(-1im*(V[2,2])/hbar-nac(pos+der_pos_3,2,2)*(vel+der_vel_3))
    der_c1_4 = h*((c1+der_c1_3)*(-1im*(val[1])/hbar-imag(nac(pos+der_pos_3,1,1))*(vel+der_vel_3))+(c2+der_c2_3)*(-nac(pos+der_pos_3,1,2)*(vel+der_vel_3)))
    der_c2_4 = h*((c1+der_c1_3)*(-nac(pos+der_pos_3,2,1)*(vel+der_vel_3))+(c2+der_c2_3)*(-1im*(val[2])/hbar-imag(nac(pos+der_pos_3,2,2))*(vel+der_vel_3)))
    # === final values ===
    vel += (der_vel_1+2*der_vel_2+2*der_vel_3+der_vel_4)/6
    pos += (der_pos_1+2*der_pos_2+2*der_pos_3+der_pos_4)/6
    c1 += (der_c1_1+2*der_c1_2+2*der_c1_3+der_c1_4)/6
    c2 += (der_c2_1+2*der_c2_2+2*der_c2_3+der_c2_4)/6
    return pos, c1, c2, vel
end

function new_time_evolution_rk4(pos::Real, surf::Int, c1::Number, c2::Number, vel::Real, m::Real, dt::Real)
    # ------
    # der_c1 = c1*(-1im*V[1,1]/hbar-nac(pos,1,1)*vel)+c2*(-1im*V[1,2]/hbar-nac(pos,1,2)*vel)
    # der_c2 = c1*(-1im*V[2,1]/hbar-nac(pos,2,1)*vel)+c2*(-1im*V[2,2]/hbar-nac(pos,2,2)*vel)
    # ------
    h = dt
    der_pos_1 = h*vel
    der_vel_1 = -h*der_surface(pos, surf)/m
    V = Potential(pos)
    val = eigvals(V);
    V1 = val[1]
    V2 = val[2]
    # ====== 1 ======
    der_c1_1 = h*(c1*(-1im*val[1]/hbar)+c2*(-nac(pos,1,2)*vel))
    der_c2_1 = h*(c1*(-nac(pos,2,1)*vel)+c2*(-1im*val[2]/hbar))
    # ====== 2 ======
    der_c1_2 = h*(c1*(-1im*(val[1])/hbar)+(c2)*(-nac(pos,1,2)*vel))
    der_c2_2 = h*(c1*(-nac(pos,2,1)*(vel))+c2*(-1im*(val[2])/hbar))
    # ====== 3 ======
    der_c1_3 = h*(c1*(-1im*(val[1])/hbar)+c2*(-nac(pos,1,2)*vel))
    der_c2_3 = h*(c1*(-nac(pos,2,1)*vel)+c2*(-1im*(val[2])/hbar))
    # ====== 4 ======
    der_c1_4 = h*(c1*(-1im*(val[1])/hbar)+c2*(-nac(pos,1,2)*vel))
    der_c2_4 = h*(c1*(-nac(pos,2,1)*vel)+c2*(-1im*(val[2])/hbar))
    # === final values ===
    c1 += (der_c1_1+2*der_c1_2+2*der_c1_3+der_c1_4)/6
    c2 += (der_c2_1+2*der_c2_2+2*der_c2_3+der_c2_4)/6
    return c1, c2
end

function time_evolution(pos::Real, surf::Int, m::Real, dt::Real, vel::Real)
    # This method defines the time evolution of a particle and
    # update all the variables accordingly.
    # Position is approximated at the first order,
    # Velocity is approximated at the second order.
    force = der_surface(pos, surf) # n-dimensional vector
    a1 = -force/m
    pos += vel*dt+0.5*a1*dt^2
    force = der_surface(pos, surf)
    a2 = -force/m
    vel += 0.5*(a1+a2)*dt
    return pos, vel
end

function density_matrix_coeff(c1::Number, c2::Number)
    # initialize electronic state
    a11 = c1*conj(c1)
    a12 = c1*conj(c2)
    a21 = c2*conj(c1)
    a22 = c2*conj(c2)
    return a11, a12, a21, a22
end

function coeff_b(pos::Real, vel::Real, i::Int, j::Int, c1::Number, c2::Number)::Float64
    a11, a12, a21, a22 = density_matrix_coeff(c1, c2)
    V = Potential(pos)
    #b12 = 2/hbar*imag(conj(a12)*Potential(pos)[i,j])-2*real(conj(a12)*sum(vel.*nac(pos,1,2)))
    #b21 = 2/hbar*imag(conj(a21)*Potential(pos)[i,j])-2*real(conj(a21)*sum(vel.*nac(pos,2,1)))
    b12 = -2*real(conj(a12)*sum(vel.*nac(pos,1,2)))
    b21 = -2*real(conj(a21)*sum(vel.*nac(pos,2,1)))
    b = [[0, b12],[b21, 0]]
    return b[i][j]
end

function switch_surface(pos::Real, vel::Real, surf::Int, c1::Number, c2::Number, m::Real, dt::Real)
    a11, a12, a21, a22 = density_matrix_coeff(c1, c2)
    criterion = 0
    if surf == 1
        criterion = coeff_b(pos,vel,2,1,c1,c2)*dt/a11
    else
        criterion = coeff_b(pos,vel,1,2,c1,c2)*dt/a22
    end
    criterion = maximum([0,real(criterion)])
    zeta = Random.rand()
    if  criterion > zeta
        # If I'm in 1 I want dE=E(2)-E(1)
        # while if I'm in 2 I want dE=E(1)-E(2)
        dE = eigvals(Potential(pos))[3-surf]-eigvals(Potential(pos))[surf]
        if (dE<kinetic_energy(vel, m))
            # now we have to change the velocity keeping constant the energy
            # E = 1/2*M*v*v+V
            # then from the above definition we can define the new velocity wrt the
            # energy surface we are
            tot = total_energy(pos,vel,surf,m)
            surf = 3-surf
            V_new = potential_energy(pos,surf)
            new_kinetic = tot-V_new
            vel = sign(vel)*sqrt(2*new_kinetic/m)
        end
    end
    return vel, surf
end

# ===================================================================================
# Scripts for analyzing the results
#

function external_save(path_to_file::String,
        k_list,
        n_iter::Int,
        delta_t,
        n_test::Int,
        res,
        package_time::Int,
        import_time::Int,
        dynamics_time::Int,
        total_time::Int)
        t = 0
        io = open(path_to_file, "a")
    println(io, "-------------------------------------------------------")
    rightnow = Dates.now()
    println(io, "This file was generated on: ", rightnow)
    println(io, "")
    println(io, "This is a report of time spent by the SH algorithm")
    println(io, "In this simulation we simulated the dynamics at k = $k_list")
    println(io, "The results we got are $res")
    println(io, "We used $n_iter number of iterations for the dynamics with a time step of $delta_t seconds a.u.")
    println(io, "For every k we used a total of $n_test trajectories.")
    println(io, "-------------------------------------------------------")
    println(io, "\n")
    println(io, "==========================================================================================")
    println(io, "|            Task                      |          Time          |       Percentage       |")
    println(io, "|-----------------------------------------------------------------------------------------")
    println(io, "| Initializing the variables           |$(@sprintf("%24.0f", package_time))|$(@sprintf("%1.21f", package_time/total_time))%|")
    println(io, "| Importing the packages               |$(@sprintf("%24.0f", import_time))|$(@sprintf("%1.21f", import_time/total_time))%|")
    println(io, "| Running the dynamics                 |$(@sprintf("%24.0f", dynamics_time))|$(@sprintf("%1.21f", dynamics_time/total_time))%|")
    println(io, "| Switching and adjusting the momentum |$(@sprintf("%24.0f", t))|$(@sprintf("%1.21f", t/total_time))%|")
    println(io, "| NAC terms evaluation                 |$(@sprintf("%24.0f", t))|$(@sprintf("%1.21f", t/total_time))%|")
    println(io, "|-----------------------------------------------------------------------------------------")
    println(io, "|                                      |$(@sprintf("%24.0f", total_time))|$(@sprintf("%1.21f", total_time/total_time)) ")
    println(io, "==========================================================================================")
    close(io)
end
