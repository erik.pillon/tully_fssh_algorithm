println("I'm inside the file!")
# using Dierckx
# using LinearAlgebra
# using Polynomials
# using Plots
# using LaTeXStrings
# using JLD2

# mutable struct simple_wp
#     pos::Array{Float64,1}
#     k::Array{Float64,1}
#     c1::Number
#     c2::Number
#     m::Int
# end

# function kinetic_energy(p::simple_wp)
#     return sum(p.k.*p.k)/2/p.m
# end
#
# function potential_energy(p::simple_wp)
#     if p.surface == 1
#         return E1(p.pos[1])
#     else
#         return E1(p.pos[1])
#     end
# end
#
# function total_energy(p::simple_wp)
#     return potential_energy(p)+kinetic_energy(p)
# end

# function update_density_coefficients(p::simple_wp, dt=1)
#     # for the evolution of the coefficients we work in the adiabatic representation
#     # an explicit Euler method is used
#     E_1 = E1(p.pos[1])
#     E_2 = E2(p.pos[1])
#     vel = p.k/p.m
#     d12 = d_12_interp(p.pos[1])
#     d21 = -conj(d12)
#     # we evaluate first the derivatives
#     der_c1 = p.c1*(-1im*E_1/hbar)
#     der_c1 += p.c2*(-d12*vel[1])
#     der_c2 = p.c1*(-d21*vel[1])
#     der_c2 += p.c2*(-1im*E_2/hbar)
#     p.c1 += dt*der_c1
#     p.c2 += dt*der_c2
# end
#
# function der_surface(p::simple_wp, displacement=0)
#     eps = 1.000001
#     if p.surface == 1
#         return (E1(p.pos[1]+displacement+eps)-E1(p.pos[1]+displacement-eps))/2eps
#     else
#         return (E2(p.pos[1]+displacement+eps)-E2(p.pos[1]+displacement-eps))/2eps
#     end
# end
#
# function time_evolution_rk4(p::simple_wp, dt::Real)
#     h = dt
#     # ====== 1 ======
#     E_1 = E1(p.pos[1])
#     E_2 = E2(p.pos[1])
#     d12 = d_12_interp(p.pos[1])
#     d21 = -conj(d12)
#     d11 = d_11_interp(p.pos[1])
#     d22 = d_22_interp(p.pos[1])
#     # ---------------
#     vel = p.k[1]/p.m
#     der_pos_1 = h*vel[1]
#     der_vel_1 = -h*der_surface(p)/p.m
#     der_c1_1 = h*(p.c1*(-1im*E_1/hbar-imag(d11)*vel)+p.c2*(-d12*vel))
#     der_c2_1 = h*(p.c1*(-d21*vel)+p.c2*(-1im*E_2/hbar-imag(d22)*vel))
#     # ====== 2 ======
#     der_pos_2 = h*(vel[1]+der_vel_1[1]/2)
#     der_vel_2 = -h*der_surface(p,der_pos_2[1]/2)/p.m
#     # ---------------
#     E_1 = E1(p.pos[1]+der_pos_1/2)
#     E_2 = E2(p.pos[1]+der_pos_1/2)
#     d12 = d_12_interp(p.pos[1]+der_pos_1/2)
#     d21 = -conj(d12)
#     d11 = d_11_interp(p.pos[1]+der_pos_1/2)
#     d22 = d_22_interp(p.pos[1]+der_pos_1/2)
#     # ---------------
#     der_c1_2 = h*((p.c1+der_c1_1/2)*(-1im*E_1/hbar-imag(d11)*(vel+der_vel_1/2))+(p.c2+der_c2_1/2)*(-d12*(vel+der_vel_1/2)))
#     der_c2_2 = h*((p.c1+der_c1_1/2)*(-d21*(vel+der_vel_1/2))+(p.c2+der_c2_1/2)*(-1im*E_2/hbar-imag(d22)*(vel+der_vel_1/2)))
#     # ====== 3 ======
#     E_1 = E1(p.pos[1]+der_pos_2/2)
#     E_2 = E2(p.pos[1]+der_pos_2/2)
#     d12 = d_12_interp(p.pos[1]+der_pos_2/2)
#     d21 = -conj(d12)
#     d11 = d_11_interp(p.pos[1]+der_pos_2/2)
#     d22 = d_22_interp(p.pos[1]+der_pos_2/2)
#     # ---------------
#     der_pos_3 = h*(vel[1]+der_vel_2[1]/2)
#     der_vel_3 = -h*der_surface(p,der_pos_2/2)/p.m
#     # ---------------
#     der_c1_3 = h*((p.c1+der_c1_2/2)*(-1im*E_1/hbar-imag(d11)*(vel+der_vel_2/2))+(p.c2+der_c2_2/2)*(-d12*(vel+der_vel_2/2)))
#     der_c2_3 = h*((p.c1+der_c1_2/2)*(-d21*(vel+der_vel_2/2))+(p.c2+der_c2_2/2)*(-1im*E_2/hbar-imag(d22)*(vel+der_vel_2/2)))
#     # ====== 4 ======
#     E_1 = E1(p.pos[1]+der_pos_3)
#     E_2 = E2(p.pos[1]+der_pos_3)
#     d12 = d_12_interp(p.pos[1]+der_pos_3)
#     d21 = -conj(d12)
#     d11 = d_11_interp(p.pos[1]+der_pos_3)
#     d22 = d_22_interp(p.pos[1]+der_pos_3)
#     # ---------------
#     der_pos_4 = h*(vel[1]+der_vel_3)
#     der_vel_4 = -h*der_surface(p,der_pos_3)/p.m
#     # ---------------
#     der_c1_4 = h*(((p.c1+der_c1_3)*(-1im*E_1/hbar-imag(d11))*(vel+der_vel_3))+(p.c2+der_c2_3)*(-d12*(vel+der_vel_3)))
#     der_c2_4 = h*((p.c1+der_c1_3)*(-d21*(vel+der_vel_3))+(p.c2+der_c2_3)*(-1im*E_2/hbar-imag(d22)*(vel+der_vel_3)))
#     # === final values ===
#     vel += (der_vel_1+2*der_vel_2+2*der_vel_3+der_vel_4)/6
#     p.k[1] = vel*p.m
#     p.pos[1] += (der_pos_1+2*der_pos_2+2*der_pos_3+der_pos_4)/6
#     p.c1 += (der_c1_1+2*der_c1_2+2*der_c1_3+der_c1_4)/6
#     p.c2 += (der_c2_1+2*der_c2_2+2*der_c2_3+der_c2_4)/6
# end
#
# function time_evolution(p::simple_wp, dV, dt::Real)
#     # This method defines the time evolution of a particle and
#     # update all the variables accordingly.
#     # Position is approximated at the first order,
#     # momentum is approximated at the second order.
#     # ======= Surface averaged derivative ==========
#     der_pot = [conj(p.c1) conj(p.c2)]*dV(p.pos[1])*[p.c1; p.c2]
#     # ==============================================
#     eps = 0.00000001
#     # ======= Topological Force ==========
#     d_d12 = (d_12_interp(p.pos[1]+eps)-d_12_interp(p.pos[1]-eps))/2eps
#     d_d21 = -conj(d_d12)
#     d_d11 = (d_11_interp(p.pos[1]+eps)-d_11_interp(p.pos[1]-eps))/2eps
#     d_d12 = (d_22_interp(p.pos[1]+eps)-d_22_interp(p.pos[1]-eps))/2eps
#     c12 = conj(p.c2)*p.c1*d_d21
#     c21 = conj(p.c1)*p.c2*d_d12
#     # ====================================
#     force = -der_pot-1im*hbar*(c21+c12)*p.k[1]/p.m
#     a1 = force/p.m
#     p.k[1] += force*dt/2
#     p.pos[1] += p.k[1]*dt+0.5*a1*dt^2
#     # ======= Topological Force ==========
#     d_d12 = (d_12_interp(p.pos[1]+eps)-d_12_interp(p.pos[1]-eps))/2eps
#     d_d21 = -conj(d_d12)
#     d_d11 = (d_11_interp(p.pos[1]+eps)-d_11_interp(p.pos[1]-eps))/2eps
#     d_d12 = (d_22_interp(p.pos[1]+eps)-d_22_interp(p.pos[1]-eps))/2eps
#     c12 = conj(p.c2)*p.c1*d_d21
#     c21 = conj(p.c1)*p.c2*d_d12
#     # ====================================
#     force = -der_pot-1im*hbar*(c21+c12)*p.k[1]/p.m
#     a2 = force/p.m
#     p.k[1] += a2*dt/2
# end
#
# function density_matrix_coeff(p::simple_wp)
#     # initialize electronic state
#     a11 = p.c1*conj(p.c1)
#     a12 = p.c1*conj(p.c2)
#     a21 = p.c2*conj(p.c1)
#     a22 = p.c2*conj(p.c2)
#     return a11, a12, a21, a22
# end
#
# function coeff_b(p::simple_wp,i::Int,j::Int)::Float64
#     a11, a12, a21, a22 = density_matrix_coeff(p)
#     vel = p.k[1]/p.m
#     if i == 1
#         d12 = d_12_interp(p.pos[1])
#         b = -2*real(conj(a12)*sum(vel.*d12))
#     else
#         d21 = -conj(d_12_interp(p.pos[1]))
#         b = -2*real(conj(a21)*sum(vel.*d21))
#     end
#     return b
# end
#
# function run_dynamics(p::simple_wp, dV, dt::Number, ts::Int)
#     # -- INPUT --
#     # p: Particle
#     # dt: timestep
#     # ts: number of timesteps
#     i = 0
#     while i<ts && abs(p.pos[1])<5.5
#         time_evolution(p,dV,dt)
#         i += 1
#     end
# end
