A = parse(Float64,ARGS[1])
println(A)

using LinearAlgebra

function Potential(x::Real)::Array{Real,2}
    A = 0.01
    B = 1.6
    C = 0.005
    D = 1.0
    V = zeros(2,2)
    if x>0
        V[1,1] = A*(1-exp(-B*x))
    else
        V[1,1] = -A*(1-exp(B*x))
    end
    V[2,2] = -V[1,1]
    V[1,2] = C*exp(-D*x^2)
    V[2,1] = V[1,2]
    return V
end

function nac(pos::Real, i::Int, j::Int)
    # function for the evaluation of the non adiabatic coupling vector d_ij
    # non-adiabatic vector cupling = Connection (= vector potential)
    # i and j can take ony values 1 and 2
    #
    # N.B.: if i==j, then the result needs to be a purely imaginary quantity
    dim = length(pos);
    dx = 0.000001;
    nac = zeros(dim);
    for count = 1:dim
        e_vec1 = eigvecs(Potential(pos[count]))[:,i];
        e_vec2_1 = eigvecs(Potential(pos[count]-dx))[:,j];
        e_vec2_2 = eigvecs(Potential(pos[count]+dx))[:,j];
        der = (e_vec2_2-e_vec2_1)/2/dx;
        nac[count] = sum(conj(e_vec1).*der)
    end
    return nac[1]
end
