# Next step is the following:
# create the full picture of the original work of Tully
#   model = ["model 1", "model 2", "model 3"]
#   pic = ["pic a","pic b","pic c"]
# and then one goes
#   for p = pic ...
# and then can create all the plots

#
# “God does not care about our mathematical difficulties. He integrates empirically.”
# Albert Einstein
#

println("--- Script started ---")
println("--- Importing methods ---")
include("particle_methods.jl")
println("--- methods imported correctly ---")

# -----------------------------
using LinearAlgebra
using Plots
using Random
# -----------------------------

dt = 1;

# the label debug can assume two values:
# - false: will run a single trajectory dynamics and will plot all the meaningful quantities
#   for debugging
# - true: will run plot the results of the original paper of Tully

debug = true;

# Possible models are
#   - Simple_avoided_crossing
#   - Dual_avoided_crossing
#   - Extended_coupling

Potential(x) = Dual_avoided_crossing(x)
dV(x) = dV_Dual_avoided_crossing(x)

n_time_steps = 1000
dens = zeros(n_time_steps)
nacs = zeros(n_time_steps)
cb = zeros(n_time_steps)
E_s = zeros(n_time_steps)
T_s = zeros(n_time_steps)
V_s = zeros(n_time_steps)
c1s = zeros(Complex,(1,n_time_steps))
c2s = zeros(Complex,(1,n_time_steps))
E1 = zeros(n_time_steps)
E2 = zeros(n_time_steps)
psi_1 = Array{Float64,2}(undef,2,n_time_steps)
psi_2 = Array{Float64,2}(undef,2,n_time_steps)

x = range(-5,stop=5,length=n_time_steps)

for (index, val) in enumerate(x)
    nacs[index] = nac(val,1,2)
    E1[index], E2[index] = eigvals(Potential(val))[:]
    e = eigvecs(Potential(val))
    if index == 1
        psi_1[:,index], psi_2[:,index]  = e[:,1], e[:,2]
        continue
    end
    psi_1[:,index], psi_2[:,index]  = e[:,1], e[:,2]
    # if sum(psi_1[:,index].*psi_1[:,index-1]) < 0
    #     psi_1[:,index] *= -1
    #     nacs[index] *= -1
    # end
    if sum(psi_2[:,index].*psi_2[:,index-1]) < 0
        psi_2[:,index] *= -1
        nacs[index] *= -1
    end
end


gr()
# wavefunction density plot
p1 = plot(x,nacs,
    label="nac")
#ylabel!("Norm of the wavefunction")
#title!("Time evolution of the pseudo-particle")
# non adiabatic coupling terms plot
p2 = plot(1:n_time_steps,E1)
p2 = plot!(1:n_time_steps,E2)
# evolution of the coefficients c_1 and c_2
# final plot
plot(p1,p2,layout=(2,1),legend=true)
# plot!(1:n_time_steps,cb)

println("saving the nac data")
io = open("Desktop/GitLab_works/tully_fssh_algorithm/Julia/nac.txt", "w+") do io
    for x in nacs
        println(io, x)
    end
end

println("script ended\n")
