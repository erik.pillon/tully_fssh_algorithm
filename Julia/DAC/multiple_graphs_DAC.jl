# multiple_graphs_DAC.jl

using Plots
using Dierckx
using Polynomials
using JLD2
using JLD

system = "/Dual_avoided_crossing.jld"

s = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/Surfaces"*system
#s = system

d_22_interp = load(s,"d_22_interp")
d_12_interp = load(s,"d_12_interp")
d_11_interp = load(s,"d_11_interp")
E2 = load(s,"E2")
E1 = load(s,"E1")

# ================= FIRST PLOT =================
x = range(-5,stop=5, length=5000)
y1 = E1(x)
y2 = E2(x)
y3 = d_12_interp(x)

p1 = plot(x,y1,
    label = "E1",
    xlabel = "Nuclear Coordinates")
plot!(x,y2,label = "E2")
plot!(x,y3/12,
    line = (:dash, :green),
    label="d_12/12")
# ==============================================

# ================= SECOND PLOT =================
p2 = plot(xlabel="log(Energy) (a.u.)",
    ylabel="Transmission Probability",
    tickfontsize = 10,
    legend=:topleft)

# -----------------------------------------------
# main_2d.out
k_range = range(-4,stop=1,step=0.25)
transm = [0.001, 0.009, 0.037, 0.085, 0.136,
    0.0995, 0.0245, 0.029, 0.244, 0.537,
    0.6705, 0.602, 0.3595, 0.107, 0.001,
    0.0825, 0.2635, 0.4705, 0.6045, 0.7245, 0.786]
plot!(k_range, transm,
    line = (:purple),
    linewidth = 2,
    markersize = 5,
    markershape=:diamond,
    markercolor=:purple,
    markerstrokecolor = :purple,
    label="Standard FSSH")
# plot!(k_range, transm, seriestype = :scatter,label="")
# -----------------------------------------------
# modified_wrong_Ehrenfest.out
# standard ehrenfest
transm = [0.058759884418298776, 0.05262747395044259, 0.04208446963323773,
    0.02507471206745231, 0.004492100614569053, 0.005552496810214715, 0.06260532907200325,
    0.20049381900353444, 0.4222860330277598, 0.5908641525559082, 0.6214427571345973,
    0.5121480849752283, 0.28810268350130264, 0.06875887274718538, 0.0019763021232312972,
    0.09826078253413709, 0.2750192315232064, 0.4597793433997796, 0.6142397254476057,
    0.7216633845955948, 0.7771789442491354]
plot!(k_range, transm,
    line = (:blue),
    linewidth = 2,
    markershape=:utriangle,
    markersize = 5,
    markercolor=:blue,
    markerstrokecolor = :blue,
    label="Standard Ehrenfest")
# plot!(k_range, transm, seriestype = :scatter,label="")

# top_force.jl
# top_force.out.out
t = [0.07127433333199465, 0.07723846456607177, 0.18059777637021382, 0.1355615002762202,
    0.28923559599313675, 0.12929496071147198, 0.4957508161944523,  0.4145337367606318,
    0.026194198158740054, 0.502676087737328, 0.781413615034046, 0.836360426025925,
    0.685734296770501, 0.38867168580722344, 0.04825871583145349, 0.25908319576116595,
    0.49441993316038824, 0.6629027833022452, 0.7766720028547789, 0.8465253844657674, 0.8803744809770746]
plot!(k_range, t.^2,
    line = (:red),
    linewidth = 2,
    markershape=:x,
    markersize = 5,
    markercolor=:red,
    markerstrokecolor=:red,
    label="Ehrenfest (CG)")
# plot!(k_range, t.^2, seriestype = :scatter, label="")
# ------------------------------------------------------------------------------

# extended_FSSH.out
transm = [0.0026666666666666666, 0.004, 0.012, 0.021, 0.029333333333333333, 0.044,
    0.068, 0.09133333333333334, 0.13533333333333333, 0.19266666666666668, 0.289,
    0.38433333333333336, 0.501, 0.6516666666666666, 0.7793333333333333,
    0.9016666666666666, 0.967, 0.9866666666666667, 0.9926666666666667,
    0.9946666666666667, 0.996]
plot!(k_range, transm,
    line = (:green),
    linewidth = 2,
    markersize = 5,
    markershape=:diamond,
    markercolor=:green,
    markerstrokecolor = :purple,
    label="Top Force")

# ------------------------------------------------------------------------------
# standard_ehrenfest_erik
# transm = [0.058759884418298776, 0.05262747395044259, 0.04208446963323773,
#     0.02507471206745231, 0.004492100614569053, 0.005552496810214715,
#     0.06260532907200325, 0.20049381900353444, 0.4222860330277598,
#     0.5908641525559082, 0.6214427571345973, 0.5121480849752283,
#     0.28810268350130264, 0.06875887274718538, 0.0019763021232312972,
#     0.09826078253413709, 0.2750192315232064, 0.4597793433997796,
#     0.6142397254476057, 0.7216633845955948, 0.7771789442491354]
# plot!(k_range, transm,
#     line = (:yellow),
#     linewidth = 2,
#     markersize = 5,
#     markershape=:utriangle,
#     markercolor=:yellow,
#     markerstrokecolor = :purple,
#     label="Ehrenfest Erik")
# ------------------------------------------------------------------------------



fig = plot(p1,p2,
    layout=grid(2,1,
    heights=[0.4, 0.6]),
    size = (800,800),
    tickfontsize=12,
    yguidefontsize=10,
    legendfontsize=10)

# fig = p2

path = "/home/erik/Desktop/GitLab_projects/tully_fssh_algorithm/Julia/DAC/"
name = "Dual_avoided_crossing_results"
savefig(fig, path*name)
